/*
 * ndkbenchmark.c
 *
 * NDK Benchmark Application
 *
 * Copyright (C) 2005, 2010-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  ndkbenchmark.c
 *
 *   @brief   
 *      Contains functions for the NDK benchmark test.
 *
 */
 
#include "hpdspua.h"

/* BIOS6 includes to calculate load and handle tasks */
#include <ti/sysbios/utils/Load.h>
#include <ti/bios/include/tsk.h>

/* The following code and define is used for code timing measurements */
//#define TIMING
#ifdef TIMING
extern void print_nimu_timing(void);
extern void reset_nimu_timing(void);
#include <ti/csl/csl_tsc.h>
#define MAX_TIMING_PACKETS	1000
static CSL_Uint64	 benchmark_time_start;
static CSL_Uint64	 benchmark_time_end;
#endif

extern int parseKeyValuePairs(char *);

/* Tasks for running the tests */
static void   copyUDPTestee_NetworkOpen();
static void   copyUDPTester_NetworkOpen();
static void   copyTCPTestee_NetworkOpen();
static void   copyTCPTester_NetworkOpen();
static void   UDP_perform_receive(void);
static void   UDP_perform_send(void);
static void   TCP_perform_receive(void);
static void   TCP_perform_send(void);

/* Test Configurables */
static char REMOTE_IPADDR_STRING[17];
static char LOCAL_IPADDR_STRING[17];
static char PROTOCOL[4];
static int32_t PACKETS  =   5000;
static int32_t TESTSIZE =   1000;
static int32_t DIRECTION = 0;
static int32_t PROTO = 0;

/* stores the results we send back to the client */
#pragma DATA_SECTION(response, ".far:WEBDATA");
static char response [2048];
#pragma DATA_SECTION(tmpresponse, ".far:WEBDATA")
static char tmpresponse [200];

/*************************************************************************
 *  @b ndkThroughputBench(SOCKET htmlSock, int ContentLength, char *pArgs )
 * 
 *  @n
 *  
 * 	This is the CGI function which will start the benchmark tests.
 *
 *  @param[in]  
 *  htmlSock - Socket to the browser
 * 
 *  @param[in] 
 *  ContentLength - Length of the page request from the browser
 * 
 *  @param[in] 
 *  pArgs - Not used
 *  
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/
int32_t ndkThroughputBench(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
    IPN     myIP;
    IPN     yourIP;
    struct  sockaddr_in Info;
    int32_t     InfoLength;
 
 
    /*  Process the POST variables to get the test parameters */
    if (!html_processPost(htmlSock,ContentLength)) {
        return 1;
    }

    InfoLength = sizeof(Info);
    getsockname( htmlSock, (PSA)&Info, &InfoLength );
    myIP = Info.sin_addr.s_addr;
    NtIPN2Str( myIP, LOCAL_IPADDR_STRING);

    InfoLength = sizeof(Info);
    getpeername( htmlSock, (PSA)&Info, &InfoLength );
    yourIP = Info.sin_addr.s_addr;
    NtIPN2Str( yourIP, REMOTE_IPADDR_STRING);


    TESTSIZE  = 1000;
    PACKETS =  atoi(html_getValueFor("PKTSIZE"));
    strcpy(PROTOCOL, html_getValueFor("protocol"));

    platform_write("\n %s Throughput test : %s for %d packets of %d bytes\n", PROTOCOL, POST_values[0], PACKETS*1000,TESTSIZE);
 
     if ( !strcmp(POST_values[0], "RX"))
     {
        if (!strcmp(PROTOCOL, "UDP"))
            copyUDPTestee_NetworkOpen();
        else
            copyTCPTestee_NetworkOpen();
        DIRECTION = 1;
     }
     else if (!strcmp(POST_values[0], "TX"))
     {
        if (!strcmp(PROTOCOL, "UDP"))
            copyUDPTester_NetworkOpen();
        else
            copyTCPTester_NetworkOpen();
        DIRECTION = 0;
     }
     else
     {
        platform_write( "Unknown test option\n");
        return (1);
     }

 
       html_start(PAGEHEAD);
       html ("<body>");

       html ("<div id='wrapper'><div id='logo'><h1>Throughput Test</h1>");
	   html ("</div><div id='header'><div id='menu'><ul><li><a href='bench.html'>Back</a></li>");
	   html ("</ul></div></div></div>");

       html_var ("<div id='page'>");
       html_var ("<div id='content'>");
       html_var ("<p>Test is in progess. The results will be displayed when it completes</p>");
       html_var ("<hr>");       


       html_var ("<div id='ndkbenchmark.NDKBenchmark_container'>");                                  
       html_var ("<!--[if !IE]> -->");
       html_var ("<object classid='java:ndkbenchmark.NDKBenchmark.class' type='application/x-java-applet' archive='NDKBenchmark.jar' width='600' height='400' standby='Test is in progress...' >");
       html_var ("<param name='archive' value='NDKBenchmark.jar' />");
       html_var ("<param name='mayscript' value='true' />");
       html_var ("<param name='ipaddress' value=%s />", LOCAL_IPADDR_STRING);
       html_var ("<param name='packets' value=%d />", PACKETS);
       html_var ("<param name='direction' value=%d />", DIRECTION);
       html_var ("<param name='protocol' value=%d />", PROTO);
       html_var ("<param name='scriptable' value='true' />");
       html_var ("<param name='image' value='loading.gif' />");
       html_var ("<param name='boxmessage' value='Loading Benchmark Applet...' />");
       html_var ("<param name='boxbgcolor' value='#FFFFFF' />");
       html_var ("<param name='test_string' value='outer' />");
       html_var ("<!--<![endif]-->");
       html_var ("<object classid='clsid:CAFEEFAC-0016-0000-FFFF-ABCDEFFEDCBA' codebase='http://java.sun.com/update/1.5.0/jinstall-1_5_0_15-windows-i586.cab'  width='600' height='400' standby='Test is in progress...'  >");
       html_var ("<param name='code' value='ndkbenchmark.NDKBenchmark' />");
       html_var ("<param name='archive' value='NDKBenchmark.jar' />");
       html_var ("<param name='mayscript' value='true' />");
       html_var ("<param name='scriptable' value='true' />");
       html_var ("<param name='image' value='loading.gif' />");
       html_var ("<param name='ipaddress' value=%s />", LOCAL_IPADDR_STRING);
       html_var ("<param name='packets' value=%d />", PACKETS);
       html_var ("<param name='direction' value=%d />", DIRECTION);
       html_var ("<param name='protocol' value=%d />", PROTO);
       html_var ("<param name='boxmessage' value='Loading Benchmark Applet...' />");
       html_var ("<param name='boxbgcolor' value='#FFFFFF' />");
       html_var ("<param name='test_string' value='inner' />");
       html_var ("<p>");
       html_var ("<strong>");
       html_var ("This browser does not have a Java Plug-in.");
       html_var ("<br />");
       html_var ("<a href='http://www.java.com/getjava' title='Download Java Plug-in'>");
       html_var ("Get the latest Java Plug-in here.");
       html_var ("</a>");
       html_var ("</strong>");
       html_var ("</p>");
       html_var ("</object>");
       html_var ("<!--[if !IE]> -->");
       html_var ("</object>");
       html_var ("<!--<![endif]-->");
       html_var ("</div>");
       html ("</div><div style='clear: both;'>&nbsp;</div></div>");
       html (PAGEFOOTER);
       html_end ("</body></html>");

       // Send header
       httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);
       // After this call we MUST send the data since a CRLF is being sent
       httpSendEntityLength(htmlSock, html_getsize() );
       // Send the page
       httpSendClientStr(htmlSock, html_getpage());
    
       return 0;
}

/*************************************************************************
 *  @b copyxxOpen(SOCKET htmlSock, int ContentLength, char *pArgs )
 * 
 *  @n
 * 	Spins up the task to handle receicving or transmitting the test packets.
 *
 *  @retval
 *  None
 ************************************************************************/
static void copyUDPTestee_NetworkOpen()
{

    /* Create the UDP Receive Performance Task. */
    PROTO = 0;
    (void) TaskCreate( UDP_perform_receive, "UDPBenchmarkRX", OS_TASKPRIHIGH, 0x1400, 0, 0, 0 );
}

static void copyUDPTester_NetworkOpen()
{
    PROTO = 0;
    /* Create the UDP Sender Performance Task. */
    (void) TaskCreate( UDP_perform_send, "UDPBenchmarkTX", OS_TASKPRIHIGH, 0x1400, 0, 0, 0 );

}

static void copyTCPTestee_NetworkOpen()
{
    PROTO = 1;
    /* Create the TCP Receive Performance Task. */
    (void) TaskCreate( TCP_perform_receive, "TCPBenchmarkRX", OS_TASKPRIHIGH, 0x1400, 0, 0, 0 );

}

static void copyTCPTester_NetworkOpen()
{
     PROTO = 1;
    /* Create the TCP Sender Performance Task. */
    (void) TaskCreate( TCP_perform_send, "TCPBenchmarkTX", OS_TASKPRIHIGH, 0x1400, 0, 0, 0 );

}

/*************************************************************************
 *  @b UDP_perform_receive(void)
 * 
 *  @n
 *  
 * 	UDP Receive benchmark.
 * 
 *  @retval
 *  None
 ************************************************************************/
/* This is the main UDP Receiver Performance Monitor Task. */
void sendclient();

static void UDP_perform_receive(void)
{
    SOCKET          sudp = INVALID_SOCKET;
    struct sockaddr_in sin1;
    char*           pBuf;
    HANDLE          hBuffer;
    int32_t         bytes;
    uint32_t        tsMsec, ts1Msec, startMsec, endMsec;
    uint32_t        ts, ts1, tn;
    uint32_t        totalBytes = 0;
    int32_t         counter = 0;
#ifdef TIMING
    uint32_t		bm_time;
    uint32_t		i = 0;
#endif
    struct timeval timeout;


   	platform_write( "UDP Receive Task started\n");

    /* Allocate the file environment for this task */
    fdOpenSession( TaskSelf() );

    /* Create the main UDP listen socket */
    sudp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if( sudp == INVALID_SOCKET ) {
        platform_write( "Failed to create the receive socket \n");
        goto leave;
    }
    
    /* Set Port = 10001, leaving IP address = Any */
    bzero( &sin1, sizeof(struct sockaddr_in) );
    sin1.sin_family = AF_INET;
    sin1.sin_len    = sizeof( sin1 );
    sin1.sin_port   = htons(10001);

    /* Bind the socket */
    if ( bind( sudp, (PSA) &sin1, sizeof(sin1) ) < 0 ) {
        platform_write( "Failed to bind the receive socket \n");
    	goto leave;
    }

    /* Set a time out for the receive in case there is data loss.. time out is in seconds */
    timeout.tv_sec  = 60;	/* wait up to 60 seconds per packet */
    timeout.tv_usec = 0;
    if( setsockopt( sudp, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0 )
    {
        platform_write( "Failed to set the sockopt for receive socket \n");
		goto leave;
    }

    /* Wait for all the data to arrive. This check ensures that we will not come out of the
     * loop at all even if there is a single packet drop. */
#ifdef TIMING
    reset_nimu_timing();
#endif

    while (totalBytes < (TESTSIZE * PACKETS* 1000))
    {
        bytes = (int)recvnc( sudp, (void **)&pBuf, 0, &hBuffer );

		/*platform_write("looping to receive data %d , total = %d\n", bytes, totalBytes);*/
        if (bytes < 0)
        {
        	if (fdError() == EWOULDBLOCK) {
        		break; /* we get "would block" on a receive timeout */
        	}
        	else {
				/* Receive failed: Close the session & kill the task */
				platform_write("Receive failed after total bytes %d Error:%d\n",totalBytes, fdError());
				goto leave;
        	}
        }
        else if (bytes == 0)
            break;

        /* Check if this is the first received packet */
        if (counter == 0) {
            /* YES. Record the start timer. */
            llEnter();
            ts = llTimerGetTime(&tsMsec);
            llExit();
            counter++;
#ifdef TIMING
            /* record start time */
            benchmark_time_start = CSL_tscRead();
#endif
        } 
 
        /* Clean out the buffer */
        recvncfree( hBuffer );

        /* Increment the statistics. */
        totalBytes = totalBytes + bytes;

#ifdef TIMING
        /* record end time */
        i++;
        if (i == MAX_TIMING_PACKETS) {
        	benchmark_time_end = CSL_tscRead();
        }
#endif
    }

    /* Control Comes here only if all the data has been received */
    llEnter();
    ts1 = llTimerGetTime(&ts1Msec);
    llExit();

    /* Compute total time in milliseconds */
    startMsec  = (ts * 1000) + tsMsec;
    endMsec    = (ts1 * 1000) + ts1Msec;
    tn = endMsec - startMsec;

    if (totalBytes > 0 )
    {
        /* Print out the Receive Results. */
        /* Must send back the ----- line as the applet keys on it */
            platform_write("------------------------------------------------\n");
            platform_write("Total Data received: %d bytes\n", totalBytes);
            sprintf(response,"Total Data received: %d bytes\n", totalBytes);
            platform_write("Start Time         : %d msec\n", startMsec);
            sprintf(tmpresponse,"Start Time         : %d msec\n", startMsec);
            strcat(response, tmpresponse);
            platform_write("End   Time         : %d msec\n", endMsec);
            sprintf(tmpresponse,"End   Time         : %d msec\n", endMsec);
            strcat(response, tmpresponse);
            platform_write("Total Time expired : %d msec\n", tn);
            sprintf(tmpresponse,"Total Time expired : %d msec\n", tn);
            strcat(response, tmpresponse);
            sprintf(tmpresponse,"Receive Throughput : %u Mb/s\n", (((totalBytes/tn)*1000)*8)/1000000);
            strcat(response, tmpresponse);
            platform_write("Receive Throughput : %u Mb/s \n", (((totalBytes/tn)*1000)*8)/1000000);
#ifdef TIMING
            bm_time = (uint32_t) benchmark_time_end - benchmark_time_start;
            platform_write("Total ticks for %d packets were %d \n", MAX_TIMING_PACKETS, bm_time);
            print_nimu_timing();
#endif
            sprintf (tmpresponse, "Note: These numbers reflect the time it takes the\n");
            strcat(response, tmpresponse);
            platform_write(tmpresponse);
            sprintf(tmpresponse, "packets to be transmitted by the PC and received\n");
            platform_write(tmpresponse);
            strcat(response, tmpresponse);
            sprintf(tmpresponse, "by the EVM.\n");
            platform_write(tmpresponse);
            strcat(response, tmpresponse);
            platform_write("------------------------------------------------\n");
     }
     else
    {
         sprintf(response,"\n\n.....Failed to receive...please re run the test\n\n");
         platform_write("\n\n.....Failed to receive...please re run the test\n\n"); 
     }

    /* Spin up a task to send the results back */
    (void) TaskCreate( sendclient, "SendClient", OS_TASKPRINORM, 0x1400, 0, 0, 0 );

leave:
    if (sudp != INVALID_SOCKET )
       fdClose(sudp);
    TaskSleep(2000);
    fdCloseSession( TaskSelf() );
    TaskDestroy( TaskSelf() );
        
	return;    
}

/*************************************************************************
 *  @b sendclient(void)
 * 
 *  @n
 *  
 * 	Sends the results of the test back to the applet for display.
 * 
 *  @retval
 *  None
 ************************************************************************/
void sendclient()
{
    SOCKET          sudp = INVALID_SOCKET;
    struct sockaddr_in  sin1;
    int32_t             count = 0;
    int32_t             bytes;
 
    /* Raise priority to transfer data & wait for the link to come up. */
    TaskSetPri(TaskSelf(), 1);
   
    /* Allocate the file environment for this task */
    fdOpenSession( TaskSelf() );
    
    /* Create the main UDP listen socket */
    sudp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if( sudp == INVALID_SOCKET ) 
    {
        platform_write("Fail socket, %d\n", fdError());
		goto leave;
    }

    /* Set Port = 7000, IP address = IPAddrSend */
    bzero( &sin1, sizeof(struct sockaddr_in) );
    sin1.sin_family         = AF_INET;
    sin1.sin_len            = sizeof( sin1 );
    sin1.sin_addr.s_addr    = inet_addr(REMOTE_IPADDR_STRING); 
    sin1.sin_port           = htons(7000);

    // Connect socket
    if ( connect( sudp, (PSA) &sin1, sizeof(sin1) ) < 0 ) 
    {
        platform_write("Fail connect, %d\n", fdError());
		goto leave;
    }

    /* Send out all the packets. */
    bytes = send(sudp, (char *)&response, strlen(response), 0);
    if( bytes < 0 ) 
    {
        platform_write("Error: Send of results failed Packets Transmitted: %d Error:%d\n", count, fdError());
		goto leave;
     } 

leave:

    /* Close the socket. */
    if( sudp != INVALID_SOCKET ) 
   		fdClose( sudp );
    fdCloseSession( TaskSelf() );
    TaskDestroy( TaskSelf() );
}

/*************************************************************************
 *  @b UDP_perform_send(void)
 * 
 *  @n
 *  
 * 	UDP Transmit benchmark.
 * 
 *  @retval
 *  None
 ************************************************************************/
static void UDP_perform_send()
{
    SOCKET              sudp = INVALID_SOCKET;
    struct sockaddr_in  sin1;
    uint32_t          	tsMsec, ts1Msec, startMsec, endMsec;
    uint32_t          	ts, ts1, tn;
    int32_t             count = 0;
    uint32_t         	totalBytes = 0;
    int32_t            	bytes;
    uint32_t			mbps;
    char				*pBuf  = NULL;
    char				*pNcBuf = NULL;
    HANDLE          	hBuffer;
    struct timeval  	timeout;
    Error_Block	    	errorBlock;
#ifdef TIMING
    uint32_t			bm_time;
#endif

    Error_init(&errorBlock);

    platform_write( "UDP Transmit Task started\n");
    
    /* Raise priority to transfer data & wait for the link to come up. */
    TaskSetPri(TaskSelf(), 1);
    
    /* Allocate the file environment for this task */
    fdOpenSession( TaskSelf() );

    /* Create the UDP socket */
    sudp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if( sudp == INVALID_SOCKET ) 
    {
        platform_write("Failed to create socket, %d\n", fdError());
		goto leave;
    }

    /* Set Port = 10001, leaving IP address = Any */
    bzero( &sin1, sizeof(struct sockaddr_in) );
    sin1.sin_family = AF_INET;
    sin1.sin_len    = sizeof( sin1 );
    sin1.sin_addr.s_addr    = inet_addr(LOCAL_IPADDR_STRING); 
    sin1.sin_port   = htons(10001);

    /* Bind the socket */
    if ( bind( sudp, (PSA) &sin1, sizeof(sin1) ) < 0 ) {
        platform_write( "Failed to bind the socket \n");
		goto leave;
    }

    // Set Port = 10001, IP address = IPAddrSend
    bzero( &sin1, sizeof(struct sockaddr_in) );
    sin1.sin_family         = AF_INET;
    sin1.sin_len            = sizeof( sin1 );
    sin1.sin_addr.s_addr    = inet_addr(REMOTE_IPADDR_STRING); 
    sin1.sin_port           = htons(10001);

    /* Connect socket */
    if ( connect( sudp, (PSA) &sin1, sizeof(sin1) ) < 0 ) 
    {
        platform_write("Failed to connect to the socket, %d\n", fdError());
		goto leave; 
    }


    /* Set a time out for the receivein case there is data loss.. time out is in seconds */
    timeout.tv_sec  = 120;   /* wait up to 60 seconds for receiver ack */
    timeout.tv_usec = 0;
    if( setsockopt( sudp, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0 )
    {
        platform_write( "Failed to set the sockopt for receive socket \n");
		goto leave;
    }

    /* Allocate a working buffer */
    if( !(pBuf = Memory_alloc( NULL, platform_roundup(TESTSIZE, PLATFORM_CACHE_LINE_SIZE), PLATFORM_CACHE_LINE_SIZE, &errorBlock)) )
    {
        platform_write( "Failed temp buffer allocation\n");
        goto leave;
     }
   
    /* Wait for receiver ack. */
    platform_write("Waiting for receiver to get ready.....\n");
    bytes = (int)recvnc( sudp, (void **)&pNcBuf, 0, &hBuffer );

    /* Clean out the buffer */
    recvncfree( hBuffer );
        
    platform_write( "    Sending %d frames of %d bytes \n", PACKETS*1000,TESTSIZE);

    /* Get the start time */
    llEnter();
    ts = llTimerGetTime(&tsMsec);
    llExit();

#ifdef TIMING
    reset_nimu_timing();
#endif

    /* Send out all the packets. */
    for( count = 0; count < PACKETS*1000; count++ )
    {
#ifdef TIMING
    	if (count == 0)  {
            /* record start time */
            benchmark_time_start = CSL_tscRead();
    	}
#endif
        bytes = send(sudp, pBuf, TESTSIZE, 0);
        if( bytes < 0 ) 
        {
            platform_write("Error: Send failed Packets Transmitted: %d Error:%d\n", count, fdError());
			goto leave;
        } 

        /* Account for the number of bytes txed. */                        
        totalBytes = totalBytes + bytes;
#ifdef TIMING
    	if (count == MAX_TIMING_PACKETS)  {
            /* record end time */
            benchmark_time_end = CSL_tscRead();
    	}
#endif
    }

    /* Get the end time */
    llEnter();
    ts1 = llTimerGetTime(&ts1Msec);
    llExit();

    /* Compute total time in milliseconds */
    startMsec  = (ts * 1000) + tsMsec;
    endMsec    = (ts1 * 1000) + ts1Msec;
    tn = endMsec - startMsec;

    /* Print out the Transmit Results. */
    platform_write("------------------------------------------------\n");
    sprintf (response, "Total Data transmitted: %d bytes\n", totalBytes);
    platform_write("Total Data transmitted: %d bytes\n", totalBytes);
    sprintf (tmpresponse, "Start Time            : %d msec\n", startMsec);
    platform_write("Start Time            : %d msec\n", startMsec);
    strcat(response, tmpresponse);
    sprintf (tmpresponse, "End   Time            : %d msec\n", endMsec);
    platform_write("End   Time            : %d msec\n", endMsec);
    strcat(response, tmpresponse);
    sprintf (tmpresponse, "Total Time expired    : %d msec\n", tn);
    platform_write("Total Time expired    : %d msec\n", tn);
    strcat(response, tmpresponse);
    mbps = (((totalBytes/tn)*1000)*8)/1000000;
    if (mbps > 1024) mbps = 1024;			/* we cant be faster than 1 Gig */
    sprintf (tmpresponse, "Transmit Throughput : %u Mb/s\n", mbps);
    platform_write("Transmit Throughput : %u Mb/s\n", mbps);
#ifdef TIMING
    bm_time = (uint32_t) benchmark_time_end - benchmark_time_start;
    platform_write("Total ticks for %d packets were %d \n", MAX_TIMING_PACKETS, bm_time);
    print_nimu_timing();
#endif
    strcat(response, tmpresponse);
    sprintf (tmpresponse, "Note: These numbers are as measured on the EVM and \n");
    platform_write(tmpresponse);
    strcat(response, tmpresponse);
    sprintf (tmpresponse, "reflect how fast it was able to transmit the data. \n");
    platform_write(tmpresponse);
    strcat(response, tmpresponse);

    platform_write("------------------------------------------------\n");

    /* Spin up a task to send th eresults back */
    (void) TaskCreate( sendclient, "SendClient", OS_TASKPRINORM, 0x1400, 0, 0, 0 );
    
leave:

	if( pBuf ) {
		Memory_free(NULL, pBuf, platform_roundup(TESTSIZE, PLATFORM_CACHE_LINE_SIZE));
	}

    if (sudp != INVALID_SOCKET ) {
       fdClose(sudp);
    }

    TaskSleep(2000);
    fdCloseSession( TaskSelf() );
    TaskDestroy( TaskSelf() );

}


/*************************************************************************
 *  @b TCP_perform_receive(void)
 * 
 *  @n
 *  
 * 	TCP Receive benchmark.
 * 
 *  @retval
 *  None
 ************************************************************************/
static void TCP_perform_receive()
{
    struct timeval  to;
    char            *pBuf;
    HANDLE          hBuffer;
    uint8_t			not_first_packet_rx;
    uint32_t        tsMsec, ts1Msec, startMsec, endMsec;
    int32_t         bytes, count, size;
    UINT32          ts, ts1, tn, totalBytes;
    SOCKET  		stcp = INVALID_SOCKET;
    SOCKET  		stcp_child = INVALID_SOCKET;
    struct  sockaddr_in sin1;

    platform_write( "TCP Receive Task started\n");
   
    /* Allocate the file environment for this task */
    fdOpenSession( TaskSelf() );

    /* Create the main TCP listen socket */
    stcp = socket(AF_INET, SOCK_STREAMNC, IPPROTO_TCP);
    if( stcp == INVALID_SOCKET ) 
    {
       platform_write( "Fail socket, %d\n", fdError());
       goto leave;
     }

        /* Set Port = 10001, IP address = IPAddrSend */
        bzero( &sin1, sizeof(struct sockaddr_in) );
        sin1.sin_family = AF_INET;
        sin1.sin_len    = sizeof( sin1 );
        /* sin1.sin_addr.s_addr = inet_addr(LOCAL_IPADDR_STRING); */
        sin1.sin_port   = htons(10001);

        if( bind( stcp, (struct sockaddr *)&sin1, sizeof(sin1) ) < 0 )
        {
            fdClose( stcp);
            stcp = INVALID_SOCKET;
            platform_write("Fail to bind socket, %d\n", fdError());
            goto leave;
        }

       /* If the socket is bound and TCP, start listening */
       if( listen( stcp, 1) < 0 )
       {
            fdClose( stcp );
            platform_write("Fail to listen on socket, %d\n", fdError());
            stcp = INVALID_SOCKET;
            goto leave;
       }

      size = sizeof(sin1);
      bzero( &sin1, sizeof(struct sockaddr_in) );
      stcp_child = accept(stcp, (PSA)&sin1, &size);
     if (stcp_child == INVALID_SOCKET)
      {
            platform_write("Failed accept due to error: %d \n", fdError());
            goto leave;
      }

    /* Configure our socket timeout to be 60 seconds */
    to.tv_sec  = 60;
    to.tv_usec = 0;
    setsockopt( stcp_child, SOL_SOCKET, SO_SNDTIMEO, &to, sizeof( to ) );
    setsockopt( stcp_child, SOL_SOCKET, SO_RCVTIMEO, &to, sizeof( to ) );

    /* Start timer on first packet received */
	not_first_packet_rx = 1;

    /* Accumulate total bytes received */
    totalBytes =0;

#ifdef TIMING
    reset_nimu_timing();
#endif

    for (count = 0; count < PACKETS*1000  ||totalBytes <  PACKETS*1000*1000 ; count++) {
                   
        /* There is data available on the active connection */
        bytes = (int)recvnc( stcp_child, (void **)&pBuf, 0, &hBuffer );

       /* If the connection is closed or got an error, break */
       if (bytes <= 0) {
           break;
       }
       else {
           recvncfree( hBuffer );
       }
       totalBytes+= bytes;

       /* Get start time at first packet received */
       if (not_first_packet_rx){
    	   llEnter();
			ts = llTimerGetTime(&tsMsec);
			llExit();
			not_first_packet_rx = 0;
        }
     }

   /* Get ending timestamp */
   llEnter();
   ts1 = llTimerGetTime(&ts1Msec);
   llExit();

    /* Compute total time in milliseconds */
    startMsec  = (ts * 1000) + tsMsec;
    endMsec    = (ts1 * 1000) + ts1Msec;
    tn = endMsec - startMsec;

    if (totalBytes > 0 )
    {

            /* Print out the Receive Results. */
            /* Must send back the ----- line as the applet keys on it */
            platform_write("------------------------------------------------\n");
            platform_write("Total Data received: %d bytes\n", totalBytes);
            sprintf(response,"Total Data received: %d bytes\n", totalBytes);
            platform_write("Start Time         : %d msec\n", startMsec);
            sprintf(tmpresponse,"Start Time         : %d msec\n", startMsec);
            strcat(response, tmpresponse);
            platform_write("End   Time         : %d msec\n", endMsec);
            sprintf(tmpresponse,"End   Time         : %d msec\n", endMsec);
            strcat(response, tmpresponse);
            platform_write("Total Time expired : %d msec\n", tn);
            sprintf(tmpresponse,"Total Time expired : %d msec\n", tn);
            strcat(response, tmpresponse);
            sprintf(tmpresponse,"Receive Throughput : %u Mb/s\n", (((totalBytes/tn)*1000)*8)/1000000);
            strcat(response, tmpresponse);
            platform_write("Receive Throughput : %u Mb/s \n", (((totalBytes/tn)*1000)*8)/1000000);
#ifdef TIMING
            print_nimu_timing();
#endif
            sprintf (tmpresponse, "Note: These numbers reflect the time it takes the \n");
            strcat(response, tmpresponse);
            platform_write(tmpresponse);
            sprintf (tmpresponse, "packets to be transmitted by the PC and received by\n");
            strcat(response, tmpresponse);
            platform_write(tmpresponse);
            sprintf(tmpresponse, "the EVM.\n");
            platform_write(tmpresponse);
            strcat(response, tmpresponse);
            platform_write("------------------------------------------------\n");
     }
     else
    {
         sprintf(response,"\n\n.....Failed to receive...please re run the test\n\n");
         platform_write("\n\n.....Failed to receive...please re run the test\n\n"); 
     }

     /* Spin up a task to send th eresults back */
     (void) TaskCreate( sendclient, "SendClient", OS_TASKPRINORM, 0x1400, 0, 0, 0 );


leave:
    if( stcp_child != INVALID_SOCKET )
       fdClose( stcp_child );

    if (stcp != INVALID_SOCKET )
       fdClose(stcp);

    TaskSleep(2000);

    fdCloseSession( TaskSelf() );

    TaskDestroy( TaskSelf() );
}

/*************************************************************************
 *  @b TCP_perform_send(void)
 * 
 *  @n
 *  
 * 	TCP Send benchmark.
 * 
 *  @retval
 *  None
 ************************************************************************/
static void TCP_perform_send()
{
    SOCKET  	stcp = INVALID_SOCKET;
    struct  	sockaddr_in sin1;
    int32_t     count,bytes, res;
    uint32_t	mbps;
    uint32_t  	ts, tsMsec, ts1, ts1Msec, startMsec, endMsec;
    uint32_t  	tn, totalBytes;
    char    	*pBuf = NULL;
    Error_Block	errorBlock;


    platform_write( "TCP Transmit Task started\n");
    
   	Error_init(&errorBlock);

    /* Allocate the file environment for this task */
    fdOpenSession( TaskSelf() );

    /* Create the main TCP listen socket */
    stcp = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if( stcp == INVALID_SOCKET ) 
    {
       platform_write( "Failed to create socket, %d\n", fdError());
       goto leave;
     }

        /* Set Port = 10001, IP address = IPAddrSend */
        bzero( &sin1, sizeof(struct sockaddr_in) );
        sin1.sin_family = AF_INET;
        sin1.sin_len    = sizeof( sin1 );
        sin1.sin_addr.s_addr = inet_addr(REMOTE_IPADDR_STRING);
        sin1.sin_port   = htons(10001);

        for ( count = 0; count < 30 ; count ++)
        {
            /* Connect socket */
             res = connect( stcp, (PSA) &sin1, sizeof(sin1) );
             if (res < 0 )
            {
                 TaskSleep(5000);
            }
            else 
                 break;
         }

         if (res < 0 )
         {
            platform_write( "Failed to connect to socket, %d\n", fdError());
            goto leave;
         }

        /* Allocate a working buffer */
        if( !(pBuf = Memory_alloc( NULL, platform_roundup(TESTSIZE, PLATFORM_CACHE_LINE_SIZE), PLATFORM_CACHE_LINE_SIZE, &errorBlock)) )
        {
            platform_write( "Failed buffer allocation\n");
            goto leave;
        }

        platform_write( "    Sending %d frames of %d bytes \n", PACKETS*1000,TESTSIZE);

        totalBytes = 0;

        /* Get timestamp */
        llEnter();
        ts = llTimerGetTime(&tsMsec);
        llExit();

#ifdef TIMING
		reset_nimu_timing();
#endif

        for( count = 0; count < PACKETS*1000; count++ )
        {
            if( ((bytes = send( stcp, pBuf, (int)TESTSIZE, 0 )) < 0) )
            {
                platform_write( "send failed (%d)\n",fdError());
                goto leave;
            }

            totalBytes += bytes;        
        }

    /* Get timestamp again!! */
    llEnter();
    ts1 = llTimerGetTime(&ts1Msec);
    llExit();

    /* Compute total time in milliseconds */
    startMsec  = (ts * 1000) + tsMsec;
    endMsec    = (ts1 * 1000) + ts1Msec;
    tn = endMsec - startMsec;
           
    if (totalBytes > 0 )
    {
	   /* Print out the Transmit Results. */
	    platform_write("------------------------------------------------\n");
	    sprintf (response, "Total Data transmitted: %d bytes\n", totalBytes);
	    platform_write("Total Data transmitted: %d bytes\n", totalBytes);
	    sprintf (tmpresponse, "Start Time            : %d msec\n", startMsec);
	    platform_write("Start Time            : %d msec\n", startMsec);
	    strcat(response, tmpresponse);
	    sprintf (tmpresponse, "End   Time            : %d msec\n", endMsec);
	    platform_write("End   Time            : %d msec\n", endMsec);
	    strcat(response, tmpresponse);
	    sprintf (tmpresponse, "Total Time expired    : %d msec\n", tn);
	    platform_write("Total Time expired    : %d msec\n", tn);
	    strcat(response, tmpresponse);
	    mbps = (((totalBytes/tn)*1000)*8)/1000000;
	    if (mbps > 1024) mbps = 1024;			/* we cant be faster than 1 Gig */
	    sprintf (tmpresponse, "Transmit Throughput : %u Mb/s\n", mbps);
	    platform_write("Transmit Throughput : %u Mb/s\n", mbps);
#ifdef TIMING
        print_nimu_timing();
#endif
	    strcat(response, tmpresponse);
	    sprintf (tmpresponse, "Note: These numbers are as measured on the EVM and \n");
	    platform_write(tmpresponse);
	    strcat(response, tmpresponse);
	    sprintf (tmpresponse, "reflect how fast it was able to transmit the data. \n");
	    platform_write(tmpresponse);
	    strcat(response, tmpresponse);
	    platform_write("------------------------------------------------\n");
    }
     else
    {
         sprintf(response,"\n\n.....Failed to receive...please re run the test\n\n");
         platform_write("\n\n.....Failed to receive...please re run the test\n\n"); 
     }

    /* Spin up a task to send th eresults back */
    (void) TaskCreate( sendclient, "SendClient", OS_TASKPRINORM, 0x1400, 0, 0, 0 );

leave:
     if( pBuf ) {
        Memory_free( NULL, pBuf, platform_roundup(TESTSIZE, PLATFORM_CACHE_LINE_SIZE) );
      }
        
      // We only get here on an error - close the sockets
      if( stcp != INVALID_SOCKET ) {
         fdClose( stcp );
      }
       
    TaskSleep(2000);

    fdCloseSession( TaskSelf() );

    TaskDestroy( TaskSelf() );
}




