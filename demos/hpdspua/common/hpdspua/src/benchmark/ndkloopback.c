/*
 * ndkloopback.c
 *
 * NDK Loopback Application
 *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  ndkloopback.c
 *
 *   @brief
 *      Contains functions for the NDK loopback test.
 *
 */

#include "hpdspua.h"

/* BIOS6 includes to calculate load and handle tasks */
#include <ti/sysbios/utils/Load.h>
#include <ti/bios/include/tsk.h>
#include <ti/sysbios/knl/Clock.h>

#ifndef C665_PLATFORMS
extern uint32_t nimu_getrxdrops(void);
#endif

/* The following code and define is used for code timing measurements */
//#define NIMU_TIMING
#ifdef NIMU_TIMING
extern void print_nimu_timing(void);
extern void reset_nimu_timing(void);
#endif

//#define TIMING
#ifdef TIMING
#include <ti/csl/csl_tsc.h>
#define MAX_TIMING_PACKETS	1000
int64_t	 lb_time_start;
int64_t	 lb_time_end;
#endif

/* Forward references */
static void UDP_perform_receive(void);			/* UDP loopback task 		*/
static void copyUDPTestee_NetworkOpen(void);	/* Starts UDP loopback test */

#ifdef TCP_TX_ONLY
static void TCP_perform_send(void);				/* TCP transmit task		*/
#endif

static void TCP_perform_receive(void);			/* TCP loopback task 		*/
static void copyTCPTestee_NetworkOpen(void);	/* Starts TCP loopback test */

#ifdef TCP_TX_ONLY
/* Used for TCP transmit test */
static int32_t PACKETS  =   5000;
static int32_t TESTSIZE =   1460;
#endif

/* true if we are executing a  loopback */
static uint8_t	ndk_loopback_test;

/* TCP=1 or UDP=0 */
static int32_t loopback_protocol;

/* Loopback port to receive on */
static int32_t rxloopback_port;

/* Loopback port to tx on */
static int32_t txloopback_port;

/* True if we should set a static route for the loopback */
static uint8_t fstatic;

/* Destination mac - only used when setting a static route */
static uint8_t			mac[6];

/* IP Addresses */
static char REMOTE_IPADDR_STRING[17];
static char LOCAL_IPADDR_STRING[17];

/* UDP Socket */
static SOCKET   sudp;

/* TCP Socket */
static SOCKET  	stcp;			/* for receives */
static SOCKET 	lbtcpSocket;	/* for transmits */

/* Packet to send back */
#pragma DATA_ALIGN(lbPkt, 128);
static uint8_t lbPkt[1514];

//#define UDP_TX_ONLY
#define TCP_LOOPBACK

/*************************************************************************
 *  @b ndkLoopbackStart(SOCKET htmlSock, int ContentLength, char *pArgs )
 * 
 *  @n
 *  
 * 	This is the CGI function which will start the NDK loopback benchmark.
 *
 *  @param[in]  
 *  htmlSock - Socket to the browser
 * 
 *  @param[in] 
 *  ContentLength - Length of the page request from the browser
 * 
 *  @param[in] 
 *  pArgs - Not used
 *  
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/
int32_t ndkLoopbackStart(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
    IPN     myIP;
    struct  sockaddr_in Info;
    int32_t InfoLength;
    int32_t	bError;
	char	*pprotocol;
	char	*prxport, *ptxport;
	char	*premip;
	char 	*pstaticroute;
	char 	*pmac;

 
    /*  Process the POST variables to get the test parameters */
    if (!html_processPost(htmlSock,ContentLength)) {
        return 1;
    }

    InfoLength = sizeof(Info);
    getsockname( htmlSock, (PSA)&Info, &InfoLength );
    myIP = Info.sin_addr.s_addr;
    NtIPN2Str( myIP, LOCAL_IPADDR_STRING);

    /* Get the fields from the form */
    premip 	= html_getValueFor("IP");
    pprotocol= html_getValueFor("protocol");
	prxport = html_getValueFor("rxport");
	ptxport = html_getValueFor("txport");
	pstaticroute = html_getValueFor("staticroute");


	/* Start the page */
	html_start("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>");
	html ("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='content-type' content='text/html; charset=utf-8' />");
	html ("<title>High Performance Digital Signal Processor Utility</title><link rel='stylesheet' href='default.css' type='text/css' />");
	html ("<SCRIPT TYPE='text/javascript'>");
	/*html ("<!-- ");*/
	html ("function writesubmit() { ");
	html (" document.lbform.submit(); }");
	/*html (" //--> ");*/
	html ("</SCRIPT></head>");

	html ("<body>");
	html_var (PAGEWRAPPER, "Network Loopback");
	html ("<div id='page'><div id='content'>");

	/* Edit the fields.. set bError if we find one */
	bError = 0;

	if (ndk_loopback_test) {
    	html ("<p>A loopback is already running. You may only do one loopback at a time. </p>");
    	bError = 1;
	}

    if (prxport && (strlen(prxport) > 0)) {
    	rxloopback_port = atoi(prxport);
		if (rxloopback_port < 0 || rxloopback_port > 65535){
	    	html ("<p> You must specify a receive port in the range of 0..65535</p>");
	    	bError = 1;
		}
    }
    else {
    	html ("<p> You must specify a receive port in the range of 0..65535.</p>");
    	bError = 1;
    }

    if (ptxport && (strlen(ptxport) > 0)) {
    	txloopback_port = atoi(ptxport);
		if (rxloopback_port < 0 || rxloopback_port > 65535){
	    	html ("<p> You must specify a transmit port in the range of 0..65535</p>");
	    	bError = 1;
		}
    }
    else {
    	html ("<p> You must specify a transmit port in the range of 0..65535.</p>");
    	bError = 1;
    }

	if (pprotocol && (strlen(pprotocol) > 0)) {
		if (strcmp (pprotocol, "UDP") == 0) {
			 loopback_protocol = 0;
		}
		else
		if (strcmp (pprotocol, "TCP") == 0) {
			 loopback_protocol = 1;
		}
		else {
	    	html ("<p> Protocol must be set to UDP or TCP. </p>");
	    	bError = 1;
		}
	}
	else {
    	html ("<p> You must select a protocol to use. </p>");
    	bError = 1;
	}

	if (premip && (strlen(premip) > 0)) {
		/* validate IP address here */
		strcpy(REMOTE_IPADDR_STRING, premip);
	}
	else {
    	html ("<p> You must specify an IP address to loop the packets back to. </p>");
    	bError = 1;
	}

    if (pstaticroute && (strlen(pstaticroute) > 0)) {
    	fstatic = 1;
		memset (&mac[0], 0, 6);
    	pmac = html_getValueFor("mac0");
    	mac[0] = atoi(pmac);
    	pmac = html_getValueFor("mac1");
    	mac[1] = atoi(pmac);
    	pmac = html_getValueFor("mac2");
    	mac[2] = atoi(pmac);
    	pmac = html_getValueFor("mac3");
    	mac[3] = atoi(pmac);
    	pmac = html_getValueFor("mac4");
    	mac[4] = atoi(pmac);
    	pmac = html_getValueFor("mac5");
    	mac[5] = atoi(pmac);
    	platform_write("Debug: %x:%x:%x:%x:%x:%x \n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    }
    else {
    	fstatic = 0;	/* check boxes are not posted unless they are checked */
    }

    /* If there are no errors, return  a page saying test is starting.
     *  Else return errors inserted above
     */
	if (bError == 0) {
		html ("<div style='padding: 2px;'>");
		html_var ("<p> %s loopback started on port %d. Packets will be sent to %s, port %d.</p>", pprotocol, rxloopback_port, premip, txloopback_port);
		if (fstatic){
			html ("<p> You are using a static route </p>");
			html_var ("<p> Destination MAC is: %x:%x:%x:%x:%x:%x</p>", mac[0], mac[1],mac[2],mac[3],mac[4],mac[5] );
		}
		html ("<form name='lbform' action='lbstop.cgi' method='post'>");
		html ("<input type='button' value='Stop loopback' onClick='writesubmit();'>");
		html ("</form>");
		html ("<p>&nbsp</p>");
	}
	
	/* end page and content divs */
	html ("</div><div style='clear: both;'></div></div>");

	html (PAGEFOOTER);

	html_end ("</body></html>");

  
    // Send header
    httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);
    // After this call we MUST send the data since a CRLF is being sent
    httpSendEntityLength(htmlSock, html_getsize() );
    // Send the page
    httpSendClientStr(htmlSock, html_getpage());


    /* Start loopback if protocol is UDP and there were no validation errors */
	if ( (loopback_protocol == 0) && (bError == 0)) {
		platform_write ("%s loopback started on port %d. Packets will be sent to %s, port %d. \n", pprotocol, rxloopback_port, premip, txloopback_port);
		if (fstatic){
			platform_write ("You are using a static route.\n");
			platform_write ("Destination MAC is: %x:%x:%x:%x:%x:%x. \n", mac[0], mac[1],mac[2],mac[3],mac[4],mac[5]);
		}

		copyUDPTestee_NetworkOpen();
	}

	if ( (loopback_protocol == 1) && (bError == 0)) {
		platform_write ("%s loopback started on port %d. Packets will be sent to %s, port %d. \n", pprotocol, rxloopback_port, premip, txloopback_port);
		if (fstatic){
			platform_write ("You are using a static route.\n");
			platform_write ("Destination MAC is: %x:%x:%x:%x:%x:%x. \n", mac[0], mac[1],mac[2],mac[3],mac[4],mac[5]);
		}

		copyTCPTestee_NetworkOpen();
	}

    return 0;

}

/*************************************************************************
 *  @b ndkLoopbackStop(SOCKET htmlSock, int ContentLength, char *pArgs )
 *
 *  @n
 *
 * 	This is the CGI function which will stop the NDK loopback benchmark.
 *
 *  @param[in]
 *  htmlSock - Socket to the browser
 *
 *  @param[in]
 *  ContentLength - Length of the page request from the browser
 *
 *  @param[in]
 *  pArgs - Not used
 *
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/
int32_t ndkLoopbackStop(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
    /*  Process the POST */
    if (!html_processPost(htmlSock,ContentLength)) {
        return 1;
    }

	/* Start the page */
    html_start(PAGEHEAD);

	html ("<body>");
	html_var (PAGEWRAPPER, "Network Loopback Terminate");
	html ("<div id='page'><div id='content'>");

	html ("<p>Loopback is being terminated. Results written to the console. </p>");

	/* end page and content divs */
	html ("<div style='clear: both;'></div></div>");

	html (PAGEFOOTER);

	html_end ("</body></html>");

    /* Send header */
    httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);
    /* After this call we MUST send the data since a CRLF is being sent */
    httpSendEntityLength(htmlSock, html_getsize() );
    /* Send the page */
    httpSendClientStr(htmlSock, html_getpage());


    /*
     * Stops the loopback. The loopbacks are camped on the socket with no timeout
     * so close the socket on them to pop off.
     *
     */
	platform_write ("Closing the socket ....\n");
	ndk_loopback_test = 0;
    if (loopback_protocol) {
    	/* TCP */
        if( stcp != INVALID_SOCKET )
        	fdClose(stcp);
        if( lbtcpSocket != INVALID_SOCKET )
        	fdClose(lbtcpSocket);
    }
    else {
    	/* UDP */
        if( sudp != INVALID_SOCKET )
        	fdClose(sudp);
    }

    return 0;

}


/*************************************************************************
 *  @b copyxxOpen(SOCKET htmlSock, int ContentLength, char *pArgs )
 *
 *  @n
 * 	Spins up the task to handle receiving or transmitting the test packets.
 *
 *  @retval
 *  None
 ************************************************************************/
static void copyUDPTestee_NetworkOpen()
{

    /* Create the UDP Receive Performance Task. */
    (void) TaskCreate( UDP_perform_receive, "UDPLoopback", OS_TASKPRIHIGH, 0x1400, 0, 0, 0 );

}

static void copyTCPTestee_NetworkOpen()
{
    /* Create the TCP Receive Performance Task. */
    (void) TaskCreate( TCP_perform_receive, "TCPBenchmarkRX", OS_TASKPRIHIGH, 0x1400, 0, 0, 0 );

}


/*************************************************************************
 *  @b UDP_perform_receive(void)
 *
 *  @n
 *
 * 	UDP Receive benchmark.
 *
 *  When running UDP test, pay attention to Smartbits receive rates. Smartbits
 *  receive counter may fool the user with received packet counts. Smartbits
 *  does not differentiate between UDP and ICMP packets. It just receives and adds
 *  up the number.
 *
 *  @retval
 *  None
 ************************************************************************/

static void UDP_perform_receive(void)
{
	struct sockaddr_in sin1;
	struct sockaddr_in to;
	char*           pBuf;
	HANDLE          hBuffer;
	int32_t			len = 0;
	int32_t        	bytes;
#ifdef UDP_TX_ONLY
	int32_t			i;
#endif
	uint32_t		socket_error;
	uint32_t		num_pkts_looped;
#ifdef TIMING
    uint32_t		lb_time;
#endif

    sudp = INVALID_SOCKET;

    num_pkts_looped = 0;

    /* this is test */
   	platform_write( "UDP Loopback Task started\n");

    /* Allocate the file environment for this task */
   	fdOpenSession( TaskSelf() );

    /* Create the main UDP listen socket */
    sudp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if( sudp == INVALID_SOCKET ) {
        platform_write( "Failed to create the receive socket \n");
        goto leave;
    }

    /* Set Port = 5678, leaving IP address = Any */
    bzero( &sin1, sizeof(struct sockaddr_in) );
    sin1.sin_family = AF_INET;
    sin1.sin_len    = sizeof( sin1 );
    sin1.sin_port   = htons(rxloopback_port);

    /* Bind the socket */
    if ( bind( sudp, (PSA) &sin1, sizeof(sin1) ) < 0 ) {
        platform_write( "Failed to bind the receive socket \n");
    	goto leave;
    }

    /* prepare address to send packet out back to SB */
    memset ((void *)&to, 0, sizeof(to));
    to.sin_family      = AF_INET;
    to.sin_addr.s_addr = inet_addr(REMOTE_IPADDR_STRING);
    to.sin_port        = htons(txloopback_port);

    if (fstatic) {
    	if (LLIAddStaticEntry( inet_addr(REMOTE_IPADDR_STRING), (uint8_t *)&mac[0] ) == -1) {
    		platform_write("Failed to add static route. \n");
    		platform_write("A possible cause of why this failed is that static routes must be on the same subnet (i.e. subnet mask 255.255.255.0).\n");
    		platform_write("Therefore destination IP and the packet generator IP must be on the same subnet.\n");
    		goto leave;
    	}
    }

    /* Wait for all the data to arrive. This check ensures that we will not come out of the
     * loop at all even if there is a single packet drop. */
    ndk_loopback_test = 1;

#ifdef TIMING
    lb_time_start = 0; lb_time_end = 0;
#endif

#ifdef NIMU_TIMING
 	reset_nimu_timing();
#endif

 	len = sizeof (to);
    while (ndk_loopback_test) {

    	bytes = (int)recvncfrom( sudp, (void **)&pBuf, MSG_WAITALL, (struct sockaddr *)&to, &len, &hBuffer );

    	/* we should only come off the socket on a timeout or error */
     	if (bytes < 0) {
     		socket_error = fdError();
     		if (socket_error == EHOSTDOWN) {
     			platform_write("Socket reporting host is down. Set a static route and try again. \n");
     			fdClose(sudp);
     		}
     		else
     		if (socket_error == EWOULDBLOCK){
     	  		platform_write("Receive timeout expired with nothing received.\n");
     	  		continue;
     		}
     		else
     		if (socket_error == EBADF) {
     			platform_write("Socket was closed. \n");
     			/* assume it was closed by the stop command */
     		}
     		else {
     	  		platform_write("Socket error = %d.\n", socket_error);
     	  		fdClose(sudp);
     		}
      	    break; /* Were toast */
    	}

    	/* copy data to tx buffer */
    	 memcpy(&lbPkt[0], pBuf, bytes);

    	 /* Clean out the buffer */
    	 recvncfree( hBuffer );

    	 /* Keep a count of what we have processed */
    	 num_pkts_looped = num_pkts_looped + 1;


#ifdef UDP_TX_ONLY
    	 for(i=0; i<1500; i++)
    		 lbPkt[i]= 'a';

    	 for( i = 0; i < 1000000000; i++ ){
    		 /* send received packet out on the same socket and same port */
    		 if (num_pkts_looped == 0) {
    		      /* record start time */
    		      lb_time_start = CSL_tscRead();
    		 }
    		 if (sendto( sudp, lbPkt, 1403, 0, (struct sockaddr *)&to, sizeof(to)) < 0) {
    			 platform_write("UDP loopback: send failed (socket error = %d)\n",fdError());
    			 fdClose(sudp);
    			 goto leave;
    		 }

    	 	 /* Keep a count of what we have processed */
    	 	 num_pkts_looped = num_pkts_looped + 1;
    	}


    	break;

#endif

    	if (sendto( sudp, lbPkt, bytes, 0, (struct sockaddr *)&to, sizeof(to)) < 0) {
    	    platform_write("UDP loopback: send failed (socket error = %d)\n",fdError());
    	    fdClose(sudp);
    	    goto leave;
    	}

#ifdef TIMING
    	if (num_pkts_looped == MAX_TIMING_PACKETS){
    		/* record end time */
    		lb_time_end = CSL_tscRead();
    		break;
    	}
#endif

    }

    platform_write ("Processed %u packets \n", num_pkts_looped);

#ifndef C665_PLATFORMS
    platform_write ("NIMU dropped %u packets due to lack of NDK buffers.\n", nimu_getrxdrops());
#endif

#ifdef TIMING
    if (lb_time_end != 0) {
    	lb_time =  (uint32_t )    (lb_time_end - lb_time_start);

    	platform_write("Total ticks for %d packets were %d \n", num_pkts_looped, lb_time);
    }
#endif

#ifdef NIMU_TIMING
   print_nimu_timing();
#endif

leave:

    if (fstatic) {
    	LLIRemoveStaticEntry( inet_addr(REMOTE_IPADDR_STRING) );
    }

    TaskSleep(2000);

    fdCloseSession( TaskSelf() );

	platform_write("Loopback terminated. \n");

	/* No return from a task destroy ... */
    TaskDestroy( TaskSelf() );

	return;
}


/*************************************************************************
 *  @b TCP_perform_receive(void)
 *
 *  @n
 *
 * 	TCP Receive benchmark.
 *
 *  @retval
 *  None
 ************************************************************************/
static void TCP_perform_receive()
{

#ifdef TCP_TX_ONLY
	/* This block is for TCP transmit only test */
	TCP_perform_send();
	return;

#else
	/* This is for the normal loopback test we do */
    char            *pBuf;
    int32_t         bytes, size;
    SOCKET  		stcp_child = INVALID_SOCKET;
    struct  		sockaddr_in sin1;
    uint32_t		count;
    int32_t			res;
    uint32_t		socket_error;
	unsigned long	num_pkts_looped;
	Error_Block		errorBlock;

    lbtcpSocket = stcp = INVALID_SOCKET;

    num_pkts_looped = 0;

    /* Allocate a working buffer */
    if( !(pBuf = Memory_alloc( NULL, platform_roundup(2000, PLATFORM_CACHE_LINE_SIZE), PLATFORM_CACHE_LINE_SIZE, &errorBlock)) )
    {
    	platform_write( "Failed buffer allocation\n");
        goto leave;
    }

    platform_write( "TCP Receive Task started\n");

    /* Allocate the file environment for this task */
    fdOpenSession( TaskSelf() );

    /* Create the main TCP listen socket */
    stcp = socket(AF_INET, SOCK_STREAMNC, IPPROTO_TCP);
    if( stcp == INVALID_SOCKET )
    {
       platform_write( "Fail socket, %d\n", fdError());
       goto leave;
    }
    platform_write( "TCP socket created\n");


    /* Set Port IP address = IPAddrSend */
    bzero( &sin1, sizeof(struct sockaddr_in) );
    sin1.sin_family = AF_INET;
    sin1.sin_len    = sizeof( sin1 );
    sin1.sin_port   = htons(rxloopback_port);

    if( bind( stcp, (struct sockaddr *)&sin1, sizeof(sin1) ) < 0 )
        {
    	fdClose( stcp);
        stcp = INVALID_SOCKET;
        platform_write("Fail to bind socket, %d\n", fdError());
        goto leave;
    }

    platform_write( "TCP socket bound\n");

    /* If the socket is bound and TCP, start listening */
    if( listen( stcp, 1) < 0 )
    {
    	fdClose( stcp );
        platform_write("Fail to listen on socket, %d\n", fdError());
        stcp = INVALID_SOCKET;
        goto leave;
    }

    platform_write( "TCP socket is listening\n");

    size = sizeof(sin1);
    bzero( &sin1, sizeof(struct sockaddr_in) );

    stcp_child = accept(stcp, (PSA)&sin1, &size);

    if (stcp_child == INVALID_SOCKET){
    	platform_write("Failed accept due to error: %d \n", fdError());
        goto leave;
    }

    platform_write( "TCP socket accepted\n");


    if (fstatic) {
    	if (LLIAddStaticEntry( inet_addr(REMOTE_IPADDR_STRING), (uint8_t *)&mac[0] ) == -1) {
    		platform_write("Failed to add static route. \n");
    		platform_write("A possible cause of why this failed is that static routes must be on the same subnet (i.e. subnet mask 255.255.255.0).\n");
    		platform_write("Therefore destination IP and the packet generator IP must be on the same subnet.\n");
    		goto leave;
    	}
    }

    /* Create loop back socket */
    lbtcpSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(lbtcpSocket  == INVALID_SOCKET ) {
    	platform_write( "Fail create loopback socket, %d\n", fdError());
        goto leave;
    }
    platform_write( "TCP loopback socket created\n");

    /* Set Port = 5001, IP address = IPAddrSend */
    bzero( &sin1, sizeof(struct sockaddr_in) );
    sin1.sin_family = AF_INET;
    sin1.sin_len    = sizeof( sin1 );
    sin1.sin_addr.s_addr = inet_addr(REMOTE_IPADDR_STRING);
    sin1.sin_port   = htons(5001);

    /* connect back*/
    TaskSleep(2000);
    for ( count = 0; count < 30 ; count ++){
    	/* Connect socket */
        res = connect( lbtcpSocket, (PSA) &sin1, sizeof(sin1) );
        if (res < 0 )
        	TaskSleep(5000);
        else
        	break;
    }

    if (count == 30 ){
    	platform_write( "Failed to connect to socket, %d\n", fdError());
        goto leave;
    }
    platform_write( "socket loop back connected \n");

    /* Wait for all the data to arrive. This check ensures that we will not come out of the
     * loop at all even if there is a single packet drop. */
    ndk_loopback_test = 1;

    platform_write( "TCP initialized\n");

    while (ndk_loopback_test) {

     	bytes = (int)recv( stcp_child, (void *) lbPkt, 1460, 0);

    	/* we should only come off the socket on a timeout or error */
     	if (bytes < 0) {
     		socket_error = fdError();
     		if (socket_error == EHOSTDOWN) {
     			platform_write("Socket reporting host is down. Set a static route and try again. \n");
     			goto leave;
     		}
     		else
     		if (socket_error == EWOULDBLOCK){
     	  		continue;
     		}
     		else {
     	  		platform_write("Socket error = %d. Loopback terminating ...\n", socket_error);
     	  		goto leave;
     		}
    	}

     	/* send received packet out on the same child socket */
     	if (send( lbtcpSocket, lbPkt, bytes, 0) != bytes) {
     		platform_write("TCP loopback: send failed (%d)\n",fdError());
     		goto leave;
     	}

     	num_pkts_looped += bytes; /* track the amount of data we loop back for TCP */
    }


leave:


    platform_write ("Loop backed %u bytes.\n", num_pkts_looped);

#ifndef C665_PLATFORMS
    platform_write ("NIMU dropped %u packets due to lack of NDK buffers.\n", nimu_getrxdrops());
#endif

    if( pBuf ) {
            Memory_free( NULL, pBuf, platform_roundup(2000, PLATFORM_CACHE_LINE_SIZE) );
    }

    /* child socket */
    if( stcp_child != INVALID_SOCKET )
       fdClose( stcp_child );

    /* loopback socket */
    if( lbtcpSocket != INVALID_SOCKET )
           fdClose( lbtcpSocket );

    if (stcp != INVALID_SOCKET )
       fdClose(stcp);

    TaskSleep(2000);

    fdCloseSession( TaskSelf() );

    TaskDestroy( TaskSelf() );

#endif
}


/*************************************************************************
 *  @b TCP_perform_send(void)
 *
 *  @n
 *
 * 	TCP Send benchmark.
 *
 *  @retval
 *  None
 ************************************************************************/
#ifdef TCP_TX_ONLY
static void TCP_perform_send()
{
    struct  	sockaddr_in sin1;
    int32_t     count, res;
    char    	*pBuf = NULL;
    Error_Block	errorBlock;

    stcp = lbtcpSocket = INVALID_SOCKET;

    platform_write( "TCP Transmit Task started\n");

   	Error_init(&errorBlock);

    /* Allocate the file environment for this task */
    fdOpenSession( TaskSelf() );

    platform_write( "task env created \n");
    /* Create the main TCP listen socket */
    stcp = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if( stcp == INVALID_SOCKET )
    {
       platform_write( "Failed to create socket, %d\n", fdError());
       goto leave;
     }

    platform_write( "socket created. attempt to connect \n");

        /* Set Port = 10001, IP address = IPAddrSend */
        bzero( &sin1, sizeof(struct sockaddr_in) );
        sin1.sin_family = AF_INET;
        sin1.sin_len    = sizeof( sin1 );
        sin1.sin_addr.s_addr = inet_addr(REMOTE_IPADDR_STRING);
        sin1.sin_port   = htons(5001);

        for ( count = 0; count < 30 ; count ++)
        {
            /* Connect socket */
             res = connect( stcp, (PSA) &sin1, sizeof(sin1) );
             if (res < 0 )
            {
                 TaskSleep(60);
                 platform_write( "attempt to connect %d\n", count);
            }
            else
                 break;
         }

         if (count == 30 )
         {
            platform_write( "Failed to connect to socket, %d\n", fdError());
            goto leave;
         }

        /* Allocate a working buffer */
        if( !(pBuf = Memory_alloc( NULL, platform_roundup(TESTSIZE, PLATFORM_CACHE_LINE_SIZE), PLATFORM_CACHE_LINE_SIZE, &errorBlock)) )
        {
            platform_write( "Failed buffer allocation\n");
            goto leave;
        }

        platform_write( "    Sending %d frames of %d bytes \n", PACKETS*1000,TESTSIZE);

        for( count = 0; count < PACKETS*2000; count++ )
        {
            if( ((send( stcp, pBuf, (int)TESTSIZE, 0 )) < 0) )
            {
                platform_write( "send failed (%d)\n",fdError());
                goto leave;
            }
        }

        goto leave;

leave:
     if( pBuf ) {
        Memory_free( NULL, pBuf, platform_roundup(TESTSIZE, PLATFORM_CACHE_LINE_SIZE) );
      }

      /* We only get here on an error - close the sockets */
      if( stcp != INVALID_SOCKET ) {
         fdClose( stcp );
      }

    TaskSleep(2000);

    fdCloseSession( TaskSelf() );

    TaskDestroy( TaskSelf() );
}
#endif
















