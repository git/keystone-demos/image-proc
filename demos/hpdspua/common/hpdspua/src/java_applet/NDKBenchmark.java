/*
 * Copyright (C) 2009-2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
package ndkbenchmark;

import java.io.*;
import java.net.*;
import java.util.StringTokenizer;
import java.applet.*;
import java.awt.Font;
import java.awt.Graphics;



public class NDKBenchmark extends Applet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	float time;
	String sResponse;

	/*
	 * Control variables
	 * */
	String ndkBenchMarkTest;
	int packetSize, packetNum;
	String testeeIP;
	static int packetDir, packetProto;
	static boolean waiting;
	Thread tester;
	SendRecvThread sthread;
	ReceiverThread rthread;
	SendRecvTCPThread stthread;
	Font bodyFont;
	Graphics myg;

	
	public void init() {
		waiting = true;
		bodyFont = new Font ("Courier", Font.PLAIN, 18);
		execute();
	}

	public void paint(Graphics g) {	
     int y = 40;
     g.setFont(bodyFont);
     myg = getGraphics();
     if (waiting)
     {
    	 g.drawString(sResponse, 20, y);
     }
     else
     {
    	 StringTokenizer st = new StringTokenizer(sResponse, "\n");
    	while(st.hasMoreTokens()){
    	  y = y + 30;
    	  String s=st.nextToken();
    	  g.drawString(s, 20, y);
    	  }

     }
	}


	public void execute()
	{
		int pktSize;
		String sendIPAddr, p, dir, proto;
		p = this.getParameter("packets");
		pktSize = Integer.parseInt(p.trim());
		dir = this.getParameter("direction");
		packetDir = Integer.parseInt(dir.trim());
		sendIPAddr = this.getParameter("ipaddress");
		proto = this.getParameter("protocol");
		packetProto = Integer.parseInt(proto.trim());
		try {
			if ( NDKBenchmark.packetProto == 0) {
				tester = new UDPTester(pktSize, sendIPAddr);
				tester.start();
			}
			else
			{
				tester = new TCPTester(pktSize, sendIPAddr);
				tester.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	class TCPTester extends Thread {
		Socket stcp;
		int pktSize, pktDir;
		String sendIPAddr;
		
		public TCPTester(int packetSize, String sendIPAddr) throws SocketException {
			this.pktSize = packetSize;
			this.sendIPAddr = sendIPAddr;
		}
		
	    public void run() {
	    	try {
	    		
	    		rthread = new ReceiverThread(7000);
				rthread.start();
				stthread = new SendRecvTCPThread(InetAddress.getByName(sendIPAddr),10001,pktSize);
				stthread.start();
                return;
	    		
	    	}
	    	catch(Exception e) {
				e.printStackTrace();
			} 
	    }
	}
	
	class UDPTester extends Thread {
		DatagramSocket socket;
		int pktSize, pktDir;
		String sendIPAddr;

		public UDPTester(int packetSize, String sendIPAddr) throws SocketException {
			this.pktSize = packetSize;
			this.sendIPAddr = sendIPAddr;
		}

		public void run() {
			try { 
				rthread = new ReceiverThread(7000);
				rthread.start();
				sthread = new SendRecvThread(InetAddress.getByName(sendIPAddr),10001,pktSize);
				sthread.start();
                return;
			} 
			catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
	}
	class SendRecvTCPThread extends Thread {

		public InetAddress server;
		public boolean stopped = false;
		public int port,size, i;
		
		public SendRecvTCPThread(InetAddress address, int port, int size) throws SocketException {
			this.server = address;
			this.port = port;
			this.size = size;
			
		}
		public void halt() {
			this.stopped = true;
		}

		public void run() {

			try {
					if(NDKBenchmark.packetDir == 1) /* Sender */
					{
						byte[] data = new byte[1500];
						int i = 0;
						sResponse = "Sending TCP test packets...";
						paint(myg);
						Socket stcp = new Socket(server,port);
						OutputStream outstream = stcp.getOutputStream();
						DataOutputStream osr = new DataOutputStream(outstream);
						while (NDKBenchmark.waiting == true && i < size*1000) // We need to be infinite loop and wait for the receiver to terminate this. 
						{
							osr.write(data,0,1000);
							i = i + 1;
						}
						stcp.close();
					} else {  /* we are the receiver */
						byte[] data = new byte[1000];
						ServerSocket stcp = new ServerSocket(port);
						Socket stcps = stcp.accept();
						InputStream instream = stcps.getInputStream();
						DataInputStream isr = new DataInputStream(instream);
						sResponse ="Receiving TCP test packets...";
						paint(myg);
						while(NDKBenchmark.waiting == true)
						{
							isr.read(data);
						}
						stcp.close();
						stcps.close();
					}
				}
				catch (IOException ex) {
					System.err.println(ex);
			    }
			}
	}
	
	class SendRecvThread extends Thread {

		public InetAddress server;
		public DatagramSocket socket;
		public boolean stopped = false;
		public int port,size;
		
		public SendRecvThread(InetAddress address, int port, int size) throws SocketException {
			this.server = address;
			this.port = port;
			this.socket = new DatagramSocket(10001);
			this.size = size;
			
		}

		public void halt() {
			this.stopped = true;
		}

		public DatagramSocket getSocket() {
			return this.socket;
		}

		public void run() {

			try {
					if(NDKBenchmark.packetDir == 1)
					{
						byte[] data = new byte[1000];
						sResponse ="Sending UDP test packets...";
						paint(myg);
						DatagramPacket output = new DatagramPacket(data, data.length, server, port);
						while (NDKBenchmark.waiting == true) // We need to be infinite loop and wait for the receiver to terminate this. 
						{
							socket.send(output);
						}
						//Thread.yield();
					} else {  /* we are the receiver */
						byte[] start = new byte[5];
						sResponse ="Receiving UDP test packets...";
						paint(myg);
						DatagramPacket output = new DatagramPacket(start, start.length, server, port);
						socket.send(output);
						byte[] tmp = new byte[1000];
						DatagramPacket dp = new DatagramPacket(tmp, tmp.length);
						while(NDKBenchmark.waiting == true)
						{
							socket.receive(dp);
						}
					}
				}
				catch (IOException ex) {
					System.err.println(ex);
			    }
			}
	}
    // Receives the test results
	class ReceiverThread extends Thread {
		DatagramSocket socket;
		public int port;
		private boolean stopped = false;

		public ReceiverThread(int port) throws SocketException {
			this.socket = new DatagramSocket(port);
			//this.socket.connect(server, port);
		}

		public void halt() {
			this.stopped = true;
		}

		public void run() {
			byte[] buffer = new byte[65507];
			while (true) {
				if (stopped)
					return;
				DatagramPacket dp = new DatagramPacket(buffer, buffer.length);
				try {
					//println("Trying receive");
					socket.receive(dp);
					sResponse = new String(dp.getData(), 0, dp.getLength());
					waiting = false;			
					paint(myg);
				} catch (IOException ex) {
					System.err.println(ex);
				}
			}
		}
	}

}
