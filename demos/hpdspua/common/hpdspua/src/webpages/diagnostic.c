 /*
 * Diagnostic.c
 *
 * CGI function to handle the POST from the diagnostic web page
 *
 * Copyright (C) 2010-2011 Texas Instruments Incorporated - http://www.ti.com/
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  diagnostic.c
 *
 *   @brief   
 *      Contains CGI functions to process a post from the Diagnostics Page.
 *
 */
 
#include "hpdspua.h"

/*************************************************************************
 *  @b processDiagnosticRequest(SOCKET htmlSock, int ContentLength, char *pArgs )
 * 
 *  @n
 *  
 *  This function the POST from the Diagnsotics page. It will run the test 
 *  specified and display the result.
 * 
 *  @param[in]  
 *  htmlSock - Socket to the browser
 * 
 *  @param[in] 
 *  ContentLength - Length of the page request from the browser
 * 
 *  @param[in] 
 *  pArgs - Not used
 *  
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/
 
int32_t processDiagnosticRequest(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
	int32_t	rc;
	uint8_t	*pch;
	int32_t	intval, i;
	char 	*test_value;
	char 	*value;

	/*  Process the POST variables. */
	if (!html_processPost(htmlSock,ContentLength)) {
		return 1;
	}

	/* Get the test to run */
	test_value = html_getValueFor("test");

	if (test_value == NULL) {
		http405(htmlSock); /* Tell the client we couldn't process it */
		platform_write( "CGI expected a field called test\n");
		return 1;
	}

	/* Output the page */
	html_start(PAGEHEAD);

	html ("<body>");

	html_var (PAGEWRAPPERBACK, "Diagnostic Result");

	html ("<div id='page'>");

	html ("<div id='content'>");

	/* Page Header */
	html (MAINHDR);

	/* Based on the form posted run the test and return the results */
	if (strcmp(test_value, "ramtest") == 0) {
		/* add paltform ram test call here */
		rc = platform_external_memory_test((uint32_t) &gRxBuffer[0], (uint32_t) &gRxBuffer[(0x400000 - sizeof (uint32_t))]);

		if (rc) {
			html ("Partial RAM test failed");
		}
		else {
			html ("Partial RAM test passed");
		}
	}
	else
	if (strcmp(test_value, "intramtest") == 0) {
		value = html_getValueFor("core");
		intval = atoi(value);
		if ((intval < 1) || (intval > gPlatformInfo.cpu.core_count)) {
			html_var("The core to test must be between 1 and %d", gPlatformInfo.cpu.core_count);
		}
		else {
			rc = platform_internal_memory_test(intval - 1);	
			if (rc == Platform_EUNSUPPORTED) {
				html_var("Core %d id, RAM test failed (unsupported in core id)", intval-1);
			} else if (rc == Platform_EFAIL) {
				html_var("Core %d RAM test failed", intval-1);
			}
			else {
				html_var("Core %d RAM test was successful", intval-1);
			}		
		}
	}
	else
	if (strcmp(test_value, "uarttest") == 0) {
		html ("<h2>Wrote the following string to the UART: </h2><br>");
		value = html_getValueFor("echotext");
		if (value) {
			pch = (Uint8 *)value;
			while (*pch != NULL){
				(void) platform_uart_write(*pch);
				pch++;
			}
			/* Send LF/CR */
			(void) platform_uart_write((Uint8)0x0D);
			(void) platform_uart_write((Uint8)0x0A);
			html(value);
		}
		else {
			html("You must specify some text to echo");
		}	
	}
	else
	if (strcmp(test_value, "ledtest") == 0) {
		value = html_getValueFor("leds");
		intval = atoi(value); 

		html_var("Flashed LED %d ", (intval + 1));

		/* flash for about a second or so */
		for (i=0; i < 10; i++) {
			(void) platform_led(intval, PLATFORM_LED_ON, PLATFORM_USER_LED_CLASS);
			(void) platform_delay(50000);
			(void) platform_led(intval, PLATFORM_LED_OFF, PLATFORM_USER_LED_CLASS);
			(void) platform_delay(50000);
		}
	}
	else {
		html("<h2>I do not recognize the test to run</h2>");
	}

	html ("<br><br></div><div style='clear: both;'>&nbsp;</div></div>");  

	html (PAGEFOOTER);

	html_end ("</body></html>");

	/* Send header */
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    /* After this call we MUST send the data since a CRLF is being sent */
	httpSendEntityLength(htmlSock, html_getsize() );

	/* Send the page */
	httpSendClientStr(htmlSock, html_getpage());


	return 1;	
}

