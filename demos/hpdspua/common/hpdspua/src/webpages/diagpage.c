 /*
 * diagpage.c
 *
 * CGI function to display the diagnostic web page
 *
 * Copyright (C) 2010-2011 Texas Instruments Incorporated - http://www.ti.com/
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  diagpage.c
 *
 *   @brief   
 *      Contains CGI functions to display the diagnsotics web page.
 *
 */
 
#include "hpdspua.h"

/*************************************************************************
 *  @b serveDiagnosticPage(SOCKET htmlSock, int ContentLength, char *pArgs )
 * 
 *  @n
 *  
 * This function displays the diagnostic page. We build the options on the 
 * page dynmaically based on the configuration of the platorm (i.e. the 
 * numbers of LEDs and so forth).
 * 
 *  @param[in]  
 *  htmlSock - Socket to the browser
 * 
 *  @param[in] 
 *  ContentLength - Length of the page request from the browser
 * 
 *  @param[in] 
 *  pArgs - Not used
 *  
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/
int32_t serveDiagnosticPage(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
	int32_t	i;

	html_start(PAGEHEAD);

	html ("<body>");

	html_var (PAGEWRAPPER, "Platform Diagnostics");

	html ("<div id='page'>");

	html ("<div id='content'>");

	/* Page Header */
	html ("<div id='page'>");
	html ("<div id='content'>");
	html ("<table width='100%'>");
	html ("<tr><td><hr>");

	html ("<table cellpadding='6'><tr><td valign='center'>");

	html ("<image src='dspchip.gif'>");
	html ("</td><td width='100%' align='justify'>");

	html ("<h2>Run board level diagnostics and access the console port on the Platform</h2><br>");

	html ("<p class='howitworks'><b>How it works:</b> There are four tests you can run from this page. Each test issues a POST request to a CGI backend service. In turn, these backend services use the SDK platform library to execute the diagnostic.</p>");
	html ("</td></tr></table><hr><br>");
	html ("<table width='100%' cellpadding='10' style='border-style:solid;border-width:1px;border-collapse:collapse'>");
	html ("<tr>");

	html ("<td width='50%' style='border-style:solid;border-width:1px;border-collapse:collapse' align='left' valign='top'>");
	html ("<form action='diagresult.cgi' method='post'>");
	html ("<b>Test External RAM</b><p>&nbsp;</p>");
	html ("<p class='howitworks'><b>How it works:</b> The DDR memory test writes a series of patterns into external RAM and then reads them back, verifying. In this particular case we are  testing the memory area used by the HTTP Server for storing an uploaded  file.</p>");
	html ("<p><input type='hidden' name='test' value='ramtest'/> <input type='submit' value='Execute RAM Test'/></p>");
	html ("</form>");
	html ("</td>");


	/* Diagnostic Tests */
	html ("<td width='50%' style='border-style:solid;border-width:1px;border-collapse:collapse' align='left' valign='top'>");
	html ("<form action='diagresult.cgi' method='post'>");
	html ("<b>Test a Processor&#39;s internal memory</b><p>&nbsp;</p>");
	html ("<p class='howitworks'><b>How it works:</b> The internal memory test writes a series of patterns into the internal RAM for a specific Core and then reads them back, verifying. </p>");

	/* Only display internal core tests for the number of cores on the platform.. if its just one then the test is not available */
	if (gPlatformInfo.cpu.core_count < 2) {
		html ("The internal memory test can not be run on a single core platform while this application is executing.");
	}
	else {
		html ("<p>Select a core and then press execute. Core 0 is unavailable since you are executing on it.</p>");

		html ("<fieldset>");
		/* we don't show core 1 (ie 0) since we are executing on it */
		for (i=2; i <= gPlatformInfo.cpu.core_count; i++) {
			if (i == 2) {
				html_var ("<input type='radio' checked name='core' value='%d'> %d&nbsp;&nbsp;", i, i-1); /* Default selection */
			}
			else {
				html_var ("<input type='radio' name='core' value='%d'> %d&nbsp;&nbsp;", i, i-1);
			}
		}
		html ("</fieldset>");
		html ("<p>&nbsp;</p>");
		html ("<input type='hidden' name='test' value='intramtest'/>");
		html ("<p><input type='submit' value='Execute Internal RAM Test'/></p>");
	}
	html ("</form>");
	html ("</td></tr>");


	html ("<tr><td width='50%' style='border-style:solid;border-width:1px;border-collapse:collapse' align='left' valign='top'>");
	html ("<form action='diagresult.cgi' method='post'>");
	html ("<p><b>Flash an LED</b></p>");
	html ("<p class='howitworks'><b>How it works:</b>  This test verifies that an LED is operational by flashing it (ON/OFF) for approximately one second.</p>");
	html ("<p>Select an LED to flash</p>");
	
	/* Only display leds that the platform has */
	html ("<fieldset> User LED <p> <br>");

	for (i = 0; i < gPlatformInfo.led[PLATFORM_USER_LED_CLASS].count; i++)
	{
		if (i == 0) {
			html_var ("<input type='radio' name='leds' value='%d' checked> %d &nbsp;&nbsp;", i, i+1); /* default selection */
		}
		else {
			if ( (i % 4) == 0 ) html ("<br>"); /* display leds in rows of 4 */
			html_var ("<input type='radio' name='leds' value='%d'> %d &nbsp;&nbsp;", i, i+1);
		}
	}

	html ("</p></fieldset><br><input type='hidden' name='test' value='ledtest'/>");
	html ("<p><input type='submit' value='Flash LED' ></p></form></td>");


	html ("<td width='50%' style='border-style:solid;border-width:1px;border-collapse:collapse' align='left' valign='top'>");

	html ("<form action='diagresult.cgi' method='post'>");

  	html ("<p> <b>UART</b></p>");
	html ("<p class='howitworks'><b>How it works:</b> This test exercises the UART by writing a string of characters to the port. The text can be viewed if you have a serial port connection set up.</p>");
	html ("<p><input type='text' name='echotext' size='40' maxlength='40'></p>");

   	html ("<input type='hidden' name='test' value='uarttest'/>"); 	
  	html ("<p><input type='submit' value='Write'/></p>");
	html ("</form>");
	html ("</td>");
	html ("</tr></table></td></tr></table>");


	html ("<br><br></div><div style='clear: both;'>&nbsp;</div></div>");  

	html (PAGEFOOTER);

	html_end ("</body></html>");

	/* Send header */
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    /* After this call we MUST send the data since a CRLF is being sent */
	httpSendEntityLength(htmlSock, html_getsize() );

	/* Send the page */
	httpSendClientStr(htmlSock, html_getpage());


	return 1;	
}

