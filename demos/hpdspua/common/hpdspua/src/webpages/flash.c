 /*
 * Flash.c
 *
 * CGI function to display the Flash Utility Page
 *
 * Copyright (C) 2010-2011 Texas Instruments Incorporated - http://www.ti.com/
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  flash.c
 *
 *   @brief   
 *      Contains CGI functions to display the Flash Utility web page.
 *
 */
 
#include "hpdspua.h"


/*************************************************************************
 *  @b serveFlashPage(SOCKET htmlSock, int ContentLength, char *pArgs )
 * 
 *  @n
 * 
 * 	CGI function called by the server to display platform infroamtion
 *  about the flash.
 *
 *  @param[in]  
 *  htmlSock - Socket to the browser
 * 
 *  @param[in] 
 *  ContentLength - Length of the page request from the browser
 * 
 *  @param[in] 
 *  pArgs - Not used
 *  
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/
/* Save the flash selected. When a user clicks the Flash Tab the current selection they may have made is not passed in */
#ifdef C66_PLATFORMS
static uint32_t flash_selection = PLATFORM_DEVID_NAND512R3A2D;
#define NAND_DEVICE     PLATFORM_DEVID_NAND512R3A2D
#define NOR_DEVICE      PLATFORM_DEVID_NORN25Q128
#else
static uint32_t flash_selection = PLATFORM_DEVID_MT29F1G08ABCHC;
#define NAND_DEVICE   PLATFORM_DEVID_MT29F1G08ABCHC
#define NOR_DEVICE    PLATFORM_DEVID_NORN25Q032A
#endif
int32_t serveFlashPage(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
	int32_t				i;
	int32_t				fTableUpdate;
	uint32_t			deviceid;
	char				*value;
	char 				szSmall[24];
	PLATFORM_DEVICE_info *p_device = NULL;

	/* Process the POST variables */
	if (!html_processPost(htmlSock,ContentLength)) {
		return 1;
	}

	html_start("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>");
	html ("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='content-type' content='text/html; charset=utf-8' />");
	html ("<title>High Performance Digital Signal Processor Utility</title><link rel='stylesheet' href='default.css' type='text/css' />");
	html ("<SCRIPT TYPE='text/javascript'>");
	/*html ("<!-- ");*/
	html ("function writesubmit() { ");
	html (" document.body.style.cursor = 'wait'; ");
	html (" document.getElementById('flashwait').innerHTML = \"<br><font color='#FF0000'>It can take a few minutes to write the Flash. Please be patient.</font>\"; ");
	html (" document.flashform.submit(); }");
	/*html (" //--> ");*/
	html ("</SCRIPT></head>");

	html ("<body>");

	html_var (PAGEWRAPPER, "Flash Utility");

	html ("<div id='page'><div id='content'>");

	/* Page Header */
	html ("<hr><table cellpadding='6'><tr>");
	html ("<td valign='center'><image src='dspchip.gif'></td>");
	html ("<td width='100%' align='justify'>");
	html ("<h2>Read or Write Flash on the Platform</h2><br>");
	html ("<p class='howitworks'><b>How it works:</b> This page allows you to get details about the flash devices on the platform and to read or write to them using the Platform Library device APIs.");
	html ("When writing the file it will be POSTED to the platform where a CGI back end service ");
	html ("will use the platform library to write it. On successful flashing of the image, the following message will be ");
	html ("displayed : Flash successfully updated");
	html ("  </p>");
	html ("</td></tr></table><hr><br>");

	/* Get the flash device to work with.. first time thru we will default to NAND */
	value = html_getValueFor("FlashDeviceType");

	if (value == NULL) {
		deviceid = flash_selection;
	}
	else {
		if (strncmp(value, "NAND", 3) == 0) {
			deviceid = NAND_DEVICE;
			flash_selection = NAND_DEVICE;
		}
		else {
			deviceid = NOR_DEVICE;
			flash_selection = NOR_DEVICE;
		}
	}

	if (deviceid == NAND_DEVICE) {
		strcpy (szSmall, "NAND");
	}
	else {
		strcpy (szSmall, "NOR");
	}

	/* Open the device so we can get the details on it */
	p_device = platform_device_open(deviceid, 0);

	if (p_device == NULL) {
		html_var("Unable to open the %s flash device (errno = 0x%x).", szSmall, platform_errno);
		goto FLASH_DONE;
	}

	/* Form to select the device type */
	html ("<div style='width: 500px; margin-left: 25px; padding: 2px;'>");
	html ("<form action='flash.cgi' method='post'>");
	html ("<p> Select the Flash Device to work with:</p>");
	if (deviceid == NAND_DEVICE) {
		html ("<p><input type='radio' value='NAND' checked name='FlashDeviceType'>&nbsp; NAND &nbsp;&nbsp;&nbsp;");
		html ("<input type='radio' name='FlashDeviceType' value='NOR'>&nbsp; NOR &nbsp;&nbsp;&nbsp;");
	}
	else {
		html ("<p><input type='radio' value='NAND' name='FlashDeviceType'>&nbsp; NAND &nbsp;&nbsp;&nbsp;");
		html ("<input type='radio' checked name='FlashDeviceType' value='NOR'>&nbsp; NOR &nbsp;&nbsp;&nbsp;");
	}
	html ("<input type='submit' value='Select'></p>");
	html ("</form></div><hr><br>");

 	/* Flash Info */
	html ("<div style='width: 500px; margin-left: 25px; padding: 2px; border: 1px gray solid;'>");
	html ("<table border='0' width='100%' id='flashinfotable' cellpadding='5'>");
	html_var ("<tr><td colspan='4'><b>%s Flash Information</b></td></tr>", szSmall);

	html_var ("<tr><td>Manufacturer ID</td><td>%d</td><td>Device ID</td><td>%d</td></tr>",
				p_device->manufacturer_id,
				p_device->device_id);
	html_var ("<tr><td>Flash Type</td><td>%s</td><td>Width in Bytes</td><td>%d</td></tr>",
				szSmall,
				p_device->width);
	html_var ("<tr><td>Block Count</td><td>%d</td><td>Page Count</td><td>%d</td></tr>",
				p_device->block_count,
				p_device->page_count);
	html_var ("<tr><td>Page Size</td><td>%d</td><td>&nbsp;</td><td>&nbsp;</td></tr>",
				p_device->page_size);
	html ("<tr><td>Bad Block Numbers:</td><td colspan='3'> ");

    if (p_device->bblist) {
		for (i=0; i < p_device->block_count; i++) {
			if (p_device->bblist[i] == 0x00) {
				html_var (" %d ", i);
				fTableUpdate = 1;
			}
		}
    }

	if (!fTableUpdate) {
		html (" None ");
	}

	html ("</td></tr>");
	html ("</table></div><br>");

 	/* Flash read */
 	html ("<div style='width: 500px; margin-left: 25px; padding: 2px; border: 1px gray solid;'>");
	html ("<form action='flashread.cgi' method='post'>");
	html ("<table border='0' cellpadding='5' id='flashreadtable' width='368'>");
	html ("<tr><td width='91%' align='left' colspan='3'><b>Read Flash</b></td></tr>");
	html ("<tr><td width='17%' align='left'>Block</td>");
	html ("<td align='left' width='28%'>");
	html_var ("<input type='text'  name='block' size='5' value='%d'></td>", p_device->block_count - 100);
	html ("<td align='left' width='43%' rowspan='2' valign='top'></td></tr>");
	html ("<tr><td width='91%' align='left' colspan='3'><input type='submit' value='Read'></td></tr>");
	html ("</table><input type='hidden' name='page' size='12' value='0'>");
	if (deviceid == NAND_DEVICE)
		html_var ("<input type='hidden' name='deviceid' size='12' value='%d'>", NAND_DEVICE);
	else
		html_var ("<input type='hidden' name='deviceid' size='12' value='%d'>", NOR_DEVICE);
	html ("</td></form></div><br>");

 	/* Flash Write */
 	html ("<div style='margin-left: 25px; width: 500px; padding: 2px; border: 1px solid gray;'>");
 	html ("<form name='flashform' action='flashwrite.cgi' method='post' enctype='multipart/form-data'>");
	if (deviceid == NAND_DEVICE)
		html_var ("<input type='hidden' name='deviceid' size='12' value='%d'>", NAND_DEVICE);
	else
		html_var ("<input type='hidden' name='deviceid' size='12' value='%d'>", NOR_DEVICE);
	html ("<table border='0' cellpadding='5' id='flashwritetable' width='471'>");
	html ("<tr><td width='75%' align='left' colspan='2'><b>Write Flash</b></td></tr>");

	html ("<tr><td width='25%' align='left'>Starting block: </td>");
	html ("<td align='left' width='71%'>");
	html_var ("<input type='text'  name='block' size='5' value='%d'></td></tr>", p_device->block_count - 100);
	html ("<tr><td width='25%' align='left'>File to Write</td>");
	html ("<td align='left' width='71%'><input type='file'  name='image' size='30'></td></tr>");
	html ("<tr><td width='76%' align='left' colspan='2'><input type='button' value='Write' onClick='writesubmit();'></td></tr>");
	html ("<tr><td id='flashwait' align='left' colspan='2'>");
	html ("Be careful writing a File to flash or you may overwrite the bootable image and/or corrupt it.</td></tr>");
	html ("</table></form></div><br>");

 	/* Flash erase */
 	html ("<div style='width: 500px; margin-left: 25px; padding: 2px; border: 1px gray solid;'>");
	html ("<form action='flasherase.cgi' method='post'>");
	html ("<table border='0' cellpadding='5' id='flasherasetable' width='368'>");
	html ("<tr><td width='91%' align='left' colspan='3'><b>Erase Flash</b></td></tr>");
	html ("<tr><td width='17%' align='left'>Block</td>");
	html ("<td align='left' width='28%'>");
	html_var ("<input type='text'  name='block' size='5' value='%d'></td>", p_device->block_count - 100);
	html ("<td align='left' width='43%' rowspan='2' valign='top'></td></tr>");
	html ("<tr><td width='91%' align='left' colspan='3'><input type='submit' value='Erase'></td></tr>");
	html ("</table><input type='hidden' name='page' size='12' value='0'>");
	if (deviceid == NAND_DEVICE)
		html_var ("<input type='hidden' name='deviceid' size='12' value='%d'>", NAND_DEVICE);
	else
		html_var ("<input type='hidden' name='deviceid' size='12' value='%d'>", NOR_DEVICE);
	html ("</td></form></div>");

FLASH_DONE:

	if (p_device != NULL) {
		platform_device_close(p_device->handle);
	}

	/* Finish up the page */
	html ("</div>");
	html ("<div style='clear: both;'>&nbsp;</div>");
	html ("</div>");

	html (PAGEFOOTER);

	html_end ("</body></html>");

	/* Send header */
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    /* After this call we MUST send the data since a CRLF is being sent */
	httpSendEntityLength(htmlSock, html_getsize() );

	/* Send the page */
	httpSendClientStr(htmlSock, html_getpage());

	return 1;
}
