/*
 * Erase.c
 *
 * CGI function to handle block erase for a flash device
 *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  Erase.c
 *
 *   @brief
 *      Contains CGI functions to erase a block on the flash.
 *
 */

#include "hpdspua.h"

/*************************************************************************
 *  @b serveFlashErasePage(SOCKET htmlSock, int ContentLength, char *pArgs )
 *
 *  @n
 *
 * 	CGI function called by the server to write a flash image.
 *
 *  @param[in]
 *  htmlSock - Socket to the browser
 *
 *  @param[in]
 *  ContentLength - Length of the page request from the browser
 *
 *  @param[in]
 *  pArgs - Not used
 *
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/
int32_t serveFlashErasePage(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
	int32_t				 block;
	char 				 *value;
	uint32_t			  deviceid;
	PLATFORM_DEVICE_info *p_device = NULL;

	/*
	** Create the response page
	*/
	html_start(PAGEHEAD);

	html ("<body>");

	html_var (PAGEWRAPPER, "Flash Erase Result");

	html ("<div id='page'><div id='content'>");

	html ("<br>");

	html ("<div align='center'>");

	/*  Process the POST variables. */
	if (!html_processPost(htmlSock,ContentLength)) {
		return 1;
	}

	/* Get the flash memory device  */
	value = html_getValueFor("deviceid");
	if (value == NULL) {
		html ("<p>You must select a flash device to work with</p>");
		goto FLASH_ERROR;
	}
	else {
		deviceid = atoi(value);
	}

	p_device = platform_device_open(deviceid, 0);
	if (p_device == NULL) {
		html ("<p>Unable to open the flash device</p>");
		goto FLASH_ERROR;
	}

	/* Get the block number to erase */
	value = html_getValueFor("block");
	block = atoi(value);

	if (platform_device_erase_block(p_device->handle, block) != Platform_EOK) {
 		html_var("<p>Unable to erase the block (platform errno = 0x%x). </p>", platform_errno);
		goto FLASH_ERROR;
 	}

	html_var("<p>Block %d was erased </p>", block);
	platform_write("Block %d was erased \n", block);

FLASH_ERROR:

	if (p_device) {
		platform_device_close(p_device->handle);
	}


	html ("</div>");

	html ("</div><div style='clear: both;'>&nbsp;</div></div>");

	html (PAGEFOOTER);

	html_end ("</body></html>");

	/* Send header */
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    /* After this call we MUST send the data since a CRLF is being sent */
	httpSendEntityLength(htmlSock, html_getsize() );

	/* Send the page */
	httpSendClientStr(htmlSock, html_getpage());


	return 1;
}


