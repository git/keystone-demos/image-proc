 /*
 * Flashread.c
 *
 * CGI function to handle the reading the flash
 *
 * Copyright (C) 2010-2011 Texas Instruments Incorporated - http://www.ti.com/
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  flashread.c
 *
 *   @brief   
 *      Contains CGI functions to read and display the Flash.
 *
 */
 
#include <ctype.h>
#include "hpdspua.h"


/*************************************************************************
 *  @b serveFlashReadPage(SOCKET htmlSock, int ContentLength, char *pArgs )
 * 
 *  @n
 * 
 * 	CGI function called by the server to display contents of the Flash.
 *  It will read one page from flash and display it. To do this, it uses 
 *  some hidden variables on the html page for the current block and page.
 *
 *  @param[in]  
 *  htmlSock - Socket to the browser
 * 
 *  @param[in] 
 *  ContentLength - Length of the page request from the browser
 * 
 *  @param[in] 
 *  pArgs - Not used
 *  
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/

int32_t serveFlashReadPage(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
	int32_t				 block, page;
	int32_t				 i;
	char 				 *value;
	uint8_t				 *buffer = NULL;
	uint32_t			  offset;
	uint32_t			  deviceid;
	PLATFORM_DEVICE_info *p_device = NULL;
	Error_Block	    	 errorBlock;

	/*  Process the POST variables. */
	if (!html_processPost(htmlSock,ContentLength)) {
		return 1;
	}

	Error_init(&errorBlock);

	/* Create the response page */
	html_start("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>");
	html("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='content-type' content='text/html; charset=utf-8' />");
	html("<title>High Performance Digital Signal Processer Utility Application</title>");
	html("<link rel='stylesheet' href='default.css' type='text/css' />");

	html(" <SCRIPT TYPE='text/javascript'> ");
	html(" function nextclick() { var page=0; page = parseInt(document.myform.elements['page'].value); page = page + 1; document.myform.elements['page'].value = page; document.myform.submit();} ");
	html(" function prevclick() { var page=0; page = parseInt(document.myform.elements['page'].value); page = page - 1; if (page < 0) page = 0; document.myform.elements['page'].value = page; document.myform.submit();} ");	
	html(" </SCRIPT> </head>");

	html ("<body>");

	html_var (PAGEWRAPPER, "Flash Contents");

	html("<div id='page'>");

	html("<form name='myform' action='flashread.cgi' method='post'>");

	html("<table border='0' width='760' id='table1'>");
	html("<tr><td width='346'><input type='button' value='Previous' onclick='prevclick()' /></td>");
	html("<td><input type='button' value='Next     ' onclick='nextclick()' /></td></tr>");
	html("</table>");

	html("<hr WIDTH='100%' SIZE='5' COLOR='red'>");

	/* Get the index to the flash memory device */
	value = html_getValueFor("deviceid");
	if (value == NULL) {
		html ("<p>You must select a flash device to work with</p>");
		goto DONE;
	}
	else {
		deviceid = atoi(value);
	}

	/* We are occasionally getting a busy from the NAND when reading the spare area looking
	 * for bad blocks.. so try a few times to open.
	 */
	i = 0;
	do {
		p_device = platform_device_open(deviceid, 0);
		i++;
	} while (i < 4 && p_device == NULL);

	if (p_device == NULL) {
		html_var ("<p>Unable to open the NAND flash to read from (platform errno = 0x%x)</p>", platform_errno);
		goto DONE;
	}
 	
	value = html_getValueFor("block");
	block = atoi(value);


	/* Is block in range? */
	if ((block < 0) || (block >= p_device->block_count)) {
		html_var("<p>Your block must be in the range of 0 to %d</p>", p_device->block_count - 1);
		goto DONE;
	}

	/* Get memory to hold the page we will read in */
	buffer = (uint8_t *) Memory_alloc( NULL, platform_roundup(p_device->page_size, PLATFORM_CACHE_LINE_SIZE), PLATFORM_CACHE_LINE_SIZE, &errorBlock);

    if ( !buffer ){
		html ("<p>Unable to allocate memory to read flash</p>");
		goto DONE;
	}

    if (p_device->bblist) {
		if (p_device->bblist[block] == 0x00)
		{
			html ("<p>The block you are trying to read is marked as a bad block.</p>");
			goto DONE;
		}
    }

    memset (buffer, 0, p_device->page_size);

	value = html_getValueFor("page");
	page = atoi(value);

	/* Wrap the page value if it exceeds max pages for the device */
	if (page > p_device->page_count) page = 0;

	html_var ("<h3>Reading Block %d Page %d</h3><br>", block, page);

	html("<div id='contentscroll'>");

	platform_blocknpage_to_offset(p_device->handle, &offset, block, page);

	if (platform_device_read(p_device->handle, offset, buffer, p_device->page_size) == Platform_EOK) {

		html("<table border='0' cellpadding='4' id='flashreadtable'>");

		for (i=0; i < p_device->page_size; i +=8)
		{
			html("<tr>");
			html_var("<td>0x%02x</td><td>0x%02x</td><td>0x%02x</td><td>0x%02x</td><td>0x%02x</td><td>0x%02x</td><td>0x%02x</td><td>0x%02x</td>",
						*(buffer+i), *(buffer+i+1), *(buffer+i+2), *(buffer+i+3), *(buffer+i+4), *(buffer+i+5), *(buffer+i+6),
						*(buffer+i+7));
			html("<td>&nbsp;</td>");
			html_var("<td>%c</td><td>%c</td><td>%c</td><td>%c</td><td>%c</td><td>%c</td><td>%c</td><td>%c</td>",
						toascii(*(buffer+i)), toascii(*(buffer+i+1)), toascii(*(buffer+i+2)),
						toascii(*(buffer+i+3)), toascii(*(buffer+i+4)), toascii(*(buffer+i+5)),
						toascii(*(buffer+i+6)), toascii(*(buffer+i+7)));
			html("</tr>");
		}
		html ("</table>");
	}
	else {
		html_var ("<p> Failed to read at Block %d Page %d </p>", block, page);
	}

	html("</div>");



DONE:

	if (buffer) {
		Memory_free( NULL, buffer,  platform_roundup(p_device->page_size, PLATFORM_CACHE_LINE_SIZE) );
	}

	if (p_device != NULL) {
		platform_device_close(p_device->handle);
	}

	html_var("<input type='hidden' name='block' size='1' value='%d'><input type='hidden' name='page' size='1' value='%d'>", block, page);
	html_var("<input type='hidden' name='deviceid' size='12' value='%d'>", deviceid);
	html("</form>");

	html ("</div>");

	html ("</div><div style='clear: both;'>&nbsp;</div></div>");

	html (PAGEFOOTER);

	html_end ("</body></html>");

	/* Send header */
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    /* After this call we MUST send the data since a CRLF is being sent */
	httpSendEntityLength(htmlSock, html_getsize() );

	/* Send the page */
	httpSendClientStr(htmlSock, html_getpage());


	return 1;
}
