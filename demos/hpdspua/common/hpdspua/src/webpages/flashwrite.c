 /*
 * Flashwrite.c
 *
 * CGI function to handle the Flash write page
 *
 * Copyright (C) 2010-2011 Texas Instruments Incorporated - http://www.ti.com/
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  flash.c
 *
 *   @brief   
 *      Contains CGI functions to write a image to the Flash.
 *
 */
 
#include "hpdspua.h"

/*************************************************************************
 *  @b serveFlashWritePage(SOCKET htmlSock, int ContentLength, char *pArgs )
 * 
 *  @n
 *  
 * 	CGI function called by the server to write a flash image.
 *
 *  @param[in]  
 *  htmlSock - Socket to the browser
 * 
 *  @param[in] 
 *  ContentLength - Length of the page request from the browser
 * 
 *  @param[in] 
 *  pArgs - Not used
 *  
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/
int32_t serveFlashWritePage(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
	int32_t 			 len;
	int32_t				 block, page, block_size, page_size, num_blocks_needed;
	char 				 *value;
	FLASH_ADDRESS 		  sblock;
	FLASH_ADDRESS 		  eblock;
	uint32_t			  deviceid;
	uint32_t			  offset;
	PLATFORM_DEVICE_info *p_device = NULL;

	/*
	** Create the response page
	*/
	html_start(PAGEHEAD);

	html ("<body>");

	html_var (PAGEWRAPPER, "Flash Result");

	html ("<div id='page'><div id='content'>");

	html ("<br>");

	html ("<div align='center'>");

	/*
	**  Process the POST. It is multi-part MIME.
	*/
	memset(gRxBuffer, 0xFF, MAX_POST_FILESZ);

	len = html_processFileUpload(htmlSock, ContentLength, gRxBuffer, MAX_POST_FILESZ);

	/*
	** if (we had some sort of internal socket error)
	*/
	if (len == HTML_FATAL) {
		html_var ("There was an error receiving the data. You can try again."); 
		goto FLASH_ERROR;
	}

	/*
	** if (the file was too large for our receive buffer gRxBuffer to hold)
	*/
	if (len == HTML_TOO_LARGE) {
		html_var ("Your file was too large. It must be less than %d bytes.", MAX_POST_FILESZ); 
		goto FLASH_ERROR;
	}
	
	/*
	** if (the fields in the form didn't parse properly or were not set)
	*/
	if (len == HTML_PARSER_ERROR) {
		if (POST_files[0].name[0] == '\0') {
			html("You did not specify a program to write."); 
		}
		else {
			html("Internal Error parsing the Post data."); 
		}
		goto FLASH_ERROR;
	}

	/*
	** if (there was an unexpected error code)
	*/
	if (len <= 0) {
		/* we shouldn't land here unless someone added new error codes. */
		html_var ("There was an error receiving the data (%d). You can try again.", len); 
		goto FLASH_ERROR;
	}

	/*
	** Looks like we have a file to flash
	*/


	/* Get the flash memory device to write to */
	value = html_getValueFor("deviceid");
	if (value == NULL) {
		html ("<p>You must select a flash device to work with</p>");
		goto FLASH_ERROR;
	}
	else {
		deviceid = atoi(value);
	}

	p_device = platform_device_open(deviceid, 0);
	if (p_device == NULL) {
		html ("<p>Unable to open the flash to read from</p>");
		goto FLASH_ERROR;
	}

	/* save off values for various calculations and checks */
	block_size = p_device->page_size * p_device->page_count;
	page_size  = p_device->page_size;

	/* Make sure the file does not  exceed the size of the flash */
	if (len > (p_device->block_count * block_size)) {
		html_var ("<p>The file you are writing is larger than the flash size of %d</p>", p_device->block_count * block_size);
		goto FLASH_ERROR;
	}
		
	/* Get the block number to start at*/
	value = html_getValueFor("block");
	block = atoi(value);

	/* based on the size of the file and the block size, determine the number of blocks required
	   to store the file. */		 
	
    if (len < block_size){
    	num_blocks_needed = 1;
    }
    else {
    	num_blocks_needed = len / block_size;
    	/* we may need 1 more block to get the remainder of the file */
    	if (len % block_size)
    		num_blocks_needed += 1;
    }

	/* based on the starting block, make sure we have enough consecutive blocks to hold the data */
    if ((block + num_blocks_needed) > p_device->block_count) {
		html_var ("<p>You need %d blocks to hold the image. Based on your starting block you do not have enough room.</p>", num_blocks_needed);
		goto FLASH_ERROR;
	}

 	/* 
 	** Write the file to flash..use the block specified but set the starting page to 0. The file that was uploaded
 	** will be pointed to by the images block (the storage for that file is in gRxBuffer).
 	*/

    /* Get starting offset to write at and record starting and ending block/page for display */
 	sblock.block = block;
 	sblock.page  = 0;

 	(void) platform_blocknpage_to_offset(p_device->handle, &offset, sblock.block, sblock.page);
	(void) platform_offset_to_blocknpage(p_device->handle, (offset+len), &eblock.block, &eblock.page);

	platform_write("Writing file %s to the flash...\n", POST_files[0].name);
	if (platform_device_write(p_device->handle, offset, POST_files[0].p_file, len) != Platform_EOK) {
 		html_var("<p>Unable to write the file to flash (platform errno = 0x%x). </p>", platform_errno);
 		//platform_offset_to_blocknpage(p_device->handle, p_device->error_info.offset, &sblock.block, &sblock.page);
 		//html_var("<p>The error occurred at block %d page %d. </p>", sblock.block, sblock.page);
 		goto FLASH_ERROR;
 	}

	html("<p>Flash successfully updated</p>");
	platform_write("Flash successfully updated: \n");

	/* Number pages it took */	
	page = (len / page_size) + 1;

	html_var("<p>File %s </p>", POST_files[0].name);
	
	html_var("<p>Total bytes: %d Total Pages:  %d </p>", len, page);

	html_var("<p>Starting block %d page %d  :  Ending block %d page %d </p>", 
			 sblock.block, sblock.page, eblock.block, eblock.page);

	
	platform_write( "File: %s \n", POST_files[0].name);

	platform_write( "(Total bytes %d) (Total Pages  %d) \n", len, page);

	platform_write( "(Start block %d page %d) (End block %d page %d) \n",  sblock.block, sblock.page, eblock.block, eblock.page);

FLASH_ERROR:

	if (p_device) {
		platform_device_close(p_device->handle);
	}


	html ("</div>");

	html ("</div><div style='clear: both;'>&nbsp;</div></div>");

	html (PAGEFOOTER);

	html_end ("</body></html>");

	/* Send header */
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    /* After this call we MUST send the data since a CRLF is being sent */
	httpSendEntityLength(htmlSock, html_getsize() );

	/* Send the page */
	httpSendClientStr(htmlSock, html_getpage());


	return 1;	
}
