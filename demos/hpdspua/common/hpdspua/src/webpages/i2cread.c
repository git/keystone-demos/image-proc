 /*
 * i2cread.c
 *
 * CGI function to handle the reading the flash
 *
 * Copyright (C) 2010-2011 Texas Instruments Incorporated - http://www.ti.com/
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  i2cread.c
 *
 *   @brief   
 *      Contains CGI functions to read and display the EEPROM.
 *
 */
 
#include <ctype.h>
#include "hpdspua.h"

/* Turn this on to dump the contanents of the EEPROM being read into gRxBuffer. This allows
 * us to read the memory location with CCS and save it to a file. This file can then be 
 * compared to the appropriate .dat to be sure it matches.
 */ 
#define EEPROM_DUMP_DEBUG	0

/* Create our own page size that can reasonably be read in a browser window */
static int32_t eeprom_page_size = 1024;


/*************************************************************************
 *  @b serveEepromReadPage(SOCKET htmlSock, int ContentLength, char *pArgs )
 * 
 *  @n
 *  
 * 	CGI function called by the server to display contents of the EEPROM.
 *  The EEPROM is displayed in 1K chunks. We keep track of where we are 
 *  in reading the EEPROM by storing hidden fields on the page that we 
 *  process here.
 *
 *  @param[in]  
 *  htmlSock - Socket to the browser
 * 
 *  @param[in] 
 *  ContentLength - Length of the page request from the browser
 * 
 *  @param[in] 
 *  pArgs - Not used
 *  
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/
int32_t serveEepromReadPage(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
	int32_t					address, slave_address;
	int32_t					i;
	int32_t					length;
	uint32_t 				eeprom_size;
	char 					*value;
	uint8_t					*buffer	  = NULL;
	PLATFORM_DEVICE_info	*p_device = NULL;
	Error_Block	    		errorBlock;
	
	/*  Process the POST variables. */
	if (!html_processPost(htmlSock,ContentLength)) {
		return 1;
	}

	Error_init(&errorBlock);

	/* Create the response page */
	html_start("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>");
	html("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='content-type' content='text/html; charset=utf-8' />");
	html("<title>High Performance Digital Signal Processer Utility Application</title>");
	html("<link rel='stylesheet' href='default.css' type='text/css' />");

	html(" <SCRIPT TYPE='text/javascript'> ");
	html(" function nextclick() { var addr=0; addr = parseInt(document.myform.elements['address'].value); addr = addr + 1024; document.myform.elements['address'].value = addr; document.myform.submit();} ");
	html(" function prevclick() { var addr=0; addr = parseInt(document.myform.elements['address'].value); addr = addr - 1024; if (addr < 0) addr = 0; document.myform.elements['address'].value = addr; document.myform.submit();} ");	
	html(" </SCRIPT> </head>");

	html ("<body>");

	html_var (PAGEWRAPPER, "EEPROM Contents");

	html("<div id='page'>");

	html("<form name='myform' action='eepromread.cgi' method='post'>");

	html("<table border='0' width='760' id='table1'>");
	html("<tr><td width='346'><input type='button' value='Previous' onclick='prevclick()' /></td>");
	html("<td><input type='button' value='Next     ' onclick='nextclick()' /></td></tr>");
	html("</table>");

	html("<hr WIDTH='100%' SIZE='5' COLOR='red'>");

	value = html_getValueFor("slave_address");
	slave_address = atoi(value);

	value = html_getValueFor("address");
	address = atoi(value);

	p_device = platform_device_open(slave_address, 0);

	if (p_device == NULL) {
		html("<p>Unable to open the EEPROM device for reading</p>");
		goto DONE;
	}

	eeprom_size = p_device->page_size;

	if ((address < 0) || (address > eeprom_size)) {
		html_var("<p>Your address must be in the range of 0 to %d</p>", eeprom_size);
		goto DONE;
	}

	buffer = (uint8_t *)Memory_alloc( NULL, eeprom_page_size, 0, &errorBlock );

    if ( !buffer ){
		html ("<p>Unable to allocate memory to read flash</p>");
		goto DONE;
	}

	html_var ("<h3>Read starting at address %d</h3><br>", address);

	html("<div id='contentscroll'>");

	memset (buffer, 0, eeprom_page_size);

	if ((address+eeprom_page_size) > eeprom_size) {
		/* we don't have a full page to read */
		length = (address+eeprom_page_size) - eeprom_size;
	}
	else {
		length = eeprom_page_size;
	}

	if (platform_device_read(p_device->handle, address, (Uint8 *)buffer, length) == Platform_EOK) {
 		html("<table border='0' cellpadding='4' id='flashreadtable'>");       
		for (i=0; i < length; i +=8) {
			
			html("<tr>");
			html_var("<td>0x%02x</td><td>0x%02x</td><td>0x%02x</td><td>0x%02x</td><td>0x%02x</td><td>0x%02x</td><td>0x%02x</td><td>0x%02x</td>", 
						*(buffer+i), *(buffer+i+1), *(buffer+i+2), *(buffer+i+3), *(buffer+i+4), *(buffer+i+5), *(buffer+i+6),
						*(buffer+i+7));
			html("<td>&nbsp;</td>");
			html_var("<td>%c</td><td>%c</td><td>%c</td><td>%c</td><td>%c</td><td>%c</td><td>%c</td><td>%c</td>", 
						toascii(*(buffer+i)), toascii(*(buffer+i+1)), toascii(*(buffer+i+2)), 
						toascii(*(buffer+i+3)), toascii(*(buffer+i+4)), toascii(*(buffer+i+5)), 
						toascii(*(buffer+i+6)), toascii(*(buffer+i+7)));
			html("</tr>");
		}

		html ("</table>");
	}
	else {
		html ("<p>There was a problem reading the EEPROM.</p>");
	}
	html("</div>");

 /* Debug Code */
#if EEPROM_DUMP_DEBUG
	if (platform_device_read(p_device->handle, address, (uint8_t *)gRxBuffer, eeprom_size);
    	platform_write( "Read EEPROM at %x and dumped at memory location gRxBuffer %x \n", slave_address,  &gRxBuffer);
#endif
 /* Debug Code */


DONE:

	if (buffer) {
		Memory_free(NULL, buffer, eeprom_page_size);
	}

	if (p_device) {
		platform_device_close(p_device->handle);
	}

	/* Store the address we are currently using as hidden fields so we can read them back in on a button click */
	html_var("<input type='hidden' name='address' size='1' value='%d'>", address);
	html_var("<input type='hidden' name='slave_address' size='1' value='%d'>", slave_address);
	html("</form>");

	html ("</div>");

	html ("</div><div style='clear: both;'>&nbsp;</div></div>");

	html (PAGEFOOTER);

	html_end ("</body></html>");

	/* Send header */
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    /* After this call we MUST send the data since a CRLF is being sent */
	httpSendEntityLength(htmlSock, html_getsize() );

	/* Send the page */
	httpSendClientStr(htmlSock, html_getpage());


	return 1;	
}


