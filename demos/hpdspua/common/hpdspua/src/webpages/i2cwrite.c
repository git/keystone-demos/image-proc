 /*
 * i2cwrite.c
 *
 * CGI function to handle the I2C write page
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  i2cwrite.c
 *
 *   @brief   
 *      Contains CGI functions to write a EEPROM image after it is 
 *      POSTed from the EEPROM web page. The image to be posted 
 *      must have been created with the EEPROM image utility.
 *
 */

#if 0
#include "hpdspua.h"

/* until we can get it from the platform library */
#define EEPROM_SIZE	65536		

/*************************************************************************
 *  @b serveEepromWritePage(SOCKET htmlSock, int ContentLength, char *pArgs )
 * 
 *  @n
 *  
 * 	CGI function called by the server to write a eeprom image.
 *
 *  @param[in]  
 *  htmlSock - Socket to the browser
 * 
 *  @param[in] 
 *  ContentLength - Length of the page request from the browser
 * 
 *  @param[in] 
 *  pArgs - Not used
 *  
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/
int serveEepromWritePage(SOCKET htmlSock, int ContentLength, char *pArgs )
{
	int		len;
	Int32	address, slave_address;
	Int32	rc;
	char	*value;


	/*
	**  Process the POST. It is multi-part MIME
	*/
	len = html_processFileUpload(htmlSock, ContentLength, gRxBuffer, MAX_POST_FILESZ);

	/*
	** Create the response page
	*/
	html_start(PAGEHEAD);

	html ("<body>");

	html_var (PAGEWRAPPER, "I2C Result");

	html ("<div id='page'><div id='content'>");

	html ("<br>");

	html ("<div align='center'>");

	/*
	** if (we had some sort of internal socket error)
	*/
	if (len == HTML_FATAL) {
		html_var ("There was an error receiving the data. You can try again."); 
		goto EPAGEDONE;
	}

	/*
	** if (the file was too large for our receive buffer gRxBuffer to hold)
	*/
	if (len == HTML_TOO_LARGE) {
		html_var ("Your file was too large. It must be less than %d bytes.", MAX_POST_FILESZ); 
		goto EPAGEDONE;
	}
	
	/*
	** if (the fields in the form didnt parse properly or were not set)
	*/
	if (len == HTML_PARSER_ERROR) {
		if (POST_images[0].name[0] == '\0') {
			html("You did not specify a program to run."); 
		}
		else {
			html("Internal Error parsing the Post data."); 
		}
		goto EPAGEDONE;
	}

	/*
	** if (there was an unexpected error code)
	*/
	if (len <= 0) {
		// we shouldnt land here unless someone added new error codes.
		html_var ("There was an error receiving the data (%d). You can try again.", len); 
		goto EPAGEDONE;
	}

	/*
	** Looks like we have a file to write.
	*/

	/* Get the slave address */
	value = html_getValueFor("slave_address");
	slave_address = atoi(value);
	
	/* Get the address to start at*/
	value = html_getValueFor("address");
	address = atoi(value);

	if ((address + len) > EEPROM_SIZE){
		html("The length of the file exceeds the size of the EEPROM."); 
		goto EPAGEDONE;
	}

	
	/*
	** Write the EEPROM
	*/
	printf ("writing eeprom file size of %d at address %x \n", len, address);

	/* If I set the address to 0, then the buffer must have the address as the first 4 bytes */
	/*rc = platform_eeprom_write(slave_address, address, len, (Uint8 *) gRxBuffer);*/

	if (rc !=  Platform_EOK) {
		html_var ("There was a problem writing the EEPROM: Code (%d)", rc); 
	}
	else {
		html_var ("EEPROM written successfully"); 
	}

EPAGEDONE:

	html ("</div>");

	html ("</div><div style='clear: both;'>&nbsp;</div></div>");

	html (PAGEFOOTER);

	html_end ("</body></html>");

	// Send header
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    // After this call we MUST send the data since a CRLF is being sent
	httpSendEntityLength(htmlSock, html_getsize() );

	// Send the page
	httpSendClientStr(htmlSock, html_getpage());


	return 1;	
}
#endif

