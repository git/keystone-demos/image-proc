 /*
 * Information.c
 *
 * CGI function to display the information page
 *
 * Copyright (C) 2010-2011 Texas Instruments Incorporated - http://www.ti.com/
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  information.c
 *
 *   @brief   
 *      Contains CGI functions to create the Information web page and
 *      serve it back to a browser.
 *
 */


#include "hpdspua.h"
#include "csl_version.h"
#include <ti/sysbios/knl/Clock.h>
#ifdef C66_PLATFORMS
#include "csl_cpsw_3gfssAux.h"
#include "csl_cpgmac_slAux.h"
#include "csl_cpsgmiiAux.h"
extern const char *Pa_getVersionStr();
extern const char *Qmss_getVersionStr();
extern const char *Cppi_getVersionStr();
#endif


/* Forward reference */
void CreateIPUse(SOCKET htmlSock);
void CreatehtmlSockets(SOCKET htmlSock);
void CreateRoute(SOCKET htmlSock);


/*************************************************************************
 *  @b serveInformationPage(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
 * 
 *  @n
 *  
 * 	CGI function called by the server to create the Information web page.
 *
 *  @param[in]  
 *  htmlSock - Socket to the browser
 * 
 *  @param[in] 
 *  ContentLength - Length of the page request from the browser
 * 
 *  @param[in] 
 *  pArgs - Not used
 *  
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/
#ifdef C66_PLATFORMS
int32_t serveInformationPage(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
	int32_t 				i;
	char 					szSmall[12];
   	uint32_t 				scalar = 0;

    /* Needed to convert clock ticks into seconds */
    if (scalar == 0) {
    	scalar = 1000000u / Clock_tickPeriod;
    }
    
	html_start(PAGEHEAD);

	html ("<body>");

	html_var (PAGEWRAPPER, "Information");

	html ("<div id='page'><div id='content'>");

	/* Page Header */
	html ("<hr><table cellpadding='6'><tr>");
	html ("<td valign='center'><image src='dspchip.gif'></td>");
	html ("<td width='100%' align='justify'>");
	html ("<h2>Information</h2><br>");
	html ("<p class='howitworks'><b>How it works:</b> A CGI back end function uses API's in the Platform Library, NDK and other components to collect and display the information. </p>");
	html ("</td></tr></table><hr><br>");
	
	/* System Up time */
	html ("<div align='center'>");
	html ("<h3>System Up Time</h3><br>");	
	html ("<br>");
	html ("<table  border='0' cellspacing='0' cellpadding='5'>");
	html_var("<tr><td>%d seconds</td></tr>", ((unsigned long)Clock_getTicks() / scalar));
	html ("</table>");	
	html ("</div><br><br>");
	

	/* Platform Information */
	html ("<div align='center'>");
	html ("<h3>Platform Information</h3><br>");	
	html ("<br>");
	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	html_var("<tr><td>Board</td><td>%s</td></tr>", gPlatformInfo.board_name);
	html_var("<tr><td>Board Revision</td><td>%d</td></tr>", gPlatformInfo.board_rev);
	html_var("<tr><td>CPU Frequency</td><td>%d MHz</td></tr>", gPlatformInfo.frequency);
	html_var("<tr><td>Number of CPU's (Cores)</td><td>%d</td></tr>", gPlatformInfo.cpu.core_count);
	html_var("<tr><td>CPU Revision</td><td>%d</td></tr>", gPlatformInfo.cpu.revision_id);
	html_var("<tr><td>Core we are running on</td><td>%d</td></tr>", platform_get_coreid());	
	
	if (gPlatformInfo.cpu.endian ==  PLATFORM_LE) {
		html_var("<tr><td>Endian Mode</td><td>Little</td></tr>");
	}
	else {
		html_var("<tr><td>Endian Mode</td><td>Big</td></tr>");
	}

	if (platform_get_switch_state(1)) {
		strcpy (szSmall, "On");
	}
	else {
		strcpy (szSmall, "Off");
	}
	html_var("<tr><td>User Switch 1</td><td>%s</td></tr>", szSmall);

#ifndef C66_PLATFORMS
	if (platform_get_switch_state(2)) {
		strcpy (szSmall, "On");
	}
	else {
		strcpy (szSmall, "Off");
	}
	html_var("<tr><td>User Switch 2</td><td>%s</td></tr>", szSmall);
#endif

	html ("</table>");	
	html ("</div>");

	// Program Versions
	html (HDR);
	html ("<div align='center'>");
	html ("<h3>SDK Program Versions</h3><br>" );
	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	html_var("<tr><td>HUA</td><td>%s</td></tr>", BLM_VERSION);
	html_var("<tr><td>Platform Library</td><td>%s</td></tr>", gPlatformInfo.version);
	html_var("<tr><td>CSL</td><td>%s</td></tr>", CSL_versionGetStr());
	//	html_var("<tr><td>NIMU Library</td><td>%s</td></tr>", NIMU_LIB_VERSION);
	html_var("<tr><td>PA LLD</td><td>%s</td></tr>", Pa_getVersionStr());
	html_var("<tr><td>QMSS LLD</td><td>%s</td></tr>", Qmss_getVersionStr());
	html_var("<tr><td>CPPI LLD</td><td>%s</td></tr>", Cppi_getVersionStr());
	html ("</table>");
	html ("</div>");

	// Switch Configuration
	i = 0;
	html (HDR);
	html ("<div align='center'>");
	html_var ("<h3>Switch Configuration (Port %d) 1=Enabled, 0=Disabled</h3><br>", i );
	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	html_var("<tr><td>Full Duplex Enabled</td><td>%d</td></tr>", 	CSL_CPGMAC_SL_isFullDuplexEnabled(i));
	html_var("<tr><td>Gigabit Enabled</td><td>%d</td></tr>", 		CSL_CPGMAC_SL_isGigabitEnabled(i));
	html_var("<tr><td>Force Gigabit Enabled</td><td>%d</td></tr>", 	CSL_CPGMAC_SL_isGigForceModeEnabled(i));
	html_var("<tr><td>Idle Mode Enabled</td><td>%d</td></tr>", 		CSL_CPGMAC_SL_isIdleModeEnabled(i));
	html_var("<tr><td>Loopback Enabled</td><td>%d</td></tr>", 		CSL_CPGMAC_SL_isLoopbackModeEnabled(i));
	html_var("<tr><td>GMII Enabled</td><td>%d</td></tr>", 			CSL_CPGMAC_SL_isGMIIEnabled(i));
	html_var("<tr><td>Rx Flow Control Enabled</td><td>%d</td></tr>",CSL_CPGMAC_SL_isRxFlowControlEnabled(i));
	html_var("<tr><td>TxFlowControl Enabled</td><td>%d</td></tr>",	CSL_CPGMAC_SL_isTxFlowControlEnabled(i));
	html_var("<tr><td>Tx Pacing Enabled</td><td>%d</td></tr>", 		CSL_CPGMAC_SL_isTxPaceEnabled(i));
	html_var("<tr><td>Rx CEF Enabled</td><td>%d</td></tr>",			CSL_CPGMAC_SL_isRxCEFEnabled(i));
	html_var("<tr><td>Rx CSF Enabled</td><td>%d</td></tr>",			CSL_CPGMAC_SL_isRxCSFEnabled(i));
	html_var("<tr><td>Max Receive Frame Length</td><td>%d</td></tr>",		CSL_CPGMAC_SL_getRxMaxLen(i));
	html ("</table>");
	html ("</div>");

	/* IP Information */
	html (HDR);
	html ("<div align='center'>");
	html ("<h3>IP Information</h3><br>");	
	CreateIPUse(htmlSock);
	html ("</div>");

	/* MAC Addresses */
	html (HDR);
	html ("<div align='center'>");
	html ("<h3>MAC Addresses (EFUSE)</h3><br>" );
	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	html ("<tr><td  bgcolor='#C0C0C0'>Port</td><td  bgcolor='#C0C0C0'>MAC</td></tr>");
   for (i=0; i < gPlatformInfo.emac.port_count; i++) {
		html_var("<tr><td>%d</td><td>%02X-%02X-%02X-%02X-%02X-%02X</td></tr>",
		        	i, gPlatformInfo.emac.efuse_mac_address[0], gPlatformInfo.emac.efuse_mac_address[1], 
		        	gPlatformInfo.emac.efuse_mac_address[2], gPlatformInfo.emac.efuse_mac_address[3], 
		        	gPlatformInfo.emac.efuse_mac_address[4], gPlatformInfo.emac.efuse_mac_address[5]);
	}
	html ("</table>");
	html ("</div>");   

	/* Service Information */
	html (HDR);
	html ("<div align='center'>");
	html ("<h3>Service Information</h3><br>");
	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	html ("<tr><td bgcolor='#C0C0C0'>Service</td><td bgcolor='#C0C0C0'>Status</td></tr>");
	for (i=0; i < CFGITEM_SERVICE_MAX; i++) {
		if (ServiceStatus[i].status == CIS_SRV_STATUS_ENABLED) {
			html_var("<tr><td>%s</td><td><font color='#00FF00'>%s</font></td></tr>",
            		ServiceStatus[i].name, StatusStr[ServiceStatus[i].status]);			
		}
		else 
		if (ServiceStatus[i].status == CIS_SRV_STATUS_FAILED) {
			html_var("<tr><td>%s</td><td><font color='#FF0000'>%s</font></td></tr>",
            		ServiceStatus[i].name, StatusStr[ServiceStatus[i].status]);			
		}
		else {
			html_var("<tr><td>%s</td><td>%s</td></tr>",
            		ServiceStatus[i].name, StatusStr[ServiceStatus[i].status]);	
		}	
	}
	html ("</table>");
    html ("</div>");

	/* Socket Information */
	html (HDR);
	html ("<div align='center'>");
	html ("<h3>Socket Information</h3><br>");
	CreatehtmlSockets(htmlSock);
	html ("</div>");

    
	/* TCP IP Routing Table */
	html (HDR);
	html ("<div align='center'>");
	html ("<h3>TCP/IP Current Route Table</h3><br>" );
	CreateRoute(htmlSock);
	html ("</div>");

	html ("</div><div style='clear: both;'>&nbsp;</div></div>");

	html (PAGEFOOTER);

	html_end ("</body></html>");

	/* Send header */
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    /* After this call we MUST send the data since a CRLF is being sent */
	httpSendEntityLength(htmlSock, html_getsize() );

	/* Send the page */
	httpSendClientStr(htmlSock, html_getpage());

	return 1;	
}
#else
int32_t serveInformationPage(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
	int32_t 	i;
	char 		szSmall[12];
   	uint32_t 	scalar = 0;

    /* Needed to convert clock ticks into seconds */
    if (scalar == 0) {
    	scalar = 1000000u / Clock_tickPeriod;
    }

	html_start(PAGEHEAD);

	html ("<body>");

	html_var (PAGEWRAPPER, "Information");

	html ("<div id='page'><div id='content'>");

	/* Page Header */
	html ("<hr><table cellpadding='6'><tr>");
	html ("<td valign='center'><image src='dspchip.gif'></td>");
	html ("<td width='100%' align='justify'>");
	html ("<h2>Information</h2><br>");
	html ("<p class='howitworks'><b>How it works:</b> A CGI back end function uses API's in the Platform Library, NDK and other components to collect and display the information. </p>");
	html ("</td></tr></table><hr><br>");

	/* System Up time */
	html ("<div align='center'>");
	html ("<h3>System Up Time</h3><br>");
	html ("<br>");
	html ("<table  border='0' cellspacing='0' cellpadding='5'>");
	html_var("<tr><td>%d seconds</td></tr>", ((unsigned long)Clock_getTicks() / scalar));
	html ("</table>");
	html ("</div><br><br>");


	/* Platform Information */
	html ("<div align='center'>");
	html ("<h3>Platform Information</h3><br>");
	html ("<br>");
	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	html_var("<tr><td>Board</td><td>%s</td></tr>", gPlatformInfo.board_name);
	html_var("<tr><td>Board Revision</td><td>%d</td></tr>", gPlatformInfo.board_rev);
	html_var("<tr><td>CPU Frequency</td><td>%d MHz</td></tr>", gPlatformInfo.frequency);
	html_var("<tr><td>Number of CPU's (Cores)</td><td>%d</td></tr>", gPlatformInfo.cpu.core_count);
	html_var("<tr><td>CPU Revision</td><td>%d</td></tr>", gPlatformInfo.cpu.revision_id);
	html_var("<tr><td>Core we are running on</td><td>%d</td></tr>", platform_get_coreid());

	if (gPlatformInfo.cpu.endian ==  PLATFORM_LE) {
		html_var("<tr><td>Endian Mode</td><td>Little</td></tr>");
	}
	else {
		html_var("<tr><td>Endian Mode</td><td>Big</td></tr>");
	}

	if (platform_get_switch_state(1)) {
		strcpy (szSmall, "On");
	}
	else {
		strcpy (szSmall, "Off");
	}
	html_var("<tr><td>User Switch 1</td><td>%s</td></tr>", szSmall);
	if (platform_get_switch_state(2)) {
		strcpy (szSmall, "On");
	}
	else {
		strcpy (szSmall, "Off");
	}
	html_var("<tr><td>User Switch 2</td><td>%s</td></tr>", szSmall);

	html ("</table>");
	html ("</div>");

	// Program Versions
	html (HDR);
	html ("<div align='center'>");
	html ("<h3>SDK Program Versions</h3><br>" );
	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	html_var("<tr><td>HUA</td><td>%s</td></tr>", BLM_VERSION);
	html_var("<tr><td>Platform Library</td><td>%s</td></tr>", gPlatformInfo.version);
	html_var("<tr><td>CSL</td><td>%s</td></tr>", CSL_versionGetStr());
	html_var("<tr><td>Ethernet Driver</td><td>%s</td></tr>", emac_get_version());
	html ("</table>");
	html ("</div>");

	/* IP Information */
	html (HDR);
	html ("<div align='center'>");
	html ("<h3>IP Information</h3><br>");
	CreateIPUse(htmlSock);
	html ("</div>");

	/* MAC Addresses */
	html (HDR);
	html ("<div align='center'>");
	html ("<h3>MAC Addresses (EFUSE)</h3><br>" );
	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	html ("<tr><td  bgcolor='#C0C0C0'>Port</td><td  bgcolor='#C0C0C0'>MAC</td></tr>");
   for (i=0; i < gPlatformInfo.emac.port_count; i++) {
		html_var("<tr><td>%d</td><td>%02X-%02X-%02X-%02X-%02X-%02X</td></tr>",
		        	i, gPlatformInfo.emac.efuse_mac_address[0], gPlatformInfo.emac.efuse_mac_address[1],
		        	gPlatformInfo.emac.efuse_mac_address[2], gPlatformInfo.emac.efuse_mac_address[3],
		        	gPlatformInfo.emac.efuse_mac_address[4], gPlatformInfo.emac.efuse_mac_address[5]);
	}
	html ("</table>");
	html ("</div>");

	/* Service Information */
	html (HDR);
	html ("<div align='center'>");
	html ("<h3>Service Information</h3><br>");
	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	html ("<tr><td bgcolor='#C0C0C0'>Service</td><td bgcolor='#C0C0C0'>Status</td></tr>");
	for (i=0; i < CFGITEM_SERVICE_MAX; i++) {
		if (ServiceStatus[i].status == CIS_SRV_STATUS_ENABLED) {
			html_var("<tr><td>%s</td><td><font color='#00FF00'>%s</font></td></tr>",
            		ServiceStatus[i].name, StatusStr[ServiceStatus[i].status]);
		}
		else
		if (ServiceStatus[i].status == CIS_SRV_STATUS_FAILED) {
			html_var("<tr><td>%s</td><td><font color='#FF0000'>%s</font></td></tr>",
            		ServiceStatus[i].name, StatusStr[ServiceStatus[i].status]);
		}
		else {
			html_var("<tr><td>%s</td><td>%s</td></tr>",
            		ServiceStatus[i].name, StatusStr[ServiceStatus[i].status]);
		}
	}
	html ("</table>");
    html ("</div>");

	/* Socket Information */
	html (HDR);
	html ("<div align='center'>");
	html ("<h3>Socket Information</h3><br>");
	CreatehtmlSockets(htmlSock);
	html ("</div>");


	/* TCP IP Routing Table */
	html (HDR);
	html ("<div align='center'>");
	html ("<h3>TCP/IP Current Route Table</h3><br>" );
	CreateRoute(htmlSock);
	html ("</div>");

	html ("</div><div style='clear: both;'>&nbsp;</div></div>");

	html (PAGEFOOTER);

	html_end ("</body></html>");

	/* Send header */
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    /* After this call we MUST send the data since a CRLF is being sent */
	httpSendEntityLength(htmlSock, html_getsize() );

	/* Send the page */
	httpSendClientStr(htmlSock, html_getpage());

	return 1;
}
#endif


/* Displays our stack information */
void CreateIPUse(SOCKET htmlSock)
{
    IPN     myIP;
    IPN     yourIP;
    char    pszmyIP[32];
    char    pszyourIP[32];
    struct  sockaddr_in Info;
    int32_t     InfoLength;
    char    tmpBuf[128];
    HOSTENT *dnsInfo;
    int32_t     rc;

    InfoLength = sizeof(Info);
    getsockname( htmlSock, (PSA)&Info, &InfoLength );
    myIP = Info.sin_addr.s_addr;
    NtIPN2Str( myIP, pszmyIP );

    InfoLength = sizeof(Info);
    getpeername( htmlSock, (PSA)&Info, &InfoLength );
    yourIP = Info.sin_addr.s_addr;
    NtIPN2Str( yourIP, pszyourIP );

	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	
	html (ROW_START);
    html_var("<td>HTTP Server IP Address</td><td>%s</td>", pszmyIP);
    html (ROW_END);

	html (ROW_START);
    html("<td>HTTP Server Hostname</td>");
    rc = DNSGetHostByAddr( myIP, tmpBuf, sizeof(tmpBuf) );
    if( rc )
        html_var("<td>%s</td>", DNSErrorStr(rc) );
    else
    {
        dnsInfo = (HOSTENT*) tmpBuf;
        html_var("<td>%s</td>", dnsInfo->h_name);
    }
    html(ROW_END);

    html(ROW_START);
    html_var("<td>Your IP Address</td><td>%s</td>", pszyourIP);
    html(ROW_END);

    html(ROW_START);
    html("<td>Your Hostname</td>");
    rc = DNSGetHostByAddr( yourIP, tmpBuf, sizeof(tmpBuf) );
    if( rc )
        html_var("<td>%s</td>", DNSErrorStr(rc) );
    else
    {
        dnsInfo = (HOSTENT*) tmpBuf;
        html_var("<td>%s</td>", dnsInfo->h_name);
    }
    html(ROW_END);

    html("</table>");
    
	return;
}


static void DumphtmlSockets( SOCKET htmlSock, uint32_t htmlSockProt );

void CreatehtmlSockets(SOCKET htmlSock)
{

	html("<br>");
    html("<b>TCP Sockets</b>");
    DumphtmlSockets( htmlSock, SOCKPROT_TCP );

	html("<br>");
    html("<b>UDP Sockets</b>");
    DumphtmlSockets( htmlSock, SOCKPROT_UDP );

	return;
}

static const char *States[] = { "CLOSED","LISTEN","SYNSENT","SYNRCVD",
                          "ESTABLISHED","CLOSEWAIT","FINWAIT1","CLOSING",
                          "LASTACK","FINWAIT2","TIMEWAIT" };

static void DumphtmlSockets( SOCKET htmlSock, uint32_t htmlSockProt )
{
    uint8_t  		*pBuf;
    int32_t  		Entries,i;
    SOCKPCB 		*ppcb;
    char    		str[32];
    Error_Block	    errorBlock;

    Error_init(&errorBlock);

    pBuf = (uint8_t *)Memory_alloc(NULL, platform_roundup(2048,PLATFORM_CACHE_LINE_SIZE) , 0, &errorBlock);
    if( !pBuf ) {
        return;
    }

    /* Use llEnter / llExit since we're calling into the stack */
    llEnter();
    Entries = SockGetPcb( htmlSockProt, 2048, pBuf );
    llExit();

    html("<table border='1' cellspacing='0' cellpadding='5'>");

    html(ROW_START);
    html("<td bgcolor='#C0C0C0'>Local IP</td><td bgcolor='#C0C0C0'>LPort</td>");
    html("<td bgcolor='#C0C0C0'>Foreign IP</td><td bgcolor='#C0C0C0'>FPort</td>\r\n");
    if( htmlSockProt == SOCKPROT_TCP )
        html("<td bgcolor='#C0C0C0'>State</td>\r\n");
    html(ROW_END);

    for(i=0; i<Entries; i++)
    {
        ppcb = (SOCKPCB *)(pBuf+(i*sizeof(SOCKPCB)));

        html(ROW_START);
        NtIPN2Str( ppcb->IPAddrLocal, str );
        html_var("<td>%-15s</td><td>%-5u</td>", str, htons(ppcb->PortLocal) );
        NtIPN2Str( ppcb->IPAddrForeign, str );
        html_var("<td>%-15s</td><td>%-5u</td>\r\n", str, htons(ppcb->PortForeign) );
        if( htmlSockProt == SOCKPROT_TCP )
        {
            html_var("<td>%s</td>\r\n",States[ppcb->State]);
        }
        html(ROW_END);
    }

    html("</table>");

    Memory_free(NULL,  pBuf, platform_roundup(2048,PLATFORM_CACHE_LINE_SIZE) );
    
    return;
 
}


void CreateRoute(SOCKET htmlSock)
{
    HANDLE  	hRt,hIF,hLLI;
    uint32_t    wFlags,IFType,IFIdx;
    uint32_t  	IPAddr,IPMask;
    char    	str[32];
    uint8_t   	MacAddr[6];
 

    /* Start walking the tree */
    llEnter();
    hRt = RtWalkBegin();
    llExit();

    html("<table border='1' cellspacing='0' cellpadding='5'>");

    html(ROW_START);
    html("<td bgcolor='#C0C0C0'>Address</td><td bgcolor='#C0C0C0'>Subnet Mask</td>");
    html("<td bgcolor='#C0C0C0'>Flags</td><td bgcolor='#C0C0C0'>Gateway</td>\r\n");
    html(ROW_END);

    /* While there are routes, print the route information */
    while( hRt )
    {
        html(ROW_START);

        /* Get the IP address and IP mask and flags of the route */
        llEnter();
        IPAddr = RtGetIPAddr( hRt );
        IPMask = RtGetIPMask( hRt );
        wFlags = RtGetFlags( hRt );
        hIF    = RtGetIF( hRt );
        if( hIF )
        {
            IFType = IFGetType(hIF);
            IFIdx  = IFGetIndex(hIF);
        }
        else
            IFType = IFIdx = 0;
        llExit();

        /* Print address and mask */
        NtIPN2Str( IPAddr, str );
        html_var( "<td>%-15s</td>", str );
        NtIPN2Str( IPMask, str );
        html_var( "<td>%-15s</td>", str );

        /* Decode flags */
        if( wFlags & FLG_RTE_UP )
            strcpy(str,"U");
        else
            strcpy(str," ");
        if( wFlags & FLG_RTE_GATEWAY )
            strcat(str,"G");
        else
            strcat(str," ");
        if( wFlags & FLG_RTE_HOST )
            strcat(str,"H");
        else
            strcat(str," ");
        if( wFlags & FLG_RTE_STATIC )
            strcat(str,"S");
        else
            strcat(str," ");
        if( wFlags & FLG_RTE_CLONING )
            strcat(str,"C");
        else
            strcat(str," ");
        if( wFlags & FLG_RTE_IFLOCAL )
            strcat(str,"L");
        else
            strcat(str," ");

        html_var("<td>%s</td>", str );

        /* If the route is a gateway, print the gateway IP address as well */
        if( wFlags & FLG_RTE_GATEWAY )
        {
            llEnter();
            IPAddr = RtGetGateIP( hRt );
            llExit();
            NtIPN2Str( IPAddr, str );
            html_var( "<td>%-15s</td>", str );
        }
        /* Else if non-local host route on Ethernet, print ARP entry */
        else if( IFType == HTYPE_ETH &&
                 (wFlags&FLG_RTE_HOST) && !(wFlags&FLG_RTE_IFLOCAL) )
        {
            /* The stack has a MAC address if it has an LLI (link-layer info)
             * object, and LLIGetMacAddr returns 1.
             */
            llEnter();
            if( !(hLLI = RtGetLLI( hRt )) || !LLIGetMacAddr( hLLI, MacAddr, 6 ) )
                llExit();
            else
            {
                llExit();
                html_var("<td>%02X:%02X:%02X:%02X:%02X:%02X</td>",
                           MacAddr[0], MacAddr[1], MacAddr[2],
                           MacAddr[3], MacAddr[4], MacAddr[5] );
            }
        }
        /* Else just print out the interface */
        else if( IFIdx )
        {
            if( wFlags & FLG_RTE_IFLOCAL )
            {
                html_var("<td>local (if-%d)</td>", IFIdx );
            }
            else
            {
                html_var("<td>if-%d</td>", IFIdx );
            }
        }

        html(ROW_END);

        llEnter();
        hRt = RtWalkNext( hRt );
        llExit();
    }
    llEnter();
    RtWalkEnd( 0 );
    llExit();

    html("</table>");

	return;
}


