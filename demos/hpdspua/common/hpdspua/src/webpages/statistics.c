 /*
 * Statistics.c
 *
 * CGI function to display the statistics page
 *
 * Copyright (C) 2010-2011 Texas Instruments Incorporated - http://www.ti.com/ 
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  statistics.c
 *
 *   @brief   
 *      Contains CGI functions to create and display the Statistics web page.
 *
 */
 
#include "hpdspua.h"
#ifdef C66_PLATFORMS
#include "csl_cpsw_3gfAux.h"
#endif


#ifdef C66_PLATFORMS
#pragma DATA_SECTION(gEmacStats,".far:WEBDATA");
CSL_CPSW_3GF_STATS	gEmacStats[MAX_ETHERNET_PORTS];
#else
#pragma DATA_SECTION(gEmacStats,".far:WEBDATA");
EMAC_STATISTICS_T	gEmacStats[MAX_ETHERNET_PORTS];
#endif

/*************************************************************************
 *  @b serveStatisticsPage(SOCKET htmlSock, int ContentLength, char *pArgs )
 * 
 *  @n
 *  
 * 	CGI function called by the server to create the Statistics web page.
 *
 *  @param[in]  
 *  htmlSock - Socket to the browser
 * 
 *  @param[in] 
 *  ContentLength - Length of the page request from the browser
 * 
 *  @param[in] 
 *  pArgs - Not used
 *  
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/
#ifdef C66_PLATFORMS
int32_t serveStatisticsPage(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
	html_start(PAGEHEAD);

	html ("<body>");

	html_var (PAGEWRAPPER, "Statistical Information");

	html ("<div id='page'>");

	/* Page Header */
	html ("<hr><table cellpadding='6'><tr>");
	html ("<td valign='center'><image src='dspchip.gif'></td>");
	html ("<td width='100%' align='justify'>");
	html ("<h2>Ethernet Statistics</h2><br>");
	html ("<p class='howitworks'><b>How it works:</b> A CGI back end function uses an API call via the CSL layer to collect the switch statistics you see displayed. ");
	html (" The statistics are snapshot values.</p>");
	html ("</td></tr></table><hr><br>");

	/* Display the stats */
	html ("<div id='contentscroll'>");
	
	html ("<div style='float: left; margin-left: 20px;'>");
	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	html ("<tr><td colspan='2' bgcolor='#C0C0C0' align='center'>Port 0 Statistics</td></tr>");

	/* For switch implementations */
	CSL_CPSW_3GF_getStats ((CSL_CPSW_3GF_STATS*)&gEmacStats[0]);

	html_var("<tr><td>RxGoodFrames</td><td>%u</td></tr>",		gEmacStats[0].RxGoodFrames);
	html_var("<tr><td>RxOctets</td><td>%u</td></tr>",			gEmacStats[0].RxOctets);
	html_var("<tr><td>RxBCastFrames</td><td>%u</td></tr>",		gEmacStats[0].RxBCastFrames);
	html_var("<tr><td>RxMCastFrames</td><td>%u</td></tr>",		gEmacStats[0].RxMCastFrames);
	html_var("<tr><td>RxCRCErrors</td><td>%u</td></tr>",		gEmacStats[0].RxCRCErrors);
	html_var("<tr><td>RxAlignCodeErrors</td><td>%u</td></tr>",	gEmacStats[0].RxAlignCodeErrors);
	html_var("<tr><td>RxOversized</td><td>%u</td></tr>",		gEmacStats[0].RxOversized);
	html_var("<tr><td>RxJabber</td><td>%u</td></tr></tr>",		gEmacStats[0].RxJabber);
	html_var("<tr><td>RxUndersized</td><td>%u</td></tr>",		gEmacStats[0].RxUndersized);
	html_var("<tr><td>RxFragments</td><td>%u</td></tr>",		gEmacStats[0].RxFragments);
	html_var("<tr><td>RxSOFOverruns</td><td>%u</td></tr>",		gEmacStats[0].RxSOFOverruns);
	html_var("<tr><td>RxMOFOverruns</td><td>%u</td></tr>",		gEmacStats[0].RxMOFOverruns);
	html_var("<tr><td>RxDMAOverruns</td><td>%u</td></tr>",		gEmacStats[0].RxDMAOverruns);
	html_var("<tr><td>TxGoodFrames</td><td>%u</td></tr>",		gEmacStats[0].TxGoodFrames);
	html_var("<tr><td>TxOctets</td><td>%u</td></tr>",			gEmacStats[0].TxOctets);
	html_var("<tr><td>TxBCastFrames</td><td>%u</td></tr>",		gEmacStats[0].TxBCastFrames);
	html_var("<tr><td>TxMCastFrames</td><td>%u</td></tr>",		gEmacStats[0].TxMCastFrames);
	html_var("<tr><td>TxDeferred</td><td>%u</td></tr>",			gEmacStats[0].TxDeferred);
	html_var("<tr><td>TxCollision</td><td>%u</td></tr></tr>",	gEmacStats[0].TxCollision);
	html_var("<tr><td>TxSingleColl</td><td>%u</td>",			gEmacStats[0].TxSingleColl);
	html_var("<tr><td>TxMultiColl</td><td>%u</td></tr></tr>",	gEmacStats[0].TxMultiColl);
	html_var("<tr><td>TxExcessiveColl</td><td>%u</td>",			gEmacStats[0].TxExcessiveColl);
	html_var("<tr><td>TxLateColl</td><td>%u</td></tr></tr>",	gEmacStats[0].TxLateColl);
	html_var("<tr><td>TxUnderrun</td><td>%u</td></tr>",			gEmacStats[0].TxUnderrun);
	html_var("<tr><td>TxCarrierSLoss</td><td>%u</td></tr>",		gEmacStats[0].TxCarrierSLoss);
	html ("</table>");
	html ("</div>");

#ifdef PORT1_STATS
	html ("<div style='float: left; margin-left: 25px;'>");
	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	html ("<tr><td colspan='2' bgcolor='#C0C0C0' align='center'>Port 1/2 Statistics</td></tr>");
	html_var("<tr><td>RxGoodFrames</td><td>%u</td></tr>",		gEmacStats[1].RxGoodFrames);
	html_var("<tr><td>RxOctets</td><td>%u</td></tr>",			gEmacStats[1].RxOctets);
	html_var("<tr><td>RxBCastFrames</td><td>%u</td></tr>",		gEmacStats[1].RxBCastFrames);
	html_var("<tr><td>RxMCastFrames</td><td>%u</td></tr>",		gEmacStats[1].RxMCastFrames);
	html_var("<tr><td>RxCRCErrors</td><td>%u</td></tr>",		gEmacStats[1].RxCRCErrors);
	html_var("<tr><td>RxAlignCodeErrors</td><td>%u</td></tr>",	gEmacStats[1].RxAlignCodeErrors);
	html_var("<tr><td>RxOversized</td><td>%u</td></tr>",		gEmacStats[1].RxOversized);
	html_var("<tr><td>RxJabber</td><td>%u</td></tr></tr>",		gEmacStats[1].RxJabber);
	html_var("<tr><td>RxUndersized</td><td>%u</td></tr>",		gEmacStats[1].RxUndersized);
	html_var("<tr><td>RxFragments</td><td>%u</td></tr>",		gEmacStats[1].RxFragments);
	html_var("<tr><td>RxSOFOverruns</td><td>%u</td></tr>",		gEmacStats[1].RxSOFOverruns);
	html_var("<tr><td>RxMOFOverruns</td><td>%u</td></tr>",		gEmacStats[1].RxMOFOverruns);
	html_var("<tr><td>RxDMAOverruns</td><td>%u</td></tr>",		gEmacStats[1].RxDMAOverruns);
	html_var("<tr><td>TxGoodFrames</td><td>%u</td></tr>",		gEmacStats[1].TxGoodFrames);
	html_var("<tr><td>TxOctets</td><td>%u</td></tr>",			gEmacStats[1].TxOctets);
	html_var("<tr><td>TxBCastFrames</td><td>%u</td></tr>",		gEmacStats[1].TxBCastFrames);
	html_var("<tr><td>TxMCastFrames</td><td>%u</td></tr>",		gEmacStats[1].TxMCastFrames);
	html_var("<tr><td>TxDeferred</td><td>%u</td></tr>",			gEmacStats[1].TxDeferred);
	html_var("<tr><td>TxCollision</td><td>%u</td></tr></tr>",	gEmacStats[1].TxCollision);
	html_var("<tr><td>TxSingleColl</td><td>%u</td>",			gEmacStats[1].TxSingleColl);
	html_var("<tr><td>TxMultiColl</td><td>%u</td></tr></tr>",	gEmacStats[1].TxMultiColl);
	html_var("<tr><td>TxExcessiveColl</td><td>%u</td>",			gEmacStats[1].TxExcessiveColl);
	html_var("<tr><td>TxLateColl</td><td>%u</td></tr></tr>",	gEmacStats[1].TxLateColl);
	html_var("<tr><td>TxUnderrun</td><td>%u</td></tr>",			gEmacStats[1].TxUnderrun);
	html_var("<tr><td>TxCarrierSLoss</td><td>%u</td></tr>",		gEmacStats[1].TxCarrierSLoss);
	html ("</table>");
	html ("</div>");
#endif

	html("<div style='clear: both;'>&nbsp;</div>");

	html ("</div>"); // end Content

	html ("</div"); // end Page

	html (PAGEFOOTER);

	html_end ("</body></html>");

	// Send header
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    // After this call we MUST send the data since a CRLF is being sent
	httpSendEntityLength(htmlSock, html_getsize() );

	// Send the page
	httpSendClientStr(htmlSock, html_getpage());

	return 1;
}
#else
int32_t serveStatisticsPage(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
	EMAC_STATISTICS_T sEmacStats;

	html_start(PAGEHEAD);

	html ("<body>");

	html_var (PAGEWRAPPER, "Statistical Information");

	html ("<div id='page'>");

	/* Page Header */
	html ("<hr><table cellpadding='6'><tr>");
	html ("<td valign='center'><image src='dspchip.gif'></td>");
	html ("<td width='100%' align='justify'>");
	html ("<h2>Ethernet Statistics</h2><br>");
	html ("<p class='howitworks'><b>How it works:</b> A CGI back end function uses an API call via the CSL layer to collect the switch statistics you see displayed. ");
	html (" There are two port groups on the switch. Port 0 and Ports 1/2. Port 0 is between the Packet Accelerator and the Switch. Ports 1 and 2 are for the Ethernet Interface.");
	html (" The statistics are snapshot values.</p>");
	html ("</td></tr></table><hr><br>");

	/* Display the stats */
	html ("<div id='contentscroll'>");

	html ("<div style='float: left; margin-left: 20px;'>");
	html ("<table  border='1' cellspacing='0' cellpadding='5'>");
	html ("<tr><td colspan='2' bgcolor='#C0C0C0' align='center'>Port 0 Statistics</td></tr>");

	memset((void *)&sEmacStats, 0, sizeof (EMAC_STATISTICS_T) * MAX_ETHERNET_PORTS);

	/* For PHY implementations */
	emac_get_stats(0, &sEmacStats);

	/* Accumulate the stats for PHYs.. ideally we would check for overflow */
	gEmacStats[0].RxGoodFrames 		+= sEmacStats.RxGoodFrames;
	gEmacStats[0].RxOctets 			+= sEmacStats.RxOctets;
	gEmacStats[0].RxBCastFrames 	+= sEmacStats.RxBCastFrames;
	gEmacStats[0].RxMCastFrames 	+= sEmacStats.RxMCastFrames;
	gEmacStats[0].RxCRCErrors 		+= sEmacStats.RxCRCErrors;
	gEmacStats[0].RxAlignCodeErrors	+= sEmacStats.RxAlignCodeErrors;
	gEmacStats[0].RxOversized 		+= sEmacStats.RxOversized;
	gEmacStats[0].RxJabber 			+= sEmacStats.RxJabber;
	gEmacStats[0].RxUndersized 		+= sEmacStats.RxUndersized;
	gEmacStats[0].RxFragments 		+= sEmacStats.RxFragments;
	gEmacStats[0].RxFiltered 		+= sEmacStats.RxFiltered;
	gEmacStats[0].RxQOSFiltered 	+= sEmacStats.RxQOSFiltered;
	gEmacStats[0].RxSOFOverruns 	+= sEmacStats.RxSOFOverruns;
	gEmacStats[0].RxMOFOverruns 	+= sEmacStats.RxMOFOverruns;
	gEmacStats[0].RxDMAOverruns 	+= sEmacStats.RxDMAOverruns;
	gEmacStats[0].TxGoodFrames 		+= sEmacStats.TxGoodFrames;
	gEmacStats[0].TxOctets 			+= sEmacStats.TxOctets;
	gEmacStats[0].TxBCastFrames 	+= sEmacStats.TxBCastFrames;
	gEmacStats[0].TxMCastFrames 	+= sEmacStats.TxMCastFrames;
	gEmacStats[0].TxDeferred 		+= sEmacStats.TxDeferred;
	gEmacStats[0].TxCollision 		+= sEmacStats.TxCollision;
	gEmacStats[0].TxSingleColl 		+= sEmacStats.TxSingleColl;
	gEmacStats[0].TxMultiColl 		+= sEmacStats.TxMultiColl;
	gEmacStats[0].TxExcessiveColl 	+= sEmacStats.TxExcessiveColl;
	gEmacStats[0].TxLateColl 		+= sEmacStats.TxLateColl;
	gEmacStats[0].TxUnderrun 		+= sEmacStats.TxUnderrun;
	gEmacStats[0].TxCarrierSLoss 	+= sEmacStats.TxCarrierSLoss;

	html_var("<tr><td>RxGoodFrames</td><td>%u</td></tr>",		gEmacStats[0].RxGoodFrames);
	html_var("<tr><td>RxOctets</td><td>%u</td></tr>",			gEmacStats[0].RxOctets);
	html_var("<tr><td>RxBCastFrames</td><td>%u</td></tr>",		gEmacStats[0].RxBCastFrames);
	html_var("<tr><td>RxMCastFrames</td><td>%u</td></tr>",		gEmacStats[0].RxMCastFrames);
	html_var("<tr><td>RxCRCErrors</td><td>%u</td></tr>",		gEmacStats[0].RxCRCErrors);
	html_var("<tr><td>RxAlignCodeErrors</td><td>%u</td></tr>",	gEmacStats[0].RxAlignCodeErrors);
	html_var("<tr><td>RxOversized</td><td>%u</td></tr>",		gEmacStats[0].RxOversized);
	html_var("<tr><td>RxJabber</td><td>%u</td></tr></tr>",		gEmacStats[0].RxJabber);
	html_var("<tr><td>RxUndersized</td><td>%u</td></tr>",		gEmacStats[0].RxUndersized);
	html_var("<tr><td>RxFragments</td><td>%u</td></tr>",		gEmacStats[0].RxFragments);
	html_var("<tr><td>RxSOFOverruns</td><td>%u</td></tr>",		gEmacStats[0].RxSOFOverruns);
	html_var("<tr><td>RxMOFOverruns</td><td>%u</td></tr>",		gEmacStats[0].RxMOFOverruns);
	html_var("<tr><td>RxDMAOverruns</td><td>%u</td></tr>",		gEmacStats[0].RxDMAOverruns);
	html_var("<tr><td>TxGoodFrames</td><td>%u</td></tr>",		gEmacStats[0].TxGoodFrames);
	html_var("<tr><td>TxOctets</td><td>%u</td></tr>",			gEmacStats[0].TxOctets);
	html_var("<tr><td>TxBCastFrames</td><td>%u</td></tr>",		gEmacStats[0].TxBCastFrames);
	html_var("<tr><td>TxMCastFrames</td><td>%u</td></tr>",		gEmacStats[0].TxMCastFrames);
	html_var("<tr><td>TxDeferred</td><td>%u</td></tr>",			gEmacStats[0].TxDeferred);
	html_var("<tr><td>TxCollision</td><td>%u</td></tr></tr>",	gEmacStats[0].TxCollision);
	html_var("<tr><td>TxSingleColl</td><td>%u</td>",			gEmacStats[0].TxSingleColl);
	html_var("<tr><td>TxMultiColl</td><td>%u</td></tr></tr>",	gEmacStats[0].TxMultiColl);
	html_var("<tr><td>TxExcessiveColl</td><td>%u</td>",			gEmacStats[0].TxExcessiveColl);
	html_var("<tr><td>TxLateColl</td><td>%u</td></tr></tr>",	gEmacStats[0].TxLateColl);
	html_var("<tr><td>TxUnderrun</td><td>%u</td></tr>",			gEmacStats[0].TxUnderrun);
	html_var("<tr><td>TxCarrierSLoss</td><td>%u</td></tr>",		gEmacStats[0].TxCarrierSLoss);
	html ("</table>");	
	html ("</div>");

	html("<div style='clear: both;'>&nbsp;</div>");

	html ("</div>"); // end Content

	html ("</div"); // end Page

	html (PAGEFOOTER);

	html_end ("</body></html>");

	// Send header
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    // After this call we MUST send the data since a CRLF is being sent
	httpSendEntityLength(htmlSock, html_getsize() );

	// Send the page
	httpSendClientStr(htmlSock, html_getpage());

	return 1;	
}
#endif

