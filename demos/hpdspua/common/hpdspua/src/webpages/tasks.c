 /*
 * Tasks.c
 *
 * CGI function to display the Task List page
 *
 * Copyright (C) 2010-2011 Texas Instruments Incorporated - http://www.ti.com/
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  tasks.c
 *
 *   @brief   
 *      Contains CGI functions to create the Task List web page and
 *      serve it back to a browser.
 *
 */
 
#include "hpdspua.h"
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/knl/Task.h>



/*
 *  ======== dumpTask ========
 *
 *  Generate HTML table row for a task
 *
 */
static void dumpTask(Task_Handle task)
{
    Task_Stat 	status;
    Char		state[40];

    Task_stat(task, &status);

	switch (status.mode) {
		case ti_sysbios_knl_Task_Mode_RUNNING: strcpy (state, "Running"); break;
    	case ti_sysbios_knl_Task_Mode_READY:	strcpy (state, "Ready"); break;
    	case ti_sysbios_knl_Task_Mode_BLOCKED:	strcpy (state, "Blocked"); break;
    	case ti_sysbios_knl_Task_Mode_TERMINATED: strcpy (state, "Terminated"); break;
    	case ti_sysbios_knl_Task_Mode_INACTIVE:	strcpy (state, "Inactive"); break;
    	default: 	sprintf (state, "%d", status.mode); break;
	}

    html_var("<tr><td>0x%x %s</td><td>%d</td><td>%s</td><td>%d</td><td>%d</td></tr>",
        task, Task_Handle_name(task),
        status.priority, state, status.stackSize, status.used);

    return;
}

/*************************************************************************
 *  @b serveTaskPage(SOCKET htmlSock, int ContentLength, char *pArgs )
 * 
 *  @n
 *  
 * 	CGI function called by the server to create the Task List web page.
 *
 *  @param[in]  
 *  htmlSock - Socket to the browser
 * 
 *  @param[in] 
 *  ContentLength - Length of the page request from the browser
 * 
 *  @param[in] 
 *  pArgs - Not used
 *  
 *  @retval
 *  CGI Functions must return 1 if the socket is left open,
 *  and zero if the socket is closed. This example always
 *  returns 1.
 ************************************************************************/

int serveTaskPage(SOCKET htmlSock, int32_t ContentLength, char *pArgs )
{
	int32_t			i;
	Task_Object 	*task;
 
   

	html_start(PAGEHEAD);

	html ("<body>");

	html_var (PAGEWRAPPER, "Task List");

	html ("<div id='page'><div id='content'>");

	// Page Header
	html ("<hr><table cellpadding='6'><tr>");
	html ("<td valign='center'><image src='dspchip.gif'></td>");
	html ("<td width='100%' align='justify'>");
	html ("<h2>Task List</h2><br>");
	html ("<p class='howitworks'><b>How it works:</b> A CGI back end function uses API's in SYS/BIOS to collect and display information about the tasks that are currently running. </p>");
	html ("</td></tr></table><hr><br>");
	html (HDR);
		
	//Task Information
	html ("<div align='center'>");
 
	html ("<table border='1' cellspacing='0' cellpadding='5'>");

    html ("<tr><td bgcolor='#C0C0C0'>Task</td><td bgcolor='#C0C0C0'>Priority</td><td bgcolor='#C0C0C0'>State</td><td bgcolor='#C0C0C0'>Stack Size</td><td bgcolor='#C0C0C0'>Stack Used</td></tr>");

    for (i = 0; i < Task_Object_count(); i++) {
        task = Task_Object_get(NULL, i);
        dumpTask(task);
    }
    	
    task = Task_Object_first();
    while (task) {
        dumpTask(task);
        task = Task_Object_next(task);
    }
    	
    html ("</table>");

	html ("</div>");

	html ("</div><div style='clear: both;'>&nbsp;</div></div>");

	html (PAGEFOOTER);

	html_end ("</body></html>");

	// Send header
	httpSendStatusLine(htmlSock, HTTP_OK, CONTENT_TYPE_HTML);

    // After this call we MUST send the data since a CRLF is being sent
	httpSendEntityLength(htmlSock, html_getsize() );

	// Send the page
	httpSendClientStr(htmlSock, html_getpage());

	return 1;	
}


