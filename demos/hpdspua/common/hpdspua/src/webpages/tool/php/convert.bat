

REM Convert HTML pages into their corresponding hex array header files.

pause

php hexwriter.php bench.html
php hexwriter.php credit.html
php hexwriter.php i2c.html
php hexwriter.php index.html
php hexwriter.php default.css
php hexwriter.php ndkparms.html
php hexwriter.php ndkloopstart.html
php hexwriter.php dspchip.gif
php hexwriter.php waiting.gif
php hexwriter.php C6678_small.gif
php hexwriter.php C6670_small.gif
php hexwriter.php C6457_small.gif
php hexwriter.php C6472_small.gif
php hexwriter.php C6474_small.gif
php hexwriter.php NDKBenchmark.jar
pause