 /*
 * webpage.c
 *
 * Web page and CGI functions
 *
 * Copyright (C) 2009-2011 Texas Instruments Incorporated - http://www.ti.com/ 
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  webpage.c
 *
 *   @brief   
 *      Contains routines to configure the web pages in our HTTP server.
 * 		It also contains the functions for parsing POST variables and for 
 *  	creating dynamic web pages.
 *
 */
 
#include "hpdspua.h"

/* Turn on for benchmark applets */
#define APPLETS 1

/*
 * Every static web page and its associated content are pre-compiled into hex since we do not 
 * have a flash file system to read them from. Dynamic web pages (those with a .cgi) extension
 * are built dynamically from C code.
*/
#pragma DATA_SECTION(INDEXHTML,".far:WEBDATA");
#include "./webpages/indexhtml.h"

#pragma DATA_SECTION(DEFAULTCSS,".far:WEBDATA");
#include "./webpages/defaultcss.h"

#pragma DATA_SECTION(BENCHHTML,".far:WEBDATA");
#include "./webpages/benchhtml.h"
#pragma DATA_SECTION(NDKPARMSHTML,".far:WEBDATA");
#include "./webpages/ndkparmshtml.h"
#if APPLETS
#pragma DATA_SECTION(NDKBENCHMARKJAR,".far:WEBDATA");
#include "./webpages/NDKBenchmarkjar.h"
#endif

#pragma DATA_SECTION(NDKLOOPSTARTHTML,".far:WEBDATA");
#include "./webpages/ndkloopstarthtml.h"

#pragma DATA_SECTION(CREDITHTML,".far:WEBDATA");
#include "./webpages/credithtml.h"

#pragma DATA_SECTION(I2CHTML,".far:WEBDATA");
#include "./webpages/i2chtml.h"

#pragma DATA_SECTION(INFOHTML,".far:WEBDATA");
#include "./webpages/infohtml.h"

#pragma DATA_SECTION(BANNER, ".far:WEBDATA");
#include "./webpages/banner.h"

#pragma DATA_SECTION(IMAGE1, ".far:WEBDATA");
#pragma DATA_SECTION(IMAGE2, ".far:WEBDATA");
#pragma DATA_SECTION(IMAGE3, ".far:WEBDATA");
#include "./webpages/images.h"

#pragma DATA_SECTION(DSPCHIPGIF, ".far:WEBDATA");
#include "./webpages/dspchipgif.h"

#pragma DATA_SECTION(WAITINGGIF, ".far:WEBDATA");
#include "./webpages/waitinggif.h"

//#ifdef C66_PLATFORMS
#if defined(C66_PLATFORMS) || defined(C665_PLATFORMS)
#pragma DATA_SECTION(C6678_SMALLGIF, ".far:WEBDATA");
#include "./webpages/C6678_smallgif.h"
#pragma DATA_SECTION(C6670_SMALLGIF, ".far:WEBDATA");
#include "./webpages/C6670_smallgif.h"
#else
#pragma DATA_SECTION(C6472_SMALLGIF, ".far:WEBDATA");
#include "./webpages/C6472_smallgif.h"
#pragma DATA_SECTION(C6474_SMALLGIF, ".far:WEBDATA");
#include "./webpages/C6474_smallgif.h"
#pragma DATA_SECTION(C6457_SMALLGIF, ".far:WEBDATA");
#include "./webpages/C6457_smallgif.h"
#endif

#pragma DATA_SECTION(LOGOBAR, ".far:WEBDATA");
#include "./webpages/logobar.h" 

/*
 * We use this buffer for processing the image upload POSTs
*/
#pragma DATA_SECTION ( gRxBuffer, ".gBuffer" )
uint8_t 	gRxBuffer[MAX_POST_FILESZ];

/*
 *  Standard bits of HTML used by pages we build dynamically
*/
char *PAGEHEAD = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'> \
<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Pragma' CONTENT='no-cache' /> <meta http-equiv='content-type' content='text/html; charset=utf-8' /> \
<title>High Performance Digital Signal Processer Utility</title><link rel='stylesheet' href='default.css' type='text/css' /></head>";

char *PAGEWRAPPER = "<div id='wrapper'><div id='logo'><h1>%s</h1> \
</div><div id='header'><div id='menu'><ul> \
<li><a href='index.html'>Welcome</a></li> \
<li><a href='info.cgi'>Information</a></li> \
<li><a href='stats.cgi'>Statistics</a></li> \
<li><a href='tasks.cgi'>Task List</a></li> \
<li><a href='bench.html'>Benchmarks</a></li> \
<li><a href='diag.cgi'>Diagnostics</a></li> \
<li><a href='flash.cgi'>Flash</a></li> \
<li class='last'><a href='I2C.html'>EEPROM</a></li> \
</ul></div></div></div>";

char *PAGEWRAPPERBACK = "<div id='wrapper'><div id='logo'><h1>%s</h1> \
</div><div id='header'><div id='menu'><ul><li><a onclick='history.go(-1);'>Back</a></li> \
</ul></div></div></div>";

char *PAGEFOOTER 	= "<div id='footer'><p id='legal'>( c ) 2010-2011 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='credit.html'>Credits</a></p></div>";
char *MAINHDR 		= "<br><hr><br>";
char *HDR 			= "<br><br>";
char *ROW_END 		= "</tr>";
char *ROW_START 	= "<tr>";

/*************************************************************************
 *  @b AddWebFiles()
 * 
 *  @n
 *  
 * 	Configures the web server with our pages. The pages we serve use the 
 *  embedded file system sample code that comes with NDK. The EFS provides 
 *  a simple memory based file system with high level file operations
 *  like open, close and read.
 * 
 *  @param[in]  
 *  None 
 * 
 *  @retval
 *      None
 ************************************************************************/
void AddWebFiles(void)
{
	void *pFxn;

	/* Index Page */
    efs_createfile("index.html", INDEXHTML_SIZE, INDEXHTML);

	/* Style Sheet */
	efs_createfile("default.css", DEFAULTCSS_SIZE, DEFAULTCSS);

	/* Task List */
    pFxn = (void*) &serveTaskPage;
    efs_createfile("tasks.cgi", 0, (UINT8 *) pFxn);

	/* Benchmark Pages */
    efs_createfile("bench.html",BENCHHTML_SIZE,BENCHHTML);
	efs_createfile("ndkparms.html",NDKPARMSHTML_SIZE,NDKPARMSHTML);
#if APPLETS
	efs_createfile("NDKBenchmark.jar",NDKBENCHMARKJAR_SIZE,NDKBENCHMARKJAR);
#endif
	pFxn = (void*) &ndkThroughputBench;
	efs_createfile("benchmark.cgi", 0, (UINT8 *) pFxn);

	efs_createfile("ndkloopstart.html",NDKLOOPSTARTHTML_SIZE,NDKLOOPSTARTHTML);
	pFxn = (void*) &ndkLoopbackStart;
	efs_createfile("ndkloopstart.cgi", 0, (UINT8 *) pFxn);
	pFxn = (void*) &ndkLoopbackStop;
	efs_createfile("lbstop.cgi", 0, (UINT8 *) pFxn);

	/* Information Page */
    pFxn = (void*) &serveInformationPage;
    efs_createfile("info.cgi", 0, (UINT8 *) pFxn);

	/* Flash Pages */
    pFxn = (void*) &serveFlashPage;
    efs_createfile("flash.cgi", 0, (UINT8 *) pFxn);
    pFxn = (void*) &serveFlashWritePage;
    efs_createfile("flashwrite.cgi", 0, (UINT8 *) pFxn);
    pFxn = (void*) &serveFlashReadPage;
    efs_createfile("flashread.cgi", 0, (UINT8 *) pFxn);
    pFxn = (void*) &serveFlashErasePage;
    efs_createfile("flasherase.cgi", 0, (UINT8 *) pFxn);

	/* Statistics Page */
    pFxn = (void*) &serveStatisticsPage;
    efs_createfile("stats.cgi", 0, (UINT8 *) pFxn);

	/* EEPROM Pages */
	efs_createfile("i2c.html",I2CHTML_SIZE,I2CHTML);
    /* pFxn = (void*) &serveEepromWritePage;
    efs_createfile("eepromwrite.cgi", 0, (UINT8 *) pFxn);*/
    pFxn = (void*) &serveEepromReadPage;
    efs_createfile("eepromread.cgi", 0, (UINT8 *) pFxn);

	/* Credit Page */
	efs_createfile("credit.html",CREDITHTML_SIZE,CREDITHTML);

	/* Diagnostics Page */
	pFxn = (void*) &serveDiagnosticPage;
    efs_createfile("diag.cgi", 0, (UINT8 *) pFxn);
	pFxn = (void*) &processDiagnosticRequest;
    efs_createfile("diagresult.cgi", 0, (UINT8 *) pFxn);

	/* Load Images */
    efs_createfile("logobar.gif", LOGOBAR_SIZE, LOGOBAR);
    efs_createfile("dspchip.gif", DSPCHIPGIF_SIZE, DSPCHIPGIF);
    efs_createfile("img01.jpg", IMAGE1_SIZE, IMAGE1);
    efs_createfile("img02.jpg", IMAGE2_SIZE, IMAGE2);
    efs_createfile("img03.jpg", IMAGE3_SIZE, IMAGE3);
    efs_createfile("waiting.gif", WAITINGGIF_SIZE, WAITINGGIF);
//#ifdef C66_PLATFORMS
#if defined(C66_PLATFORMS) || defined(C665_PLATFORMS)
	efs_createfile("C6678_small.gif", C6678_SMALLGIF_SIZE, C6678_SMALLGIF);
	efs_createfile("C6670_small.gif", C6670_SMALLGIF_SIZE, C6670_SMALLGIF);
#else
	efs_createfile("C6472_small.gif", C6472_SMALLGIF_SIZE, C6472_SMALLGIF);
	efs_createfile("C6474_small.gif", C6474_SMALLGIF_SIZE, C6474_SMALLGIF);
	efs_createfile("C6457_small.gif", C6457_SMALLGIF_SIZE, C6457_SMALLGIF);
#endif
	return;
}

/*************************************************************************
 *  @b RemoveWebFiles()
 * 
 *  @n
 *  
 * 	Removes the web pages form our server and frees up the Embedded File
 *  System entries. 
 * 
 *  @param[in]  
 *  None 
 * 
 *  @retval
 *      None
 ************************************************************************/
void RemoveWebFiles(void)
{
    efs_destroyfile("index.html");
    efs_destroyfile("demo.html");
    efs_destroyfile("tasks.cgi");
    efs_destroyfile("default.css");
    efs_destroyfile("info.cgi");
    efs_destroyfile("bench.html");
	efs_destroyfile("ndkparms.html");
    efs_destroyfile("benchmark.cgi");
    efs_destroyfile("ndkloopstart.html");
    efs_destroyfile("ndkloopstart.cgi");
    efs_destroyfile("lbstop.cgi");
#if APPLETS
	efs_destroyfile("NDKBenchmark.jar");
#endif
    efs_destroyfile("diag.cgi");
    efs_destroyfile("diagresult.cgi");
    efs_destroyfile("stats.cgi");
    efs_destroyfile("flashwrite.cgi");
    efs_destroyfile("flash.cgi");
    efs_destroyfile("flashread.cgi");
    efs_destroyfile("flasherase.cgi");
    efs_destroyfile("eepromread.cgi");
    efs_destroyfile("i2c.html");
    efs_destroyfile("credit.html");
    efs_destroyfile("inform.cgi");
    efs_destroyfile("logobar.gif");
    efs_destroyfile("dspchip.gif");
    efs_destroyfile("img01.jpg");
    efs_destroyfile("img02.jpg");
    efs_destroyfile("img03.jpg");
    efs_destroyfile("waiting.gif");
#ifdef C66_PLATFORMS
	efs_destroyfile("C6678_small.gif");
	efs_destroyfile("C6670_small.gif");
#else
	efs_destroyfile("C6472_small.gif");
	efs_destroyfile("C6474_small.gif");
	efs_destroyfile("C6457_small.gif");
#endif

	return;
}

