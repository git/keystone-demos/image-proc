 /*
 * webserver.c
 *
 * Web server helper functions 
 *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  webserver.c
 *
 *   @brief   
 * 		Contains routines for parsing POST variables and for 
 *  	creating dynamic web pages.
 *
 */

#include "hpdspua.h"


/* Turn on to debug POST processing routines */
#define DEBUG_POST	0
#define DEBUG_POST_DETAILED 0

/* Maximum size of a web page we build dynamically */
#define MAX_PAGE_SIZE	1000000


/*
 * We build some web pages dynamically from the code. When we do that,
 * we use this buffer. Upside is that this is simple for an example. 
 * Downside is that its not mutli-threaded... Buffer for building a web page dynamically
*/
#pragma DATA_SECTION(htmlbuf,".far:WEBDATA");
char  htmlbuf[MAX_PAGE_SIZE];
static uint32_t EntityLength;		/* size of the page we will send - calculated dynamically 	*/
static char 		*htmlbufIndex;		/* Index into the page as we build it 						*/

/* Storage for POST variables */
#pragma DATA_SECTION(POST_names,".far:WEBDATA");
char			POST_names[MAX_POST_VARS][MAX_POST_NAME_LENGTH];

#pragma DATA_SECTION(POST_values,".far:WEBDATA");
char			POST_values[MAX_POST_VARS][MAX_POST_VALUE_LENGTH];

#pragma DATA_SECTION(POST_files,".far:WEBDATA");
post_file_s	POST_files[MAX_POST_FILES];

static uint32_t	nPostVars;	/* The number of Post Variables that were in the Form */

/* forward references */
static int32_t 	multipartParser(uint8_t *, int32_t, uint32_t);
static int32_t 	myreadline(char *, int32_t , uint8_t [], int32_t* );
static int32_t 	parseKeyValuePairs(char* );
static char* 	getformfield(char[], char* , char *, int32_t);



/*************************************************************************
 *  @b html_*(char *str)
 * 
 *  @n
 *  
 * 	This html_* routines are used to create a web page dynmically which 
 *  can be sent back to the browser. When creating a web page, we buffer 
 *  the entire page so it can be sent back at one time.
 * 
 *  html_start 	- The first routine you should call. It intiializes 
 *                the page buffer.
 * 
 *  html		- Writes a constant bit of html, ie html("<p>this is a para</p>")
 *  html_var	- Write HTML with printf style args, ie html_var("<p> para with a number %d</p>", 6)
 * 
 *  html_end 	- Called to end building the page. Use for the very last line of 
 *                html.
 *
 *  html_getpage - Retruns a pointer to the page you just built (so you can send it)
 * 
 *  html_getsize - Returns the size of the page you built. Used to set the ContentLength
 *                 for the page you are serving back
 * 
 *  @retval
 *      None
 ************************************************************************/

void html_start(char *str) {

    EntityLength = 0;
 	htmlbufIndex = &htmlbuf[0];
	memset ( (void *) htmlbufIndex, 0x20, MAX_PAGE_SIZE);
	html(str);
	return;
}

void html(char *str) {
	uint32_t	size;
	
	size = strlen(str);

	if ((EntityLength+size) > MAX_PAGE_SIZE) {
		platform_write( "The web page you are building is too large for the max buffer size of %d \n", MAX_PAGE_SIZE);
		return;
	}

	memcpy (htmlbufIndex, str, size);	
	htmlbufIndex += size;
	EntityLength += size;

	return;	
}

static char myScratchBuffer[1024];

void html_var(const char *fmt, ...) {
    va_list arg_ptr;

    va_start( arg_ptr, fmt );
    (void) vsprintf( myScratchBuffer, fmt, arg_ptr );
    va_end( arg_ptr );
    
    html(myScratchBuffer);
 
	return;	
}

void html_end(char *str) {
	uint32_t	size;

	size = strlen(str);

	if ((EntityLength+size) > MAX_PAGE_SIZE) {
		platform_write( "The web page you are building is too large for the max buffer size of %d \n", MAX_PAGE_SIZE);
		return;
	}

	memcpy (htmlbufIndex, str, size);
	EntityLength += size;

	return;	
}

uint32_t html_getsize() {
	return EntityLength;	
}

char *html_getpage() {
	return htmlbuf;	
}


/*************************************************************************
 *  @b  html_processPost(SOCKET htmlSock, int32_t ContentLength)
 * 
 *  @n
 *  
 * 	Process a POST from a web page. Does not handle multi-part mime. There
 *  is a seperate POST process routine for that (cgiParseMulti).
 * 
 *  @param[in]  
 *  htmlSock	- Socket that request came in on
 * 
 *  @param[in]  
 *  ContentLength - Length of the POST as set by the browser
 * 
 *  @retval
 *  Returns 0 on Fail and 1 on Success 
 ************************************************************************/

int32_t html_processPost(SOCKET htmlSock, int32_t ContentLength) {
    char    		*buffer, *key, *value;
    int32_t     	len;
    int32_t     	parseIndex;
    Error_Block	    errorBlock;

	buffer = NULL;

	/* Do a sanity check on content length .. need to figure out a real size here */
	if (ContentLength > 512) {
		platform_write( "Content Length exceeds our max of %d \n", 512);
		goto ERROR;
	}

	/*
	** CGI Functions can now support URI arguments as well if the
    ** pArgs pointer is not NULL, and the ContentLength were zero,
    **  we could parse the arguments off of pArgs instead.
    */

	Error_init(&errorBlock);

    /* First, allocate a buffer for the request */
    buffer = (char*) Memory_alloc(NULL, platform_roundup(ContentLength + 1, PLATFORM_CACHE_LINE_SIZE), PLATFORM_CACHE_LINE_SIZE, &errorBlock );
    if ( !buffer ) {
		platform_write( "Out of memory in html_ProcessPost\n");
        goto ERROR;
    }

    /* Setup to parse the post data */
    parseIndex = 0;
    nPostVars = 0;
    buffer[ContentLength] = '\0';

    /* Now read the data from the client */
    len = recv( htmlSock, buffer, ContentLength, MSG_WAITALL );
    if ( len < 1 ) {
		/*platform_write( "recv failed (read nothing) in html_ProcessPost \n");*/
    	Memory_free(NULL, buffer, platform_roundup(ContentLength + 1, PLATFORM_CACHE_LINE_SIZE));
        return 1;
    }

    /* Process request variables until there are none left */
    do
    {
        key   = cgiParseVars( buffer, &parseIndex );
        value = cgiParseVars( buffer, &parseIndex );

		if ((value != NULL) && (key != NULL)) {
			nPostVars++;
	
			/* Sanity check lengths */
			if ((strlen(key) > MAX_POST_NAME_LENGTH) || (strlen(value) > MAX_POST_VALUE_LENGTH)) {
				platform_write( "A name (%s) or value field (%s) exceeds our length. \n", 
								key, value);
				goto ERROR;
			}

#if DEBUG_POST	
		    platform_write("key = %s  value = %s \n", key, value);
#endif

			/* Save the variables as a name,value pair (adjust for zero based array) */
			strcpy ((char *) &POST_names[nPostVars-1][0], key);
			strcpy ((char *) &POST_values[nPostVars-1][0], value);
		
			/* we can only hold so many, sanity check it */
			if (nPostVars >= MAX_POST_VARS) {
				platform_write( "Exceeding our max number of POST variables which is %d \n", MAX_POST_VARS);
				goto ERROR;
			}
		}
    } while ( parseIndex != -1 );

	if( buffer ) {
		Memory_free(NULL, buffer, platform_roundup(ContentLength + 1, PLATFORM_CACHE_LINE_SIZE));
	}

	return 1;

ERROR:

	http405(htmlSock); /* Tell the client we could not process the post */

	if( buffer ) {
		Memory_free(NULL, buffer, platform_roundup(ContentLength + 1, PLATFORM_CACHE_LINE_SIZE));
	}

	return 0;
}


/*************************************************************************
 *  @b  html_getValueFor(char *name)
 * 
 *  @n
 *  
 * 	Returns the value associated with a named field from a POST.
 * 
 *  @param[in]  
 *  name	- The name of the field you want the value for
 * 
 * 
 *  @retval
 *  Pointer to the value or NULL if nameis not found
 ************************************************************************/
char *html_getValueFor(char *name) {
    int32_t     i;
	char	*value;
 
	value = NULL;
 
	for (i=0; i < nPostVars; i++) {
		if (strcmp((char *) &POST_names[i][0], name) == 0) {
			value = (char *) &POST_values[i][0];
			break;
		}
	}

	return value;
}

/*************************************************************************
 *  @b  html_getNumberPostVars()
 * 
 *  @n
 *  
 * 	Returns the number of Post Variables.
 * 
 *  @retval
 *  The number of Post Variables in the form just processed
 ************************************************************************/
uint32_t  html_getNumberPostVars() {
	return nPostVars;
}

/*************************************************************************
 *  @b  html_processFileUpload(SOCKET htmlSock, int32_t ContentLength, char *pBuf )
 * 
 *  @n
 *  
 *  This is our routine to process a multi-part MIME post (the kind 
 *  you use when posting a file). Ideally we would have one parser for 
 *  all types of POSTs but they can quickly become rather complicated to 
 *  implement so its eaiser to have this one and html_processPost. 
 * 
 * 
 *  @retval
 *   HTML_FATAL 		Internal error of some sort.
 *   HTML_TOO_LARGE 	If the Posted file exceeds our MAX_POST_FILESZ
 *   HTML_PARSER_ERROR 	HTML Parser error
 *   length 			The size of the file that was posted.
 *
 ***************************************************************************/

int32_t html_processFileUpload(SOCKET htmlSock, int32_t ContentLength, uint8_t *pBuf, uint32_t maxfilesize )
{
	int32_t len;

	/* Initialize global parsing variables */
	nPostVars 		= 0;
	memset (POST_files, 0, sizeof(POST_files));

#if DEBUG_POST
	platform_write( "ContentLength = %d \n", ContentLength);
#endif
  
    /* We need to be sure we dont blow our buffer here by a file that is too large. */
    if (ContentLength > maxfilesize) {
		platform_write("Post exceeds our max size of %d bytes\n", maxfilesize);
    	return (HTML_TOO_LARGE);
    }

	/* 
	* Read in the data. We should probably read this in little chunks in case ContentLength was a lie or
	* there is an error...
	*/
	len = 0;
	while (len < ContentLength ){
		/* This is a work around for IE 7.0 and 8.0 */
    	len = recv( htmlSock, pBuf, ContentLength, MSG_WAITALL );
        /*platform_write("recv with MSGWAITALL %d: %d\n",fdError() );*/	
	}

    if((len = multipartParser(pBuf, ContentLength, maxfilesize))<=0) {
 		platform_write("Parser retunred an error for the POST \n");
    	return (HTML_PARSER_ERROR);	
    }

   
    platform_write("File upload of length %d bytes\n",len);

    return( len );
}

#define BOUNDARY_TAG_SZ		100			/* Max size for a boundary tag we will support 	*/
#define	MMPARSER_LINE_SZ	1024		/* One single line of the Mime Form 			*/

static int32_t multipartParser(uint8_t* rawData, int32_t ContentLength, uint32_t maxfilesize)
{
	int32_t 	i;
	int32_t		fReadFormFields	= 1;
	int32_t		fptr 			= 0;	/* Indexes through the rawData buffer 	*/
	uint32_t	imgsize	= 0;			/* Size of the image uploaded			*/
	uint32_t  	hdrsize	= 0;			/* Size of the POST header, fields, etc	*/
	int32_t 	kvcount 		= 0;	/* Number of Post variables			*/
	char		*boundary		= NULL;	/* Boundary tag used by the Form		*/
	char		*boundaryend	= NULL;	/* Ending boundary tag used by the form */
	char		*line			= NULL;	/* One line of the posted form			*/
	char		*fieldname		= NULL;	/* Name of the posted field				*/
	Error_Block	errorBlock;

	/* Initialize the error block - passed when allocating memory  */
	Error_init(&errorBlock);

    /* 
     * Allocate buffers we need for parsing the form.
     */
    boundary = (char*) Memory_alloc(NULL, platform_roundup(BOUNDARY_TAG_SZ+1, PLATFORM_CACHE_LINE_SIZE), PLATFORM_CACHE_LINE_SIZE, &errorBlock); /* allow for NULL termination */
    if ( !boundary ) {
		platform_write( "Out of memory in multipart parser.\n");
        goto PARSERERROR;
    }
	memset (boundary, 0, BOUNDARY_TAG_SZ+1);
	
    boundaryend = (char*) Memory_alloc(NULL, platform_roundup(BOUNDARY_TAG_SZ+3,PLATFORM_CACHE_LINE_SIZE), PLATFORM_CACHE_LINE_SIZE, &errorBlock); /* allow for NULL termination and -- */
    if ( !boundaryend ) {
		platform_write( "Out of memory in multipart parser.\n");
        goto PARSERERROR;
    }
	memset (boundaryend, 0, BOUNDARY_TAG_SZ+3);

    line = (char*) Memory_alloc(NULL, platform_roundup(MMPARSER_LINE_SZ,PLATFORM_CACHE_LINE_SIZE), PLATFORM_CACHE_LINE_SIZE, &errorBlock);
    if ( !line ) {
		platform_write( "Out of memory in multipart parser.\n");
        goto PARSERERROR;
    }
	memset (line, 0, MMPARSER_LINE_SZ);

    fieldname = (char*) Memory_alloc(NULL, platform_roundup(MAX_POST_VALUE_LENGTH,PLATFORM_CACHE_LINE_SIZE), PLATFORM_CACHE_LINE_SIZE, &errorBlock);
    if ( !fieldname ) {
		platform_write( "Out of memory in multipart parser.\n");
        goto PARSERERROR;
    }	  
	memset (fieldname, 0, MAX_POST_VALUE_LENGTH);


	/*
	 * Note: rawData points to the POSTed data right after Content Length. We should
	 * immediately see a boundary marker.
	 *
	 * Get the boundary tag that the MIME form is using so we can use it to
	 * parse the form.
	 * 
	 * Should look something like: 
	 *    ---------------------------41184676334
	 */
#if DEBUG_POST_DETAILED	 
	 platform_write("Starting fragment of Posted form: \n");
	 for (i = 0; i < 60; i++) {
	 	platform_write("%c ", *(rawData + i));
	 }
	 platform_write("\n"); 
#endif

	/* boundary tags will be terminated with CR/LF */
	i = 0;
	while ((rawData[fptr] != 0x0D) && (rawData[fptr+1] != 0x0A)) {
		*(boundary+i)		= rawData[fptr];
    	*(boundaryend+i) 	= rawData[fptr];
    	i++;
    	fptr++;
	}

    fptr +=2; /* increment past CR/LF */

	/* Terminate boundary tags  to make them strings */
	*(boundary+i)	  ='\0'; 	
	*(boundaryend+i)= '-';		/* ending boundary tag is the tag followed by a -- */
	*(boundaryend+i+1)= '-';
	*(boundaryend+i+2)= '\0';

#if DEBUG_POST
	platform_write("Boundary Marker in use is %s \n", boundary);
	platform_write("End Boundary Marker in use is %s \n", boundaryend);
#endif

	/*
	 * Read in the form fields. Since we are not a robust parser we expect them to appear 
	 * in the POST before the image. This should be the case unless the Form being used 
	 * on the HTML page puts the file field before a data field.
	 * 
	 * Once we have hit the filename field, we will bail from this loop and then read in
	 * the posted file.
	 */
	while (fReadFormFields) {

		myreadline(line, MMPARSER_LINE_SZ, rawData, &fptr);	

#if DEBUG_POST
		platform_write("read line %s \n", line);
#endif

        /* case: file upload*/
        if( getformfield(line, "filename", fieldname, MAX_POST_VALUE_LENGTH) != NULL) {
        	
        	/* 
        	 * Get the filename
        	 *  e.g. Content-Disposition: form-data; name="image"; filename="hpdspua.out"
        	 */
			 strcpy (POST_files[0].name, fieldname);

#if DEBUG_POST
			platform_write("filename=%s\n", fieldname);
#endif
          	/* skip over the Content-type field Content-Type: application/octet-stream */
          	myreadline(line, MMPARSER_LINE_SZ, rawData, &fptr);
          	/* clear out whitespace or newlines before the data stream */
			while(rawData[fptr] == 0x0D || rawData[fptr] == 0x0A){
			    fptr++;
			}
          	goto PROCESSFILE;
        }
        /* case: key value pairs */
        else 
        if( getformfield(line, "name", fieldname, MAX_POST_NAME_LENGTH) != NULL) {
			/* Get the value associated with the named field */
          	myreadline(line, MMPARSER_LINE_SZ, rawData, &fptr);
			if(strchr(line,'&')!=NULL) {
	           	nPostVars += parseKeyValuePairs(line);
			}
	        else {
	        	if(kvcount < MAX_POST_VARS) {
		        	strcpy(POST_names[kvcount],fieldname);
		            strcpy(POST_values[kvcount++],line);
		            nPostVars++;
#if DEBUG_POST
	            	platform_write("Setting %s to %s\n",fieldname, line);
#endif
	         	}
	      	}
        }
#if 0
        else {
        	/* case: boundary marker between fields - do nothing */
          	if (memcmp(line, boundary, BOUNDARY_TAG_SZ) != 0) {
	        	/* We dont recognize this field */
	        	platform_write("Un-recognized field (%s) in the form that was posted.\n", line);
				goto PARSERERROR; 	
        	}
        }  
#endif
	} /* while reading form fields */

	/*
	 * We read the form fields. We should now be pointing at the octet stream
	 * for the file. Read it in until we hit the ending boundary tag.
	 */
PROCESSFILE:

	i 				= strlen(boundaryend);
	imgsize			= 0;
	hdrsize			= fptr;		/* how much of the buffer was used for header processing */
	fReadFormFields	= 1;

	/* Store the image */
	POST_files[0].p_file = (rawData+fptr);
	
	/* Calculate the length of the image */
	while ( (fReadFormFields) && (fptr < (maxfilesize - hdrsize)) ) {
		if (*(rawData+fptr) == '-') {
			if (memcmp((void *)boundaryend, (void *) (rawData+fptr), i) == 0) {
				fReadFormFields = 0;
			}
			else {
				fptr++;
				imgsize++;
			}
		}
		else {
			fptr++;
			imgsize++;
		}
	}
	
    /* Remove trailing CRLF */
    if ((*(rawData + fptr - 2) == 0x0d) && (*(rawData + fptr - 1) == 0x0a)) {
        fptr -= 2;
        imgsize -= 2;
    }

	/* store the size of the image */
	POST_files[0].size = imgsize;
	

PARSERERROR:

	if( boundary ) {
       Memory_free( NULL, boundary, platform_roundup(BOUNDARY_TAG_SZ+1, PLATFORM_CACHE_LINE_SIZE) );
	}
	
	if( boundaryend ) {
      Memory_free( NULL, boundaryend, platform_roundup(BOUNDARY_TAG_SZ+3, PLATFORM_CACHE_LINE_SIZE) );
	}

	if( line ) {
      Memory_free( NULL, line, platform_roundup(MMPARSER_LINE_SZ, PLATFORM_CACHE_LINE_SIZE) );
	}

	if( fieldname ) {
       Memory_free( NULL, fieldname, platform_roundup(MAX_POST_VALUE_LENGTH, PLATFORM_CACHE_LINE_SIZE) );
	}

#if DEBUG_POST
    platform_write("Parser returning file size of %d.\n", imgsize);
#endif
	
	return imgsize;
}

/*************************************************************************
 *  @b getformfield(char line[], char* field, char *fieldname)
 * 
 *  @n
 *  
 *  Helper function for posts. It will extract "file" from the "line"
 *  of input and place the result in "fieldname".  
 * 
 *  @param[in]  
 *		line[]  - Buffer to read the parsed text from.
 * 
 *  @retval
 *   NULL if not found or fieldname.
 *
 ***************************************************************************/
static char* getformfield(char line[], char* field, char *fieldname, int32_t maxlen)
{
  if(strstr(line, field) != NULL)
  {
    strncpy(fieldname,strstr(line,field), (maxlen-2));
    strcpy(fieldname,fieldname +strlen(field)+2);
    strcpy(strchr(fieldname,'\"'),"\0");
    return fieldname;
  }
  else
  {
    return NULL;
  }
}

/*************************************************************************
 *  @b  myreadline(char line[], int32_t len, uint8_t bigarr[],int32_t* fptr)
 * 
 *  @n
 *  
 *  This routine will read a line of text from a POSTed MIME form. A line
 * consists of all text up to a CR/LF. The CR/LF is not retruned and any 
 *  leading white space CR/LF combinations are stripped.  
 * 
 *  @param[in]  
 *		line[]  - Buffer to read the parsed text into.
 * 		len		- The size of line[].
 *		bigarr[]- The data we are parsing.
 *		*fptr	- Index into bigarr. We adjust it based on how much we parsed.
 * 
 *  @retval
 *   HTML_FATAL 		Internal error of some sort.
 *   HTML_TOO_LARGE 	If the Posted file exceeds our MAX_POST_FILESZ
 *   HTML_PARSER_ERROR 	HTML Parser error
 *   length 			The size of the file that was posted.
 *
 ***************************************************************************/
static int32_t myreadline(char line[], int32_t len, uint8_t bigarr[],int32_t* fptr)
{
  int32_t i;
  int32_t j;
  int32_t numparsed;

  i 		= 0;
  numparsed = 0;
  j 		= *fptr;
 
	 
  while(i < len) {

  	/* Look for a carriage return line feed that terminates the line. */
    if( (bigarr[j] == 0x0D) && (bigarr[j+1] == 0x0A)) {
    	j+=2;
    	numparsed+=2;
    	if (i != 0) {
    		/* Found line termination - bail */
      		break;
    	}
    }
	else {
#if DEBUG_POST_DETAILED
		platform_write( "saving character %c (%x) \n", bigarr[j], bigarr[j]);
#endif
		line[i] = bigarr[j] ;
		i++;
		j++;
		numparsed++;
	}
  }

  /* Increment the "file" pointer by the number of bytes we read */
  *fptr += numparsed;
 
 /* Make line a string that we return */
  line[i] = '\0';
 
  return i;
}

/*************************************************************************
 *  @b  parseKeyValuePairs(char* tokens)
 * 
 *  @n
 *  
 *  Parses a string and stores the POSTed field "name" and its associated
 *  value in our global POST_names/values arrays. 
 * 
 *  @param[in]  
 *  tokens	- Sring to parse.
 * 
 *  @retval
 *   
 *
 ***************************************************************************/

static int32_t parseKeyValuePairs(char* tokens)
{
  int32_t i=0,j=0;
  int32_t record=0;
  int32_t len = strlen(tokens);
KEY:
  if(tokens[i]=='&')
    goto ERROR;
  if(tokens[i]=='=')
  {
    POST_names[record][j]='\0';
    i++;
    j=0;
    goto VALUE;
  }
  POST_names[record][j]=tokens[i];
  i++;
  j++;
  goto KEY;
VALUE:
  if(tokens[i]=='&')
  {
    POST_values[record][j]='\0';
    i++;
    j=0;
    record++;
    goto KEY;
  }
  if(tokens[i]=='=')
  {
    goto ERROR;
  }
  POST_values[record][j]=tokens[i];
  i++;
  j++;
  if(i==len)
    goto END;
  goto VALUE;

END:
	POST_values[record][j]='\0';

#if DEBUG_POST
  for(i=0;i<=record;i++) {
   platform_write("POST value %s=%s\n",POST_names[i],POST_values[i]);
  }
#endif
  return record;

ERROR:

#if DEBUG_POST
  platform_write("unexpected token %c at token[%d]\n",tokens[i],i);
#endif

  return record;
}

