/*
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef _C_WEBSERVER_H
#define _C_WEBSERVER_H  /* #defined if this .h file has been included */
// C++ / C Function Declarations
#ifdef __cplusplus
#define _extern extern "C"
#define _externfar extern "C" far
#else
#define _extern extern
#define _externfar extern far
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "netmain.h"
#include <_stack.h>
#include "cgiparse.h"
#include "cgiparsem.h"
#include "platform.h"



/*******************************************************************************
 * The following routines are used to dynamically build web pages
 *******************************************************************************/

/*
 * The following routines are used for building a web page dynamically. That
 * is through a CGI function. Basically you do the following:
 * 
 *  html_start()
 *  any number of html or html_var calls
 *  html_end()
 * 
 * When you are ready to send the page you can use html_getsize to ste the 
 * ContentLength and html_getpage to retrurn a pointer to the start of the page to send.
 */
 
extern void			html_start(char *str);	// Call this one first
extern void			html(char *str);		// Adds more html
extern void         html_var(const char *fmt, ...); // Adds printf style HTML
extern void			html_end(char *str);	// This is for adding the final line of html
extern uint32_t 	html_getsize();			// Get the size of the page that was built
extern char			*html_getpage();		// get a pointer to the beginning of the page

/*******************************************************************************
 * The following routines are used to process a form that was POSTed.
 *******************************************************************************/

/* 
 * html_processFileUpload()
 * 
 * Processes the POST of a Multi-part MIME form saving the values in POST_names
 *  and POST_values and the uploaded files in POST_images. You can then use 
 *  html_getValueFor to retrieve the posted values.
 * 
 * Returns 
 * 
 * 1 on success or one of the defines below on error.
 */
extern int32_t 	html_processFileUpload(SOCKET htmlSock, int32_t ContentLength, uint8_t *pBuff, uint32_t buffsize );
#define	HTML_FATAL 			-1 	// Internal error of some sort.
#define HTML_TOO_LARGE		-2	// If the Post exceeds our MAX_POST_FILESZ
#define HTML_PARSER_ERROR 	-3 	// HTML Parser error. On mime pages it could mean no file was specified.

/* 
 * html_processPost()
 * 
 * Processes the POST of a NON MIME form saving the values in POST_names
 *  and POST_values. You can then use html_getValueFor to retrieve them.
 * 
 * Returns 
 */
extern int32_t	html_processPost(SOCKET htmlSock, int32_t ContentLength);

extern char 	*html_getValueFor(char *name);	// Return value for he name passed or NULL
extern uint32_t  html_getNumberPostVars();


/* Holds the name and value of each field posted in a form */
#define MAX_POST_VARS			20		// Can post up to this number of fields in a form
#define MAX_POST_NAME_LENGTH	40		// No name can be longer than this
#define MAX_POST_VALUE_LENGTH	1024	// No value can be longer than this

extern char	POST_names[MAX_POST_VARS][MAX_POST_NAME_LENGTH];
extern char	POST_values[MAX_POST_VARS][MAX_POST_VALUE_LENGTH];

/* Holds information about a file that has been uploaded */
#define MAX_POST_FILES			1		// Max number of files that can be uploaded
typedef struct post_file {
	uint8_t		*p_file;
	uint32_t	size;
	char		name[MAX_POST_VALUE_LENGTH];
} post_file_s;
extern post_file_s	POST_files[MAX_POST_FILES];

#endif
