/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-v38
 */

#include <xdc/std.h>

__FAR__ char custom_hpdspua_evm6472__dummy__;

#define __xdc_PKGVERS null
#define __xdc_PKGNAME custom.hpdspua.evm6472
#define __xdc_PKGPREFIX custom_hpdspua_evm6472_

#ifdef __xdc_bld_pkg_c__
#define __stringify(a) #a
#define __local_include(a) __stringify(a)
#include __local_include(__xdc_bld_pkg_c__)
#endif

