/*!
 * File generated by platform wizard. DO NOT MODIFY
 *
 */

metaonly module Platform inherits xdc.platform.IPlatform {

    config ti.platforms.generic.Platform.Instance CPU =
        ti.platforms.generic.Platform.create("CPU", {
            clockRate:      983,                                       
            catalogName:    "ti.catalog.c6000",
            deviceName:     "TMS320C6670",
            customMemoryMap:
           [          
                ["L2SRAM", 
                     {
                        base: 0x00820000,                    
                        space: "code/data",
                        name: "L2SRAM",
                        len: 0x00060000,                    
                        access: "RWX",
                     }
                ],
                ["MSMCSRAM", 
                     {
                        base: 0x0c000000,                    
                        space: "code/data",
                        name: "MSMCSRAM",
                        len: 0x00400000,                    
                        access: "RWX",
                     }
                ],
                ["L1PSRAM", 
                     {
                        base: 0x00E00000,                    
                        space: "code",
                        name: "L1PSRAM",
                        len: 0x00008000,                    
                        access: "RWX",
                     }
                ],
                ["L1DSRAM", 
                     {
                        base: 0x00F00000,                    
                        space: "data",
                        name: "L1DSRAM",
                        len: 0x00008000,                    
                        access: "RW",
                     }
                ],
                ["DDR", 
                     {
                        base: 0x80000000,                    
                        space: "code/data",
                        name: "DDR",
                        len: 0x1FFFFFFF,                    
                        access: "RWX",
                     }
                ],
                ["L2SRAM_IBL_RESERVED", 
                     {
                        base: 0x00800000,                    
                        space: "code/data",
                        name: "L2SRAM_IBL_RESERVED",
                        len: 0x00020000,                    
                        access: "RWX",
                     }
                ],
           ],
          l1DMode:"32k",
          l1PMode:"32k",
          l2Mode:"32k",

    });
    
instance :
    
    override config string codeMemory  = "DDR";   
    override config string dataMemory  = "DDR";                                
    override config string stackMemory = "MSMCSRAM";
    
}
