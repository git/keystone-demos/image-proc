@echo OFF

REM *******************************************
REM  Create release directory structure for HPDSPUA
REM *******************************************

REM I should get the directory to the root of demos

if "%1" == "" goto error


echo Directory root passed to me is  %1

REM *******************************************
REM Create directories I need (they may already exist)
REM *******************************************

REM mkdir %1\evmc6472\custom\hpdspua\evm6472
REM mkdir %1\evmc6472\hpdspua

REM mkdir %1\evmc6474l\custom\hpdspua\evm6474l
REM mkdir %1\evmc6474l\hpdspua

REM mkdir %1\evmc6457l\custom\hpdspua\evm6457l
REM mkdir %1\evmc6457l\hpdspua

mkdir %1\hua\custom
mkdir %1\hua\evmc6678l
mkdir %1\hua\evmc6657l
mkdir %1\hua\evmc6670l
mkdir %1\hua\src
mkdir %1\hua\docs

REM *******************************************
REM Clean any files that are there
REM *******************************************

REM del /F /S /Q %1\evmc6472\*

REM del /F /S /Q %1\evmc6474l\*

REM del /F /S /Q %1\evmc6457l\*

del /F /S /Q %1\hua\* 

REM *******************************************
REM Copy in the new release files. This is done in a 
REM specific order
REM *******************************************

REM xcopy common\hpdspua  %1\evmc6472\hpdspua /Y /E
REM xcopy projects\ccs4x\c6472\hpdspua  %1\evmc6472\hpdspua /Y /E
REM xcopy platforms\evmc6472  %1\evmc6472 /Y /E

REM xcopy common\hpdspua  %1\evmc6474l\hpdspua /Y /E
REM xcopy projects\ccs4x\c6474l\hpdspua  %1\evmc6474l\hpdspua /Y /E
REM xcopy platforms\evmc6474l  %1\evmc6474l /Y /E

REM xcopy common\hpdspua  %1\evmc6457l\hpdspua /Y /E
REM xcopy projects\ccs4x\c6457l\hpdspua  %1\evmc6457l\hpdspua /Y /E
REM xcopy platforms\evmc6457l  %1\evmc6457l /Y /E

xcopy common\hpdspua\src  %1\hua\src /Y /E
xcopy common\hpdspua\docs  %1\hua\docs /Y /E

copy common\hpdspua\readme.txt  %1\hua\evmc6678l /Y /E
xcopy projects\ccs5x\c6678l\hpdspua  %1\hua\evmc6678l /Y /E
xcopy platforms\evmc6678l %1\hua /Y /E

copy common\hpdspua\readme.txt  %1\hua\evmc6670l /Y /E
xcopy projects\ccs5x\c6670l\hpdspua %1\hua\evmc6670l /Y /E
xcopy platforms\evmc6670l %1\hua /Y /E

copy common\hpdspua\readme.txt  %1\hua\evmc6657l /Y /E
xcopy projects\ccs5x\c6657l\hpdspua %1\hua\evmc6657l /Y /E
xcopy platforms\evmc6657l %1\hua /Y /E

REM *******************************************
REM Clean any git or windows files
REM *******************************************

del /F /S /Q %1\Thumbs.db
del /F /S /Q %1\hpdspua_user_guide.pdf
del /F /S /Q %1\i2cwrite.c

goto end

REM *******************************************
REM Done and error blocks
REM *******************************************

:error
echo missing argument!
echo usage  relhpdspua.bat <release directory path>
goto end

:end
echo Release directory created.
