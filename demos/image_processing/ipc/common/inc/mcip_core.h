/*
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef IMAGE_PROCESSING_H
#define IMAGE_PROCESSING_H

#include <stdint.h>
#include <xdc/std.h>

#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>

#define IMAGE_PROCESSING_HEAP_NAME   "image_processing_heap"
#define IMAGE_PROCESSING_HEAPID      0

#define NUMBER_OF_SCRATCH_BUF 3

#if defined(SLAVE)
#define platform_write printf
#else
#define platform_write printf
#endif

#define GET_SLAVE_QUEUE_NAME(str, core_id) sprintf(str,"core%d_queue", core_id)

/*
 * Bitmap RGB colormap entry structure.
 */
typedef struct {
  uint8_t red;
  uint8_t green;
  uint8_t blue;
  uint8_t reserved;
} color_table_t;

typedef enum processing_type {
    edge_detection
} processing_type_e;

typedef struct processing_info {
    processing_type_e processing_type;
    uint8_t *         rgb_in;
    color_table_t   * p_color_table;
    uint16_t          bitspp;
    uint32_t          width; 
    uint32_t          height;
    uint8_t *         out;
    uint8_t *         scratch_buf[NUMBER_OF_SCRATCH_BUF];
    uint32_t          scratch_buf_len[NUMBER_OF_SCRATCH_BUF];
    int               flag;
} processing_info_t;

/* This structure holds the IPC message. The pointers should point to global/shared
 * memory space (like SL2RAM or DDR2).
 * The core_id will always slave core id.
 * In the response message the info.flag will will indicate if there is any failure.
 */
typedef struct process_message {
    MessageQ_MsgHeader  header;
    int                 core_id;
    processing_info_t   info;
} process_message_t;

void process_rgb (processing_info_t * p_info);

typedef uint32_t IPN;
typedef uint8_t UINT8;
#endif /* IMAGE_PROCESSING_H */
