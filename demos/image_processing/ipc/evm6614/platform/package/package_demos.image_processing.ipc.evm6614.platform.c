/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-y30
 */

#include <xdc/std.h>

__FAR__ char demos_image_processing_ipc_evm6614_platform__dummy__;

#define __xdc_PKGVERS null
#define __xdc_PKGNAME demos.image_processing.ipc.evm6614.platform
#define __xdc_PKGPREFIX demos_image_processing_ipc_evm6614_platform_

#ifdef __xdc_bld_pkg_c__
#define __stringify(a) #a
#define __local_include(a) __stringify(a)
#include __local_include(__xdc_bld_pkg_c__)
#endif

