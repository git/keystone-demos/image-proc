/*
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

#include <ti/ndk/inc/stkmain.h>

#include <ti/drv/emac/emac_drv.h>
#include <ti/drv/emac/src/v4/emac_drv_v4.h>

#include <ti/csl/csl.h>
#include <ti/csl/soc.h>
#include <ti/csl/cslr_device.h>
//#include <ti/csl/csl_psc.h>
//#include <ti/csl/csl_pscAux.h>

#include <ti/board/board.h>

/* TI-RTOS Header files */
#include <ti/drv/i2c/I2C.h>
#include <ti/drv/i2c/soc/I2C_v1.h>
/* UART Header files */
#include <ti/drv/uart/UART.h>
#include <ti/drv/uart/UART_stdio.h>
#include <ti/csl/soc/am572x/src/csl_device_xbar.h>

#include <xdc/runtime/IHeap.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/HeapBufMP.h>
#include <ti/ipc/MultiProc.h>

#include "demos/image_processing/ipc/common/inc/mcip_core.h"
#include "demos/image_processing/ipc/master/inc/mcip_process.h"
#include "demos/image_processing/ipc/master/inc/mcip_webpage.h"

/* Enable the below macro to have prints on the IO Console */
//#define IO_CONSOLE

#ifndef IO_CONSOLE
#define NIMU_log                UART_printf
#else
#define NIMU_log                printf
#endif

/* ========================================================================== */
/*                             Macros                                  */
/* ========================================================================== */

/**Phy address of  port 0 */
#define EMAC_CPSW_PORT0_PHY_ADDR_EVM     1
/**Phy address of port 1 */
#define EMAC_CPSW_PORT1_PHY_ADDR_EVM     2


#define MAX_TABLE_ENTRIES   3

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/**Task handle for EIP*/
Task_Handle main_task;

static int nimu_device_index = 0U;

void image_processing_webfiles_add(void);

NIMU_DEVICE_TABLE_ENTRY NIMUDeviceTable[MAX_TABLE_ENTRIES];

UInt16  remoteProcId;
#define MAX_NUM_CORE 2

//---------------------------------------------------------------------------
// Title String
//
char *VerStr = "\nTCP/IP Stack 'Image Processing Demo' Application\n\n";

//---------------------------------------------------------------------------
// Configuration
//
int32_t ipc_heap_init()
{
    HeapBufMP_Handle heapHandle;
    HeapBufMP_Params heapBufParams;
    Int              status;
    uint32_t retStat = CSL_SOK;

	/* Create the heap that will be used to allocate messages. */
    HeapBufMP_Params_init(&heapBufParams);
    heapBufParams.regionId       = 0;
    heapBufParams.name           = IMAGE_PROCESSING_HEAP_NAME;
    heapBufParams.numBlocks      = MAX_NUM_CORE;
    heapBufParams.blockSize      = sizeof(process_message_t);
    heapHandle = HeapBufMP_create(&heapBufParams);
    if (heapHandle == NULL) {
    	printf("HeapBufMP_create failed\n" );
		retStat = CSL_ESYS_FAIL;
    } else
    {
		/* Register this heap with MessageQ */
		status = MessageQ_registerHeap((IHeap_Handle)heapHandle, IMAGE_PROCESSING_HEAPID);
		if(status != MessageQ_S_SUCCESS) {
			printf("MessageQ_registerHeap failed\n" );
			retStat = CSL_ESYS_FAIL;
		} else
		{
			//printf("MessageQ_registerHeap OK (0x%x)\n", heapHandle);
			if (mc_process_init(MAX_NUM_CORE))
			{
				printf("mc_process_init failed\n");
				retStat = CSL_ESYS_FAIL;
			} else
			{
				printf("mc_process_init OK\n");
			}
		}
    }

    return retStat;
}

void TaskFxn(UArg a0, UArg a1);
extern int CpswEmacInit (STKEVENT_Handle hEvent);


/**
 *  \name main
 *  \brief Main Function
 *  \param none
 *  \return none
 *
 */
int main()
{
    /* Call board init functions */
    Board_initCfg boardCfg;
    Task_Params taskParams;
    EMAC_HwAttrs_V4 cfg;

    boardCfg =  BOARD_INIT_PINMUX_CONFIG |
        BOARD_INIT_MODULE_CLOCK | BOARD_INIT_UART_STDIO;
    Board_init(boardCfg);

    CSL_xbarMpuIrqConfigure(CSL_XBAR_INST_MPU_IRQ_92, CSL_XBAR_GMAC_SW_IRQ_RX_PULSE);
    CSL_xbarMpuIrqConfigure(CSL_XBAR_INST_MPU_IRQ_93, CSL_XBAR_GMAC_SW_IRQ_TX_PULSE);

    /* Select RGMII 2 ports GMIIx_SEL = 2 for RGMII*/
    CSL_FINS (((CSL_control_coreRegs *) CSL_MPU_CTRL_MODULE_CORE_CORE_REGISTERS_REGS)->CONTROL_IO_1,
          CONTROL_CORE_CONTROL_IO_1_GMII1_SEL, 2U);
    CSL_FINS (((CSL_control_coreRegs *) CSL_MPU_CTRL_MODULE_CORE_CORE_REGISTERS_REGS)->CONTROL_IO_1,
          CONTROL_CORE_CONTROL_IO_1_GMII2_SEL, 2U);

    /*GMAC RESET ISOLATION Enable*/
    CSL_FINS (((CSL_control_coreRegs *) CSL_MPU_CTRL_MODULE_CORE_CORE_REGISTERS_REGS)->CONTROL_IO_2,
          CONTROL_CORE_CONTROL_IO_2_GMAC_RESET_ISOLATION_ENABLE, 0U);
    CSL_FINS (((CSL_control_coreRegs *) CSL_MPU_CTRL_MODULE_CORE_CORE_REGISTERS_REGS)->CONTROL_IO_2,
          CONTROL_CORE_CONTROL_IO_2_GMAC_RESET_ISOLATION_ENABLE, 1U);


    EMAC_socGetInitCfg(0, &cfg);
    cfg.port[0].phy_addr = EMAC_CPSW_PORT0_PHY_ADDR_EVM;
    cfg.port[1].phy_addr = EMAC_CPSW_PORT1_PHY_ADDR_EVM;
    EMAC_socSetInitCfg(0, &cfg);

    Task_Params_init(&taskParams);
    taskParams.priority = 1;
    taskParams.stackSize = 0x1400;
    main_task = Task_create (TaskFxn, &taskParams, NULL);

    NIMUDeviceTable[nimu_device_index++].init =  &CpswEmacInit ;
    NIMUDeviceTable[nimu_device_index].init =  NULL ;

    BIOS_start();

    return -1;
}

/**
 *  \name TaskFxn
 *  \brief Task which do EIP initialization
 *  \param a0
 *  \param a1
 *  \return none
 *
 */
void TaskFxn(UArg a0, UArg a1)
{
    int              i;
    char	         coreName[8];
    Int              status;

    printf("Enter Ipc_start\n");
    NIMU_log("Enter Ipc_start\n");
    /*
     *  initialize the ipc layer
     */
    status = Ipc_start();
    if (status < 0)
    {
    	printf("Ipc_start failed\n");
    	NIMU_log("Ipc_start failed\n");
    } else
    {
    	printf("Ipc_start OK\n");
    	NIMU_log("Ipc_start OK\n");
    }

    /* attach the remote processors */
    for (i=0; i<MAX_NUM_CORE; i++)
    {
    	sprintf(coreName, "DSP%d", i+1);
		remoteProcId = MultiProc_getId(coreName);
		do {
			status = Ipc_attach(remoteProcId);
		} while ((status < 0) && (status == Ipc_E_NOTREADY));
    }
	printf("Ipc_attach() OK\n");
	NIMU_log("Ipc_attach() OK\n");

    /* IPC heap init */
    status = ipc_heap_init();
    if (status!=CSL_SOK)
    {
    	printf("ipc_heap_init failed\n");
        NIMU_log("ipc_heap_init failed\n");
    } else
    {
    	printf("ipc_heap_init OK\n");
    	NIMU_log("ipc_heap_init OK\n");
    }

    NIMU_log("\n\rImage Processing Demo is Ready, EVM IP Addr\n\r");
    image_processing_webfiles_add();
}

void myNetworkIPAddrHook(IPN IPAddr, uint IfIdx, uint fAdd)
{
    IPN IPTmp;

    IPTmp = NDK_ntohl(IPAddr);
    printf("Network Added:\tIf-%d:%d.%d.%d.%d\n", IfIdx,
            (UINT8)(IPTmp>>24)&0xFF, (UINT8)(IPTmp>>16)&0xFF,
            (UINT8)(IPTmp>>8)&0xFF, (UINT8)IPTmp&0xFF);
    NIMU_log("Network Added:\tIf-%d:%d.%d.%d.%d\n", IfIdx,
            (UINT8)(IPTmp>>24)&0xFF, (UINT8)(IPTmp>>16)&0xFF,
            (UINT8)(IPTmp>>8)&0xFF, (UINT8)IPTmp&0xFF);
}
