<?php
/*********************************************************************************************************
*	Copyright(C) 2010, Texas Instruments Incorporated
*
*	Module
*		hexwrite
*
*	Description
*		Converts an HTML or orhter HTML object (image file, etc) into a hex array.
&
*	Change Log
*
*	June 23rd, 2010	Jack Manbeck	Created
*
************************************************************************************************************/


/************************************************************************************************************
*	Include other PHP support scripts
************************************************************************************************************/
				
/************************************************************************************************************
*		GLOBALS
*
* Note: To use a global in a function you must declare it is  "global" within the function scope.
************************************************************************************************************/


/************************************************************************************************************
*		Class Definitions
************************************************************************************************************/

/************************************************************************************************************
*		Function Definitions
************************************************************************************************************/

/* 
** Returns True if the string has a value. 
*/
function hasvalue($val) {
	if (!$val) return 0;
	if ($val == "") return 0;
	return 1;	
}


/*
** prints program usage.
*/
function usage() {

	echo "usage: hexwrite.php [options] filename [output directory]" ."\n";

	return;	
}

/*
** Prints an error and then exits.
*/
function error_and_die($errmsg) {
	global $option_silent;
	
	if (!$option_silent) echo $errmsg . "\n";
	exit (1);	
}

/*
** gets comma seperated values
*/
if(!function_exists('str_getcsv')) {
function str_getcsv($input, $delimiter = ",", $enclosure = '"', $escape = "\\") {
        $size = 1024;
        $fp = fopen("php://temp/maxmemory:$size", 'r+');
        fputs($fp, $input);
        rewind($fp);
        $data = fgetcsv($fp, 1000, $delimiter, $enclosure); //  $escape only got added in 5.3.0
        fclose($fp);
        return $data;
}
}
 
/************************************************************************************************************
* This area starts the "main" of the PHP script
*************************************************************************************************************/

date_default_timezone_set("America/New_York");

/* 
** $argc is the count of arguments, $argv are the arguments.
**
** e.g. php-cli hexwrite.php has $argc = 1 and argv[0] = "hexwrite.php"
** e.g. php-cli hexwrite.php some.html has $argc = 2 and argv[0] = "hexwrite.php" and argv[1] = "some.html"
*/

/*
** We should have exactly two arguments
**
*/
if ($argc < 2) {
	/* Need at least two arguments */
	usage();
	exit(1);
}


/* Get the name of the file to convert from the command line */
$filename = $argv[1];

/* [Optional] output directory */
if ($argc > 2) {
    $outdir = $argv[2];
    if (!is_dir($outdir)) {
        if (!mkdir ($outdir, 0777, true)) {
            error_and_die("Can not create output directory ( " . $outdir . " ). ");
        }
    }
} else {
    $outdir = "";
}

/* Make sure the file exists */
if (!file_exists($filename)) {
	error_and_die("The file you specified ( " . $filename . " ) could not be found. ");
}

/* open the file to convert */
$original_size 	= filesize($filename);
$fpr 			= fopen($filename, "rb");

/* Creat the header file we will write to */
$parts = pathinfo($filename);
if ($outdir == "") {
    $outdir = $parts['dirname'];
}
$filename = $outdir . "\\" . $parts['filename'] . $parts['extension'] . ".h";
$fpw = fopen($filename, "w");


$sizestr = strtoupper($parts['filename'] . $parts['extension']) . "_SIZE";

$text = sprintf ("#define %s %d \n", $sizestr,$original_size );
fwrite($fpw, $text);

$text = sprintf ("unsigned char %s[] = { \n ", strtoupper($parts['filename'] . $parts['extension']));
fwrite($fpw, $text);

$bytecount = 1;

while (!feof($fpr)) {

	$byte = fread($fpr, 1);
	
	$hex = dechex(ord($byte));
	
	if ($bytecount <= $original_size)
		$hexstr = "0x". $hex .", ";
	else
		$hexstr = "0x". $hex;
	
	fwrite($fpw, $hexstr);

	/* make rows of 12 */
	if (($bytecount % 12) == 0) {
		fwrite($fpw,"\n");
	}	

	$bytecount++;
}

/* cap the array */
fwrite($fpw,"}; \n");

/* were done */
fclose($fpw);
fclose($fpr);


exit (0);

?>
