/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-B06
 */

#include <xdc/std.h>

__FAR__ char evmAM572x_mcip_dsp2__dummy__;

#define __xdc_PKGVERS null
#define __xdc_PKGNAME evmAM572x_mcip_dsp2
#define __xdc_PKGPREFIX evmAM572x_mcip_dsp2_

#ifdef __xdc_bld_pkg_c__
#define __stringify(a) #a
#define __local_include(a) __stringify(a)
#include __local_include(__xdc_bld_pkg_c__)
#endif

