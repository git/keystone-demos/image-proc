#
# Copyright (c) 2016, Texas Instruments Incorporated
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# *  Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# *  Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# *  Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
GEN_OPTS__FLAG := --cmd_file="configPkg/compiler.opt"
GEN_CMDS__FLAG := -l"configPkg/linker.cmd"

ORDERED_OBJS += \
"./mcip_core.obj" \
"./mcip_core_task.obj" \
"./mcip_slave_main.obj" \
$(GEN_CMDS__FLAG) \
-limglib.ae66 \
-llibc.a \

-include ../makefile.init

OS := $(shell uname)
ifeq ($(OS), Linux)
RM := rm -f
RMDIR := rm -d -f -r
else
RM := rm -f
RMDIR := rm -r -f
endif

# All of the sources participating in the build are defined here
# -include sources.mk
# -include subdir_vars.mk
# Add inputs and outputs from these tool invocations to the build variables 
CFG_SRCS += \
../image_processing_evmc6678l_slave.cfg 

GEN_MISC_DIRS += \
./configPkg/ 

GEN_CMDS += \
./configPkg/linker.cmd 

GEN_OPTS += \
./configPkg/compiler.opt 

GEN_FILES += \
./configPkg/linker.cmd \
./configPkg/compiler.opt 

OS := $(shell uname)
ifeq ($(OS), Linux)
GEN_FILES__QUOTED += \
"configPkg/linker.cmd" \
"configPkg/compiler.opt" 
else
GEN_FILES__QUOTED += \
"configPkg\linker.cmd" \
"configPkg\compiler.opt" 
endif

GEN_MISC_DIRS__QUOTED += \
"configPkg" 


# -include common/src/subdir_vars.mk
# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../../../../common/src/mcip_core.c \
../../../../common/src/mcip_core_task.c 

OBJS += \
./mcip_core.obj \
./mcip_core_task.obj 

C_DEPS += \
./mcip_core.d \
./mcip_core_task.d 

C_DEPS__QUOTED += \
"mcip_core.d" \
"mcip_core_task.d" 

OBJS__QUOTED += \
"mcip_core.obj" \
"mcip_core_task.obj" 

C_SRCS__QUOTED += \
"../../../../common/src/mcip_core.c" \
"../../../../common/src/mcip_core_task.c" 


# -include master/src/subdir_vars.mk
# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../../../../slave/src/mcip_slave_main.c 

OBJS += \
./mcip_slave_main.obj 

C_DEPS += \
./mcip_slave_main.d 

C_DEPS__QUOTED += \
"mcip_slave_main.d" 

OBJS__QUOTED += \
"mcip_slave_main.obj" 

C_SRCS__QUOTED += \
"../../../../slave/src/mcip_slave_main.c" 


# -include subdir_rules.mk
# Each subdirectory must supply rules for building sources it contributes
configPkg/linker.cmd: ../image_processing_evmc6678l_slave.cfg
	@echo 'Building file: $<'
	@echo 'Invoking: XDCtools'
	"${XDC_INSTALL_PATH}/xs" --xdcpath="${IPC_INSTALL_PATH}/packages;${BIOS_INSTALL_PATH}/packages;${IMGLIB_INSTALL_PATH}/packages;${UIA_INSTALL_PATH}/packages;../../../../../../../;${PDK_INSTALL_PATH};" xdc.tools.configuro -o configPkg -t ti.targets.elf.C66 -p demos.image_processing.ipc.evmc6678l.platform -r debug -c "${C6X_GEN_INSTALL_PATH}" "$<"
	@echo 'Finished building: $<'
	@echo ' '

configPkg/compiler.opt: | configPkg/linker.cmd
configPkg/: | configPkg/linker.cmd


# -include common/src/subdir_rules.mk
# Each subdirectory must supply rules for building sources it contributes
mcip_core.obj: ../../../../common/src/mcip_core.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${IMGLIB_INSTALL_PATH}/inc" --include_path="../../../../../../../" --include_path="../" -g --define=SLAVE --define=C66_PLATFORMS --define=DEVICE_C6678 --define=SOC_C6678 --diag_warning=225 --display_error_number --mem_model:const=far --mem_model:data=far --preproc_with_compile --preproc_dependency="mcip_core.d" --obj_directory="." $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

mcip_core_task.obj: ../../../../common/src/mcip_core_task.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${IMGLIB_INSTALL_PATH}/inc" --include_path="../../../../../../../" --include_path="../" -g --define=SLAVE --define=C66_PLATFORMS --define=DEVICE_C6678 --define=SOC_C6678 --diag_warning=225 --display_error_number --mem_model:const=far --mem_model:data=far --preproc_with_compile --preproc_dependency="mcip_core_task.d" --obj_directory="." $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


# -include master/src/subdir_rules.mk
# Each subdirectory must supply rules for building sources it contributes
mcip_slave_main.obj: ../../../../slave/src/mcip_slave_main.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${IMGLIB_INSTALL_PATH}/inc" --include_path="../../../../../../../" --include_path="../" -g --define=SLAVE --define=C66_PLATFORMS --define=DEVICE_C6678 --define=SOC_C6678 --diag_warning=225 --display_error_number --mem_model:const=far --mem_model:data=far --preproc_with_compile --preproc_dependency="mcip_slave_main.d" --obj_directory="." $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


# -include objects.mk
USER_OBJS :=

LIBS := -limglib.ae66 -llibc.a


ifneq ($(MAKECMDGOALS),clean)
ifneq ($(strip $(S_DEPS)),)
-include $(S_DEPS)
endif
ifneq ($(strip $(S_UPPER_DEPS)),)
-include $(S_UPPER_DEPS)
endif
ifneq ($(strip $(S62_DEPS)),)
-include $(S62_DEPS)
endif
ifneq ($(strip $(C64_DEPS)),)
-include $(C64_DEPS)
endif
ifneq ($(strip $(ASM_DEPS)),)
-include $(ASM_DEPS)
endif
ifneq ($(strip $(CC_DEPS)),)
-include $(CC_DEPS)
endif
ifneq ($(strip $(SV7A_DEPS)),)
-include $(SV7A_DEPS)
endif
ifneq ($(strip $(S55_DEPS)),)
-include $(S55_DEPS)
endif
ifneq ($(strip $(C67_DEPS)),)
-include $(C67_DEPS)
endif
ifneq ($(strip $(CLA_DEPS)),)
-include $(CLA_DEPS)
endif
ifneq ($(strip $(C??_DEPS)),)
-include $(C??_DEPS)
endif
ifneq ($(strip $(CPP_DEPS)),)
-include $(CPP_DEPS)
endif
ifneq ($(strip $(S??_DEPS)),)
-include $(S??_DEPS)
endif
ifneq ($(strip $(C_DEPS)),)
-include $(C_DEPS)
endif
ifneq ($(strip $(C62_DEPS)),)
-include $(C62_DEPS)
endif
ifneq ($(strip $(CXX_DEPS)),)
-include $(CXX_DEPS)
endif
ifneq ($(strip $(C++_DEPS)),)
-include $(C++_DEPS)
endif
ifneq ($(strip $(ASM_UPPER_DEPS)),)
-include $(ASM_UPPER_DEPS)
endif
ifneq ($(strip $(K_DEPS)),)
-include $(K_DEPS)
endif
ifneq ($(strip $(C43_DEPS)),)
-include $(C43_DEPS)
endif
ifneq ($(strip $(INO_DEPS)),)
-include $(INO_DEPS)
endif
ifneq ($(strip $(S67_DEPS)),)
-include $(S67_DEPS)
endif
ifneq ($(strip $(SA_DEPS)),)
-include $(SA_DEPS)
endif
ifneq ($(strip $(S43_DEPS)),)
-include $(S43_DEPS)
endif
ifneq ($(strip $(OPT_DEPS)),)
-include $(OPT_DEPS)
endif
ifneq ($(strip $(PDE_DEPS)),)
-include $(PDE_DEPS)
endif
ifneq ($(strip $(S64_DEPS)),)
-include $(S64_DEPS)
endif
ifneq ($(strip $(C_UPPER_DEPS)),)
-include $(C_UPPER_DEPS)
endif
ifneq ($(strip $(C55_DEPS)),)
-include $(C55_DEPS)
endif
endif

-include ../makefile.defs

# Add inputs and outputs from these tool invocations to the build variables 
EXE_OUTPUTS += \
image_processing_evmc6678l_slave.out \

EXE_OUTPUTS__QUOTED += \
"image_processing_evmc6678l_slave.out" \

BIN_OUTPUTS += \
image_processing_evmc6678l_slave.hex \

BIN_OUTPUTS__QUOTED += \
"image_processing_evmc6678l_slave.hex" \


# All Target
all: image_processing_evmc6678l_slave.out

# Tool invocations
image_processing_evmc6678l_slave.out: $(OBJS) $(GEN_CMDS)
	@echo 'Building target: $@'
	@echo 'Invoking: C6000 Linker'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi -g --define=SLAVE --define=C66_PLATFORMS --define=DEVICE_C6678 --define=SOC_C6678 --diag_warning=225 --display_error_number --mem_model:const=far --mem_model:data=far -z -m"image_processing_evmc6678l_slave.map" --heap_size=0x100000 -i"${C6X_GEN_INSTALL_PATH}/lib" -i"${C6X_GEN_INSTALL_PATH}/include" -i"${IMGLIB_INSTALL_PATH}/lib" --reread_libs --warn_sections --relocatable --xml_link_info="image_processing_evmc6678l_slave_linkInfo.xml" --rom_model --dynamic=exe -o "image_processing_evmc6678l_slave.out" $(ORDERED_OBJS)
	@echo 'Finished building target: $@'
	@echo ' '

image_processing_evmc6678l_slave.hex: $(EXE_OUTPUTS)
	@echo 'Invoking: C6000 Hex Utility'
	"${C6X_GEN_INSTALL_PATH}/bin/hex6x"  -o "image_processing_evmc6678l_slave.hex" $(EXE_OUTPUTS__QUOTED)
	@echo 'Finished building: $@'
	@echo ' '

# Other Targets
clean:
	-$(RM) $(GEN_MISC_FILES__QUOTED)$(EXE_OUTPUTS__QUOTED)$(GEN_FILES__QUOTED)$(BIN_OUTPUTS__QUOTED)$(GEN_OPTS__QUOTED)$(GEN_CMDS__QUOTED)
	-$(RMDIR) $(GEN_MISC_DIRS__QUOTED)
	-$(RM) "mcip_core.d" "mcip_core_task.d" "mcip_slave_main.d" 
	-$(RM) "mcip_core.obj" "mcip_core_task.obj" "mcip_slave_main.obj" 
	-@echo 'Finished clean'
	-@echo ' '

.PHONY: all clean dependents
.SECONDARY:

-include ../makefile.targets

