#!/bin/sh
# Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
#
#  Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions 
#  are met:
#
#    Redistributions of source code must retain the above copyright 
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the 
#    documentation and/or other materials provided with the   
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

clean()
{
  if [ -f tmp ]; then
    rm tmp
  fi
  if [ -f outfile.txt ]; then
    rm outfile.txt
  fi
}

for file in `ls src`
do
  clean
  echo "****************\nProcessing $file..."
  filename=`echo $file | sed 's/\.//g'`
  filenamecaps=`echo $filename | tr '[:lower:]' '[:upper:]' `
  filesize=`stat -c "%s" src/$file`
  echo "Generating hex dump..."
  hexdump -v -e '1/1 "%02x"' src/$file > tmp
  echo "Formatting..."
  java hexwriter
  echo "\nFinalizing headers...\n*****************\n"
  sed -i "1i #define ${filenamecaps}_SIZE $filesize\nunsigned char $filenamecaps[]={\n" output.txt
  echo "};" >> output.txt
  mv output.txt inc/$filename.h
done
