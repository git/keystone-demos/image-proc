
REM Convert HTML pages into their corresponding hex array header files.

make

::set output_dir=..\..\..\..\inc\webpages

::pause

::php hexwriter.php ..\..\index.html %output_dir%
::php hexwriter.php ..\..\dspchip.gif %output_dir%
::php hexwriter.php ..\..\ti_logo.gif %output_dir%
::php hexwriter.php ..\..\result.html %output_dir%
::php hexwriter.php ..\..\lena512color_24bit.bmp %output_dir%
::php hexwriter.php ..\..\outfile.bmp %output_dir%
::pause
