/*
 *  Copyright 2016 by Texas Instruments Incorporated.
 *
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */

/*
 *  ======== image_processing_evmke_master.cfg ========
 *
 */

/* root of the configuration object model */
var Program = xdc.useModule('xdc.cfg.Program');
var cfgArgs = Program.build.cfgArgs;
 
var Memory = xdc.useModule('xdc.runtime.Memory');
var BIOS = xdc.useModule('ti.sysbios.BIOS');
var Timestamp = xdc.useModule('xdc.runtime.Timestamp');
var HeapMem = xdc.useModule('ti.sysbios.heaps.HeapMem');
var Task = xdc.useModule('ti.sysbios.knl.Task');
var Idle = xdc.useModule('ti.sysbios.knl.Idle');
var Swi = xdc.useModule('ti.sysbios.knl.Swi');
var Clock = xdc.useModule('ti.sysbios.knl.Clock');
var Event = xdc.useModule('ti.sysbios.knl.Event');
var Mailbox = xdc.useModule('ti.sysbios.knl.Mailbox');
var Semaphore = xdc.useModule('ti.sysbios.knl.Semaphore');
var Log = xdc.useModule('xdc.runtime.Log');
var LoggerBuf = xdc.useModule('xdc.runtime.LoggerBuf');
var Hwi = xdc.useModule('ti.sysbios.hal.Hwi');
var Main = xdc.useModule('xdc.runtime.Main');
var Defaults = xdc.useModule('xdc.runtime.Defaults');
var Diags = xdc.useModule('xdc.runtime.Diags'); 
var SysMin = xdc.useModule('xdc.runtime.SysMin');
var System = xdc.useModule('xdc.runtime.System');
var Text = xdc.useModule('xdc.runtime.Text');

/* no rts heap */
Program.heap = 0x80000;
Program.argSize = 100;  /* minimum size */
Program.stack = 0x20000;


/* Create a Heap. */
var heapMemParams = new HeapMem.Params();
heapMemParams.size = 0x10000000;
///heapMemParams.sectionName = "systemHeapMaster";
///Program.global.heap0 = HeapMem.create(heapMemParams);
///Memory.defaultHeapInstance = Program.global.heap0;

var Memory = xdc.useModule('xdc.runtime.Memory');
Memory.defaultHeapInstance = HeapMem.create(heapMemParams);

/* Load and configure the IPC packages */
var MessageQ     = xdc.useModule('ti.sdo.ipc.MessageQ');
var Ipc          = xdc.useModule('ti.sdo.ipc.Ipc');
var HeapBufMP    = xdc.useModule('ti.sdo.ipc.heaps.HeapBufMP');



xdc.global.SR0_cacheEnable = true;
xdc.global.procName = "HOST";

/* configure processor names */
var procNameAry = ["HOST", "CORE0", "CORE1", "CORE2", "CORE3"];
var MultiProc = xdc.useModule('ti.sdo.utils.MultiProc');
MultiProc.setConfig(xdc.global.procName, procNameAry);

/* ipc configuration */
var Ipc = xdc.useModule('ti.sdo.ipc.Ipc');
Ipc.procSync = Ipc.ProcSync_PAIR;
Ipc.sr0MemorySetup = true;

/* shared region configuration */
var SharedRegion = xdc.useModule('ti.sdo.ipc.SharedRegion');

/* configure SharedRegion #0 (IPC) */
var SR0Mem = Program.cpu.memoryMap["DDR3_IPC"];

SharedRegion.setEntryMeta(0,
    new SharedRegion.Entry({
        name:           "SR_0",
        base:           SR0Mem.base,
        len:            SR0Mem.len,
        ownerProcId:    0,
        isValid:        true,
        cacheEnable:    xdc.global.SR0_cacheEnable
    })
);

/* The application is using the UIA benchmark events. */ 
var UIABenchmark  = xdc.useModule('ti.uia.events.UIABenchmark');

/* Load the OSAL package */
var osType = "tirtos"
var Osal = xdc.useModule('ti.osal.Settings');
Osal.osType = osType;
/* Load the CPPI package */
var Cppi                        =   xdc.loadPackage('ti.drv.cppi'); 
/* Load the QMSS package */
var Qmss                        =   xdc.loadPackage('ti.drv.qmss');

/* Load the PA package */
var devType = "k2l"
var Pa = xdc.useModule('ti.drv.pa.Settings');
Pa.deviceType = devType;

/* Load the RM package */
var Rm                          =   xdc.loadPackage('ti.drv.rm');

var socType           = "k2l";
var Nimu 		= xdc.loadPackage('ti.transport.ndk.nimu');
Nimu.Settings.socType  = socType;


/*
** Use this load to configure NDK 2.2 and above using RTSC. In previous versions of
** the NDK RTSC configuration was not supported and you should comment this out.
*/
var Ndk       = xdc.loadPackage('ti.ndk.config');
var Global    = xdc.useModule('ti.ndk.config.Global');
/* 
** This allows the heart beat (poll function) to be created but does not generate the stack threads 
**
** Look in the cdoc (help files) to see what CfgAddEntry items can be configured. We tell it NOT
** to create any stack threads (services) as we configure those ourselves in our Main Task
** thread hpdspuaStart.
*/  
Global.enableCodeGeneration = false;
/* Global.memRawPageSize = 12288; */

Clock.timerId = -1;
//Task.defaultStackSize = 4096 * 4;
Task.defaultStackSize = 8192 * 16;

/*
 * Build a custom SYS/BIOS library from sources.
 */
BIOS.libType = BIOS.LibType_Custom;


/* Circular buffer size for System_printf() */
SysMin.bufSize = 0x400;
/* 
** Create the stack Thread Task for our application.
*/
var tskNdkStackTest  		=   Task.create("&StackTest");
//tskNdkStackTest.stackSize  	= 	0x5000;
tskNdkStackTest.stackSize  	= 	8192 * 16
tskNdkStackTest.priority    = 	0x5;
BIOS.taskEnabled			=   true;

/* 
 * Create and install logger for the whole system
 */
var loggerBufParams = new LoggerBuf.Params();
loggerBufParams.numEntries = 32;
var logger0 = LoggerBuf.create(loggerBufParams);
Defaults.common$.logger = logger0;
Main.common$.diags_INFO = Diags.ALWAYS_ON;

System.SupportProxy = SysMin;
var SemiHost = xdc.useModule('ti.sysbios.rts.gnu.SemiHostSupport');

var Cache  = xdc.useModule('ti.sysbios.family.arm.a15.Cache');
var Mmu    = xdc.useModule('ti.sysbios.family.arm.a15.Mmu');

/* Enable the cache                                                           */
Cache.enableCache = true;

// Enable the MMU (Required for L1/L2 data caching)
Mmu.enableMMU = true;

// descriptor attribute structure
var peripheralAttrs = new Mmu.DescriptorAttrs();

Mmu.initDescAttrsMeta(peripheralAttrs);

peripheralAttrs.type = Mmu.DescriptorType_BLOCK;  // BLOCK descriptor
peripheralAttrs.noExecute = true;                 // not executable
peripheralAttrs.accPerm = 0;                      // read/write at PL1
peripheralAttrs.attrIndx = 1;                     // MAIR0 Byte1 describes
                                                  // memory attributes for
// Define the base address of the 2 MB page
// the peripheral resides in.
var peripheralBaseAddrs = [ 
  { base: 0x02620000, size: 0x00001000 },  // bootcfg
  { base: 0x0bc00000, size: 0x00100000 },  // MSMC config
  { base: 0x02000000, size: 0x00100000 },  // NETCP memory
  { base: 0x02a00000, size: 0x00100000 },  // QMSS config memory
  { base: 0x23A00000, size: 0x00100000 },  // QMSS Data memory
  { base: 0x02901000, size: 0x00002000 },  // SRIO pkt dma config memory
  { base: 0x01f14000, size: 0x00007000 },  // AIF pkt dma config memory
  { base: 0x021F0200, size: 0x00000600 },  // FFTC 0 pkt dma config memory
  { base: 0x021F0a00, size: 0x00000600 },  // FFTC 4 pkt dma config memory
  { base: 0x021F1200, size: 0x00000600 },  // FFTC 5 pkt dma config memory
  { base: 0x021F4200, size: 0x00000600 },  // FFTC 1 pkt dma config memory
  { base: 0x021F8200, size: 0x00000600 },  // FFTC 2 pkt dma config memory
  { base: 0x021FC200, size: 0x00000600 },  // FFTC 3 pkt dma config memory
  { base: 0x02554000, size: 0x00009000 }   // BCP pkt dma config memory
];

// Configure the corresponding MMU page descriptor accordingly
for (var i =0; i < peripheralBaseAddrs.length; i++)
{
  for (var j = 0; j < peripheralBaseAddrs[i].size; j += 0x200000)
  {
      var addr = peripheralBaseAddrs[i].base + j;
      Mmu.setSecondLevelDescMeta(addr, addr, peripheralAttrs);
    }
}                   

// Reconfigure DDR to use coherent address
Mmu.initDescAttrsMeta(peripheralAttrs);

peripheralAttrs.type = Mmu.DescriptorType_BLOCK;
peripheralAttrs.shareable = 2;            // outer-shareable (3=inner, 0=none)
peripheralAttrs.accPerm = 1;              // read/write at any privelege level
peripheralAttrs.attrIndx = 2;             // normal cacheable (0=no cache, 1=strict order)
for (var vaddr = 0x80000000, paddr = 0x800000000; vaddr < 0x100000000; vaddr += 0x200000, paddr+= 0x200000)
{
      Mmu.setSecondLevelDescMeta(vaddr, paddr, peripheralAttrs);
}
// Add MSMC as coherent
for (var addr = 0x0c000000; addr < 0x0c600000; addr += 0x200000)
{
      Mmu.setSecondLevelDescMeta(addr, addr, peripheralAttrs);
}
