/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-B06
 */

#include <xdc/std.h>

__FAR__ char evmK2L_mcip_slave__dummy__;

#define __xdc_PKGVERS null
#define __xdc_PKGNAME evmK2L_mcip_slave
#define __xdc_PKGPREFIX evmK2L_mcip_slave_

#ifdef __xdc_bld_pkg_c__
#define __stringify(a) #a
#define __local_include(a) __stringify(a)
#include __local_include(__xdc_bld_pkg_c__)
#endif

