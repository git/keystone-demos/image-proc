/**
 *   @file  CPTHelper.c
 *
 *   @brief
 *      Provides APIs to open, start, end and close CP Tracer specific use case scenarios.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "demos/image_processing/ipc/instrumentation/inc/CPTHelper.h"
#include "demos/image_processing/ipc/instrumentation/inc/cToolsHelper.h"

#define SYSETB_EDMA_DEST_ADDRESS  0x98000000
#define EDMA_BFR_WORDS     0x10000   /*128 KBytes */

#define BYTE_SWAP32(n) \
    ( ((((uint32_t) n) << 24) & 0xFF000000) |   \
      ((((uint32_t) n) <<  8) & 0x00FF0000) |   \
      ((((uint32_t) n) >>  8) & 0x0000FF00) |   \
      ((((uint32_t) n) >> 24) & 0x000000FF) )

#if TCI6614
/* Trace Funnel */
#define C66XX_TF_CNTL                       0x025A4000
#define C66XX_TF_LOCK                       0x025A4FB0

#define TF_STM_ENABLE_VALUE                 0x3FF

/* STM config addresses */
#define C66XX_STM_CONFIG_BASE               0x025A1000

#else

/* STM config addresses */
#define C66XX_STM_CONFIG_BASE               0x02421000

#endif


#define STM_CONFIG_ATB_MSG_HEAD             (C66XX_STM_CONFIG_BASE + 0x048)

uint32_t *pDmaMemory = (uint32_t *)SYSETB_EDMA_DEST_ADDRESS;


/*******************************************************************************/
/* Status of the ETB STM data - this is required to decode the compressed data */
/*******************************************************************************/
uint8_t transferETBConfig(uint8_t wrapped)
{
    char * pFileName = "C:\\temp\\CPT_etbdata.dcm";
    FILE* fp = fopen(pFileName, "w");
    if(fp)
    {
        uint32_t value = 0;
        uint32_t hp0=0, hv0= 0;
        uint32_t hp1=0, hv1= 0;

        value = (*((volatile uint32_t*)STM_CONFIG_ATB_MSG_HEAD)) >> 16;

        /*
        hp0 = (value & (0x1<<3)) >> (3);
        hv0 = (value & (0x7));
        hp1 = (value & (0x1<<7)) >> (7);
        hv1 = (value & (0x7<<4)) >> (4);
        */
        hp0 = (value & 0x8) >> (3);
        hv0 = (value & 0x7);
        hp1 = (value & 0x80) >> (7);
        hv1 = (value & 0x70) >> (4);

        fprintf(fp, "STM_data_flip=1\n");
        fprintf(fp, "STM_Buffer_Wrapped=%d\n", wrapped);
        fprintf(fp, "HEAD_Present_0=%d\n", hp0);
        fprintf(fp, "HEAD_Pointer_0=%d\n", hv0);
        fprintf(fp, "HEAD_Present_1=%d\n", hp1);
        fprintf(fp, "HEAD_Pointer_1=%d\n", hv1);
        fprintf(fp, "Needs_Preprocess=1");

        fclose(fp);
        printf("Successfully transported STM config - %s \n", pFileName);

    }
    else
    {
        printf("Error opening file - %s \n", pFileName);
        return 1;
    }

    return 0;
}

// Application SW functions
int SystemMemory_Profile_Start(void)
{
    
#if SYS_ETB_EDMA_DRAIN
    uint32_t idx;
#endif
#if SYS_ETB_CPU_DRAIN || SYS_ETB_EDMA_DRAIN || SYS_ETB_NO_DRAIN
    ctools_etb_config_t etb_config = {0};
#endif

#if SYS_ETB_CPU_DRAIN

    etb_config.etb_mode = eETB_TI_Mode;
   // etb_config.etb_mode = eETB_TI_Mode_AND_Stop_Buffer;

    if(ctools_etb_init(CTOOLS_DRAIN_ETB_CPU, &etb_config, CTOOLS_SYS_ETB) != CTOOLS_SOK)
    {
        printf ("Error: System ETB init Failed\n");
        return(-1);
    }

#endif

#if SYS_ETB_EDMA_DRAIN

    /* channel array */
    uint32_t  edma3_channels[2] = {45,0};
    /* param array */
    uint32_t  edma3_params[5] = {100,101,102,103,104};

    /* Fill EDMA destination memory with pattern to test for writes */
    for(idx = 0; idx < (EDMA_BFR_WORDS+100); idx++)
    {
        pDmaMemory[idx] = 0xcccccccc;
    }

    etb_config.edmaConfig.channels_ptr = &edma3_channels[0];
    etb_config.edmaConfig.param_ptr    = &edma3_params[0];
    etb_config.edmaConfig.cic_sel      = eCIC_1;
    etb_config.edmaConfig.cc_sel       = 1;
    etb_config.edmaConfig.dbufAddress  = SYSETB_EDMA_DEST_ADDRESS;
    etb_config.edmaConfig.dbufBytes    = EDMA_BFR_WORDS * 4;
    etb_config.edmaConfig.mode         = CTOOLS_USECASE_EDMA_CIRCULAR_MODE;

    etb_config.etb_mode = eETB_TI_Mode;

    /* The following 2 registers enable the CIC1 inputs and map them to a
     *  specific output. The System ETB half-full and full output interrupts
     *  are connected as event inputs to CIC1 event 8 and 9. Both input events
     *  are mapped to output channel 2 using the channel map byte locations for
     *  events 8 and 9 (reg0 3-0, reg1 7-4, reg2 11-8, ...). Output channel 2 is
     *  connected to the EDMA3 input channel 45, assigned above.
     */
    CIC_CHMAP_REG2(eCIC_1)  = 0x00000202;
    CIC_ENABLE_REG(eCIC_1,0) = (0x3 << 8); /* Set bits 8 & 9 */

    /* Enable host(output) event 2 in relation to channel mapping */
    CIC_ENABLE_HINT_REG(eCIC_1,0) = 0x4;
    CIC_ENABLE_GHINT_REG(eCIC_1) = 0x1;    /* Global host interrupt enable */

    if(ctools_etb_init(CTOOLS_DRAIN_ETB_EDMA, &etb_config, CTOOLS_SYS_ETB) != CTOOLS_SOK)
    {
        printf ("Error: System ETB init Failed\n");
        return(-1);
    }

#endif


#if SYS_ETB_NO_DRAIN

    if(ctools_etb_init(CTOOLS_DRAIN_NONE, &etb_config, CTOOLS_SYS_ETB) != CTOOLS_SOK)
    {
        printf ("Error: System ETB init Failed\n");
        return(-1);
    }

#endif

    if(ctools_systemtrace_init(CTOOLS_SYS_TRACE_ALL) != CTOOLS_SOK)
    {
        printf ("Error: System Trace init Failed\n");
        return(-1);
    }


#if defined(SYS_BANDWIDTH_PROFILE) || defined(SYS_LATENCY_PROFILE)

    ctools_cpt_sysprofilecfg SysMemory_Profile_Params = {NULL};

    ctools_cpt_modidqual SysMemory_ModIDQual[5];

    int i;

    for(i=0;i<5;i++)
    {
        SysMemory_ModIDQual[i].TPCntQual = NULL;
    }

    SysMemory_ModIDQual[0].CPT_ModId = eCPT_MSMC_0;
    SysMemory_ModIDQual[1].CPT_ModId = eCPT_MSMC_1;
    SysMemory_ModIDQual[2].CPT_ModId = eCPT_MSMC_2;
    SysMemory_ModIDQual[3].CPT_ModId = eCPT_MSMC_3;
    SysMemory_ModIDQual[4].CPT_ModId = eCPT_DDR;

    //Populate SysMemory Profiler Params
    SysMemory_Profile_Params.ModIDQual = SysMemory_ModIDQual;
    SysMemory_Profile_Params.CPT_ModCnt = 5;

    SysMemory_Profile_Params.CPUClockRateMhz = 983;
    SysMemory_Profile_Params.SampleWindowSize = 81916;

#if SYS_BANDWIDTH_PROFILE

    // Open System memory (MSMC and DDR3) profiler
    if (ctools_cpt_sysbwprofile_open(&SysMemory_Profile_Params) != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_sysbandwidthprofile_open Failed\n");
        return(-1);
    }

#elif SYS_LATENCY_PROFILE

    // Open System memory (MSMC and DDR3) profiler
    if (ctools_cpt_syslatprofile_open(&SysMemory_Profile_Params) != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_syslatencyprofile_open Failed\n");
        return(-1);
    }

#endif

    // Start System memory (MSMC and DDR3) profiler
    if (ctools_cpt_globalstart() != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_globalstart Failed\n");
        return(-1);
    }

#endif

#ifdef TOTAL_BANDWIDTH_PROFILE

    ctools_cpt_totalprofilecfg Total_Profile_Params;
    ctools_cpt_masteridfiltercfg  MstID_Cfg;
    
    memset((void *) &Total_Profile_Params, 0, sizeof(Total_Profile_Params));
    memset((void *) &MstID_Cfg, 0, sizeof(MstID_Cfg));
    
    eCPT_MasterID MstID_Array[] = {eCPT_MID_GEM0};

    Total_Profile_Params.CPT_ModId = eCPT_DDR; //Profiling accesses to DDR3 memory
    MstID_Cfg.CPT_MasterID = MstID_Array;
    MstID_Cfg.MasterIDgroup_Enable = 0;
    MstID_Cfg.CPT_MasterIDCnt = 1;
    Total_Profile_Params.TPCnt_MasterID = &MstID_Cfg;
    Total_Profile_Params.TPCntQual = NULL;
    Total_Profile_Params.Address_Filter_Params = NULL; //Address filtering is disabled
    Total_Profile_Params.CPUClockRateMhz = 983;
    Total_Profile_Params.SampleWindowSize = 81916;

    // Open DDR3 Total bandwidth profiler
    if (ctools_cpt_totalprofile_open(&Total_Profile_Params) != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_totalprofile_open failed\n");
        return(-1);
    }

    // Start DDR3 Total bandwidth profiler
    if (ctools_cpt_singlestart(eCPT_DDR) != CTOOLS_SOK)
    {
        printf ("Error: Total Bandwidth ctools_cpt_singlestart Failed\n");
        return(-1);
    }

#endif

#ifdef MASTER_BANDWIDTH_PROFILE

    ctools_cpt_masterprofilecfg Master_Profile_Params = {NULL};
    ctools_cpt_masteridfiltercfg  MstID0_Cfg = {NULL};
    ctools_cpt_masteridfiltercfg  MstID1_Cfg = {NULL};
    eCPT_MasterID MstID0_Array[] = {eCPT_MID_GEM0}; // Master core
#if defined(_C6657)
    eCPT_MasterID MstID1_Array[] = {eCPT_MID_GEM1}; // Slave cores
    MstID1_Cfg.CPT_MasterIDCnt = 1;
#elif defined(TCI6614) || defined(_C6670)
    eCPT_MasterID MstID1_Array[] = {eCPT_MID_GEM1, eCPT_MID_GEM2, eCPT_MID_GEM3}; // Slave cores
    MstID1_Cfg.CPT_MasterIDCnt = 3;
#elif defined(_C6678)
    eCPT_MasterID MstID1_Array[] = {eCPT_MID_GEM1, eCPT_MID_GEM2, eCPT_MID_GEM3, eCPT_MID_GEM4, eCPT_MID_GEM5, eCPT_MID_GEM6, eCPT_MID_GEM7}; // Slave cores
    MstID1_Cfg.CPT_MasterIDCnt = 7;
#endif

    Master_Profile_Params.CPT_ModId = eCPT_DDR; //Profiling accesses to DDR3 memory
    MstID0_Cfg.CPT_MasterID = MstID0_Array;
    MstID0_Cfg.MasterIDgroup_Enable = 0;
    MstID1_Cfg.CPT_MasterID = MstID1_Array;
    MstID1_Cfg.MasterIDgroup_Enable = 0;
    MstID0_Cfg.CPT_MasterIDCnt = 1;
    Master_Profile_Params.TPCnt0_MasterID = &MstID0_Cfg;
    Master_Profile_Params.TPCnt1_MasterID = &MstID1_Cfg;
    Master_Profile_Params.TPCnt0Qual = NULL;
    Master_Profile_Params.TPCnt1Qual = NULL;
    Master_Profile_Params.Address_Filter_Params = NULL; //Address filtering is disabled
    Master_Profile_Params.CPUClockRateMhz = 983;
    Master_Profile_Params.SampleWindowSize = 81916;

    // Open DDR3 Master bandwidth profiler
    if (ctools_cpt_masterprofile_open(&Master_Profile_Params) != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_masterprofile_open failed\n");
        return(-1);
    }

    // Start DDR3 Master bandwidth profiler
    if (ctools_cpt_singlestart(eCPT_DDR) != CTOOLS_SOK)
    {
        printf ("Error: Master Bandwidth ctools_cpt_singlestart Failed\n");
        return(-1);
    }

#endif

#ifdef EVENT_PROFILE

    ctools_cpt_eventtracecfg EventTrace_Params;

    ctools_cpt_masteridfiltercfg  MstID_Cfg;
    ctools_cpt_addressfiltercfg AddrFilter_Cfg;

    memset(&EventTrace_Params, 0, sizeof(EventTrace_Params));
    memset(&MstID_Cfg, 0, sizeof(MstID_Cfg));
    memset(&AddrFilter_Cfg, 0, sizeof(AddrFilter_Cfg));
    
    //Put a SoC level watchpoint on 0x99000000-0x99000004
    //In keystone devices, by default 0x8000_0000 to 0x8FFF_FFFF is mapped to 0x8_0000_0000 to 0x8_7FFF_FFFF
    AddrFilter_Cfg.AddrFilterMSBs = 0x8;
    AddrFilter_Cfg.CPT_FilterMode = eCPT_FilterMode_Inclusive;
    AddrFilter_Cfg.EndAddrFilterLSBs = 0x19000008;
    AddrFilter_Cfg.StartAddrFilterLSBs = 0x19000004;

#if defined(_C6657)
    eCPT_MasterID MstID_Array[] = {eCPT_MID_GEM1};
    MstID_Cfg.CPT_MasterIDCnt = 1;
#elif defined(TCI6614) || defined(_C6670)
    eCPT_MasterID MstID_Array[] = {eCPT_MID_GEM1, eCPT_MID_GEM2, eCPT_MID_GEM3};
    MstID_Cfg.CPT_MasterIDCnt = 3;
#elif defined(_C6678)
    eCPT_MasterID MstID_Array[] = {eCPT_MID_GEM1, eCPT_MID_GEM2, eCPT_MID_GEM3, eCPT_MID_GEM4, eCPT_MID_GEM5, eCPT_MID_GEM6, eCPT_MID_GEM7};
    MstID_Cfg.CPT_MasterIDCnt = 7;
#endif

    EventTrace_Params.CPT_ModId = eCPT_DDR; //Capturing New Request event @ DDR3 memory slave
    MstID_Cfg.CPT_MasterID = MstID_Array;
    MstID_Cfg.MasterIDgroup_Enable = 0;
    EventTrace_Params.EventTrace_MasterID = &MstID_Cfg;
    EventTrace_Params.TPEventQual = NULL;

    EventTrace_Params.Address_Filter_Params = &AddrFilter_Cfg;
    EventTrace_Params.AddrExportMask = 0; // Address Bits 10:0 are exported

    // Open DDR3 New request event capture
    if (ctools_cpt_eventtrace_open(&EventTrace_Params) != CTOOLS_SOK)
    {
        printf ("Error: New Request event capture open Failed\n");
        return(-1);
    }

    // Start DDR3 New Request event profiler
    if (ctools_cpt_singlestart(eCPT_DDR) != CTOOLS_SOK)
    {
        printf ("Error: New Request event ctools_cpt_singlestart Failed\n");
        return(-1);
    }

#endif

    return(0);

}


int SystemMemory_Profile_Stop(void)
{

#if defined(SYS_BANDWIDTH_PROFILE) || defined(SYS_LATENCY_PROFILE)

    // Stop System memory (MSMC and DDR3) profiler
    if (ctools_cpt_globalstop() != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_globalstop Failed\n");
        return(-1);
    }

    // Close System memory (MSMC and DDR3) profiler
    if (ctools_cpt_globalclose() != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_globalclose Failed\n");
        return(-1);
    }

#endif

#ifdef TOTAL_BANDWIDTH_PROFILE

    // Stop DDR3 Total bandwidth profiler
    if (ctools_cpt_singlestop(eCPT_DDR) != CTOOLS_SOK)
    {
        printf ("Error: Total Bandwidth ctools_cpt_singlestop Failed\n");
        return(-1);
    }

    // Close DDR3 Total bandwidth profiler
    if (ctools_cpt_singleclose(eCPT_DDR) < 0)
    {
        printf ("Error: Total Bandwidth ctools_cpt_singleclose Failed\n");
        return(-1);
    }

#endif

#ifdef MASTER_BANDWIDTH_PROFILE

    // Stop DDR3 Master bandwidth profiler
    if (ctools_cpt_singlestop(eCPT_DDR) != CTOOLS_SOK)
    {
        printf ("Error: Master Bandwidth ctools_cpt_singlestop Failed\n");
        return(-1);
    }

    // Close DDR3 Master bandwidth profiler
    if (ctools_cpt_singleclose(eCPT_DDR) != CTOOLS_SOK)
    {
        printf ("Error: Master Bandwidth ctools_cpt_singleclose Failed\n");
        return(-1);
    }

#endif

#ifdef EVENT_PROFILE

    // Stop DDR3 New request event profiler
    if (ctools_cpt_singlestop(eCPT_DDR) != CTOOLS_SOK)
    {
        printf ("Error: New request event ctools_cpt_singlestop Failed\n");
        return(-1);
    }

    // Close DDR3 New request event profiler
    if (ctools_cpt_singleclose(eCPT_DDR) != CTOOLS_SOK)
    {
        printf ("Error: New request event ctools_cpt_singleclose Failed\n");
        return(-1);
    }

#endif

#if 1

#if SYS_ETB_CPU_DRAIN

    uint32_t size_out, idx, wrap_flag;
    uint32_t*   pBuffer = 0;

    /* Fill EDMA destination memory with pattern to test for writes */
    for(idx = 0; idx < (EDMA_BFR_WORDS+100); idx++)
    {
        pDmaMemory[idx] = 0xcccccccc;
    }

    if(ctools_etb_cpu_drain(pDmaMemory, 32768, &size_out, &wrap_flag, CTOOLS_SYS_ETB) != CTOOLS_SOK)
    {
        printf ("Error: CPU ETB Drain Failed\n");
        return(-1);
    }

    /* Transport the ETB data captured */
        {
            /* this example uses JTAG debugger via CIO to transport data to host PC */
            /* An app can deploy any other transport mechanism to move the ETB buffer to the PC */
            FILE* fp = fopen("C:\\temp\\CPT_SYS_etbdata.bin", "wb");
            if(fp)
            {
                uint32_t sz = 0;
                uint32_t i = 1;
                char *le = (char *) &i;
                pBuffer = pDmaMemory;

                while(sz < size_out)
                {
                    uint32_t etbword = *(pBuffer+sz);
                    if(le[0] != 1) //Big endian
                        etbword = BYTE_SWAP32(etbword);

                    fwrite((void*) &etbword, 4, 1, fp);
                    sz++;
                }

                printf("Successfully transported ETB data\n");

                fclose(fp);
            }
        }

        /* Now transer the ETM configuration to help decode */
        transferETBConfig(wrap_flag);
#endif

#if SYS_ETB_EDMA_DRAIN

    uint32_t size_out;
    uint32_t*   pBuffer = 0;
    ctools_edma_result_t pct_edma_res;
    uint32_t retSize=0;

    if(ctools_etb_edma_drain(&pct_edma_res, CTOOLS_SYS_ETB) != CTOOLS_SOK)
    {
        printf ("Error: EDMA ETB Drain Failed\n");
    }

    pBuffer = (uint32_t *)pct_edma_res.startAddr;

    retSize = pct_edma_res.availableWords;

    if(pct_edma_res.isWrapped)
    {
        printf("ETB DrainBfr wrapped, Start: 0x%x, Size: %d\n",
                pct_edma_res.startAddr, retSize);
    }
    else
    {
        printf("ETB DrainBfr not wrapped, Start: 0x%x, Size: %d\n",
                pct_edma_res.startAddr, retSize);
    }

    /* Transport the ETB data captured */
        {
            /* this example uses JTAG debugger via CIO to transport data to host PC */
            /* An app can deploy any other transport mechanism to move the ETB buffer to the PC */
            FILE* fp = fopen("C:\\temp\\CPT_SYS_etbdata.bin", "wb");
            if(fp)
            {
                int32_t  cnt;
                uint32_t sz = 0;
                uint32_t i = 1;
                char *le = (char *) &i;

                /* For manual ETB drain buffer read mode, set the buffer ending
                 *  address to know when the buffer has wrapped back to the
                 *  beginning.
                 */
                uint32_t endAddress = pct_edma_res.dbufAddress + (pct_edma_res.dbufWords*4) - 1;
                sz = 0;
                for(cnt = 0; cnt < retSize; cnt++)
                {
                    uint32_t etbword = *(pBuffer+sz);
                    if(le[0] != 1) //Big endian
                       etbword = BYTE_SWAP32(etbword);

                    fwrite((void*) &etbword, 4, 1, fp);
                    sz++;

                    /* Check for wrap condition with circular buffer */
                    if((uint32_t)(pBuffer+sz) > endAddress)
                    {
                        pBuffer = (uint32_t *)pct_edma_res.dbufAddress;
                        sz = 0;
                    }
                }

                printf("Successfully transported ETB data\n");

                fclose(fp);
            }
        }

        /* Now transer the ETM configuration to help decode */
        transferETBConfig(pct_edma_res.isWrapped);
#endif

#endif

    if(ctools_systemtrace_shutdown(CTOOLS_SYS_TRACE_ALL) != CTOOLS_SOK)
    {
        printf ("Error: System Trace shutdown Failed\n");
    }

    return(0);

}
