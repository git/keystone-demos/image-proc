/** 
 *   @file  trace_function_profiling.c
 *
  *   @brief
 *      Provides APIs to start and stop statistical profile trace jobs for capturing PC and timing trace at a fixed sampling period
 *       for calculating % cycle distribution among various functions in a given application.
 *  \par
 *  NOTE:
 *      (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/


/*******************************************************************************
 * IF UPLOAD_TO_FILE_ENABLE is '1': INSTRUCTIONS FOR READING THE TRACE SAMPLES
 *******************************************************************************
 *******************************************************************************
 *  - Trace, ETB based, data is collected and stored in c:\temp\etbdata.bin
 *  - bin2tdf utility is required, located in:
 *    -> cd <CCS install dir>\ccs_base\emulation\analysis\bin
 *  - To convert binary trace data captured by the ETB, use the following
 *     command line:
 *    -> bin2tdf -bin C:/temp/etbdata.bin -app $(OUTFILE_PATH) -procid 66x -sirev 1 -rcvr ETB
 *       -output C:/temp/myTrace.tdf -dcmfile c:/temp/foo.dcm
 *       -sourcepaths $(PROJECT_SOURCE_PATH)
 *  - The myTrace.tdf file may be opened from the CCS menu Tools->
 *  - Trace Analyzer->Open Trace File In New View...
 *
 *******************************************************************************
 ******************************************************************************/

/**************************************************************************
 *************************** Include Files ********************************
 **************************************************************************/

#include <Ctools_UCLib/include/ctools_uclib.h>
#include <ETBLib/src/ti/edma_dev-c66xx.h>

#include "stdio.h"


#define BYTE_SWAP32(n) \
	( ((((uint32_t) n) << 24) & 0xFF000000) |	\
	  ((((uint32_t) n) <<  8) & 0x00FF0000) |	\
	  ((((uint32_t) n) >>  8) & 0x0000FF00) |	\
	  ((((uint32_t) n) >> 24) & 0x000000FF) )

#define CORE0_EDMA_DEST_ADDRESS  0x98000000

#define EDMA_BFR_WORDS     0x100000   /*4 MBytes */

#define TEMP_BFR_READ_SIZE 0x400


int statistical_profiling_start (void)
{
	ctools_etb_config_t etb_config = {0};

	/* channel array */
    uint32_t  edma3_channels[2] = {46,0};
    /* param array */
    uint32_t  edma3_params[5] = {100,101,102,103,104};

    etb_config.edmaConfig.channels_ptr = &edma3_channels[0];
    etb_config.edmaConfig.param_ptr    = &edma3_params[0];
#ifdef _C6657
    etb_config.edmaConfig.cc_sel       = (ctools_edma_cc_select_e)2;
#else
    etb_config.edmaConfig.cc_sel       = (ctools_edma_cc_select_e)1;
#endif
    etb_config.edmaConfig.dbufAddress  = CORE0_EDMA_DEST_ADDRESS;
    etb_config.edmaConfig.dbufBytes    = EDMA_BFR_WORDS * 4;
    etb_config.edmaConfig.mode         = CTOOLS_USECASE_EDMA_STOP_BUF_MODE;

#if !defined(_C6657)

#ifdef _C6678

    etb_config.edmaConfig.cic_sel      = eCIC_2;
    
    CIC_CHMAP_REG(eCIC_2,2)   = 0x04000000;
    CIC_CHMAP_REG(eCIC_2,3)   = 0x05050004;
    CIC_CHMAP_REG(eCIC_2,4)   = 0x00060600;
    CIC_CHMAP_REG(eCIC_2,5)   = 0x00000707;
    CIC_CHMAP_REG(eCIC_2,29)  = 0x08080000;
    CIC_CHMAP_REG(eCIC_2,30)  = 0x00090900;
    CIC_CHMAP_REG(eCIC_2,31)  = 0x0B000A0A;
    CIC_CHMAP_REG(eCIC_2,32)  = 0x0000000B;

    /* Set the corresponding enable bits for specific C6678 core TETB HFULL/FULL:
     *  Core 0-7, events 11/12, 14/15, 17/18, 20/21, 118/119, 121/122, 124/125, 127/128  ==> Register offset 0 */
    if(DNUM < 4)
        CIC_ENABLE_REG(eCIC_2,0) = (0x3 << (11 + (3*DNUM)));
    else
        CIC_ENABLE_REG(eCIC_2,3) = (0x3 << (10 + (3*DNUM)));
        
    if(DNUM == 7)
        CIC_ENABLE_REG(eCIC_2,4) = 0x1;
    
    CIC_ENABLE_HINT_REG(eCIC_2,0) = 0xF0;   /* Enable host interrupts 4 - 7  */
    CIC_ENABLE_HINT_REG(eCIC_2,1) = 0x0F;   /* Enable host interrupts 8 - 11 */
    CIC_ENABLE_GHINT_REG(eCIC_2)  = 0x1;    /* Global host interrupt enable */

#else

    etb_config.edmaConfig.cic_sel      = eCIC_1;

	CIC_CHMAP_REG(eCIC_1,2)   = 0x03000000;
	CIC_CHMAP_REG(eCIC_1,3)   = 0x04040003;
	CIC_CHMAP_REG(eCIC_1,4)   = 0x00050500;
	CIC_CHMAP_REG(eCIC_1,5)   = 0x00000606;

	/* Set the corresponding enable bits for specific C6670 core TETB HFULL/FULL:
     *  Core 0-3, events 11/12, 14/15, 17/18, 20/21  ==> Register offset 0 */

	CIC_ENABLE_REG(eCIC_1,0) = (0x3 << (11 + (3*DNUM)));

	CIC_ENABLE_HINT_REG(eCIC_1,0) = 0x78;  /* Enable host interrupts 3-6 */
	CIC_ENABLE_GHINT_REG(eCIC_1)  = 0x1;    /* Global host interrupt enable */
    
#endif

#endif

    if(ctools_etb_init(CTOOLS_DRAIN_ETB_EDMA, &etb_config, CTOOLS_DSP_ETB) != CTOOLS_SOK)
    {
        printf ("Error: DSP ETB init Failed\n");
        return(-1);
    }

    if(ctools_dsptrace_init() != CTOOLS_SOK)
    {
        printf ("Error: DSP Trace init Failed\n");
        return(-1);
    }

    if (ctools_stat_prof_start(20023) != CTOOLS_SOK)
    {
        printf ("Error: ctools_stat_prof_start Failed\n");
        return(-1);
    }

	return(0);
}


int statistical_profiling_end (void)
{
    if (ctools_stat_prof_end() != CTOOLS_SOK)
    {
        printf ("Error: ctools_stat_prof_end Failed\n");
        return(-1);
    }

    uint32_t*   pBuffer = 0;
    ctools_edma_result_t pct_edma_res;
    uint32_t retSize=0;

    if(ctools_etb_edma_drain(&pct_edma_res, CTOOLS_DSP_ETB) != CTOOLS_SOK)
    {
        printf ("Error: EDMA ETB Drain Failed\n");
        return(-1);
    }

    pBuffer = (uint32_t *)pct_edma_res.startAddr;

    retSize = pct_edma_res.availableWords;

    if(pct_edma_res.isWrapped)
    {
        printf("ETB DrainBfr wrapped, Start: 0x%x, Size: %d\n",
                pct_edma_res.startAddr, retSize);
    }
    else
    {
        printf("ETB DrainBfr not wrapped, Start: 0x%x, Size: %d\n",
                pct_edma_res.startAddr, retSize);
    }

    /* Transport the ETB data captured */
	{
		/* this example uses JTAG debugger via CIO to transport data to host PC */
		/* An app can deploy any other transport mechanism to move the ETB buffer to the PC */
		FILE* fp = fopen("C:\\temp\\StatPof_etbdata.bin", "wb");
		if(fp)
		{
			int32_t  cnt;
			uint32_t sz = 0;
			uint32_t i = 1;
			char *le = (char *) &i;

			/* For manual ETB drain buffer read mode, set the buffer ending
			 *  address to know when the buffer has wrapped back to the
			 *  beginning.
			 */
			uint32_t endAddress = pct_edma_res.dbufAddress + (pct_edma_res.dbufWords*4) - 1;
			sz = 0;
			for(cnt = 0; cnt < retSize; cnt++)
			{
				uint32_t etbword = *(pBuffer+sz);
				if(le[0] != 1) //Big endian
				   etbword = BYTE_SWAP32(etbword);

				fwrite((void*) &etbword, 4, 1, fp);
				sz++;

				/* Check for wrap condition with circular buffer */
				if((uint32_t)(pBuffer+sz) > endAddress)
				{
					pBuffer = (uint32_t *)pct_edma_res.dbufAddress;
					sz = 0;
				}
			}

			printf("Successfully transported StatProf ETB data\n");

			fclose(fp);
		}
	}

    if(ctools_dsptrace_shutdown() != CTOOLS_SOK)
    {
        printf("Error: DSP Trace shutdown Failed\n");
        return(-1);
    }

	return(0);
}
