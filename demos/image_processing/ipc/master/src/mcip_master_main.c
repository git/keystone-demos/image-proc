/*
 * Copyright (C) 2011-2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#if (defined(DEVICE_C6678)||defined(DEVICE_C6657))
#include <c6x.h>
#endif
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <ti/ndk/inc/netmain.h>
#include <demos/image_processing/ipc/master/src/system/nimu_cppi_qmss_iface.h>
#include <ti/ndk/inc/_stack.h>
#include <ti/ndk/inc/tools/console.h>
#include <ti/ndk/inc/tools/servers.h>

#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/IHeap.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/HeapBufMP.h>
#include <ti/ipc/MultiProc.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <xdc/cfg/global.h>

#include "ti/board/board.h"
#include "demos/image_processing/ipc/common/inc/mcip_core.h"
#include "demos/image_processing/ipc/master/inc/mcip_process.h"
#include "demos/image_processing/ipc/master/inc/mcip_webpage.h"

#include "ti/drv/uart/UART_stdio.h"



#include "ti/csl/csl_chipAux.h"



#ifdef AETINT_MEMWATCH

#include <Ctools_UCLib/include/ctools_uclib.h>

extern void C66x_exception_handler (void);


#endif

/**************************************************************************
 ** NDK static configuration
 ****************************************************************************/

char HostName[CFG_HOSTNAME_MAX] = {0};
char *LocalIPAddr = "0.0.0.0";          /* Set to "0.0.0.0" for DHCP client option */
char *PCStaticIP  = "192.168.2.101";    /* Static IP address for host PC */
char *EVMStaticIP = "192.168.2.100";    /*    "   IP     "   for EVM */
char *LocalIPMask = "255.255.254.0";    /* Mask for DHCP Server option */
char *GatewayIP   = "192.168.2.101";    /* Not used when using DHCP */
char *DomainName  = "demo.net";         /* Not used when using DHCP */
char *DNSServer   = "0.0.0.0";          /* Used when set to anything but zero */

/**************************************************************************
 ** IP Stack - NDK Routines
 ***************************************************************************/

/* Our NETCTRL callback functions */
static void   NetworkOpen();
static void   NetworkClose();
static void   NetworkIPAddr( IPN IPAddr, uint32_t IfIdx, uint32_t fAdd );

/*  Reporting function - IP stack calls it to give us updates */
static void   ServiceReport( uint32_t Item, uint32_t Status, uint32_t Report, void* hCfgEntry );

static int number_of_cores = 0;

void EVM_init(void);

int master_main(void)
{
    void* hCfg;
    CI_SERVICE_HTTP   http;            /* Configuration data for http including handle */
    CI_SERVICE_DHCPC dhcpservice;    /* Configuration data for dhcp client including handle */
    uint8_t dhcp_options[] = {DHCPOPT_SERVER_IDENTIFIER, DHCPOPT_ROUTER};
    HeapBufMP_Handle heapHandle;
    HeapBufMP_Params heapBufParams;
    Int              status;
#ifndef DEVICE_C6657
    NIMU_QMSS_CFG_T      qmss_cfg;
    NIMU_CPPI_CFG_T      cppi_cfg;
#endif
	int rc;
	
	///EVM_init();

    platform_write("\n\nMCSDK IMAGE PROCESSING DEMONSTRATION\n\n");

#ifndef DEVICE_C6657
	
    /* Initialize the components required to run this application:
    *  (1) QMSS
    *  (2) CPPI
    *  (3) Packet Accelerator
    */

    /* Initialize QMSS */
    if (CSL_chipReadDNUM() == 0)
    {
        qmss_cfg.master_core        = 1;
    } else
    {
    	qmss_cfg.master_core        = 0;
    }

    qmss_cfg.max_num_desc       = MAX_NUM_DESC;
    qmss_cfg.desc_size          = MAX_DESC_SIZE;
    qmss_cfg.mem_region         = Qmss_MemRegion_MEMORY_REGION0;
    if (NIMU_initQmss (&qmss_cfg) != 0)
    {
    	platform_write ("Failed to initialize the QMSS subsystem \n");
        goto close_n_exit;
    }
    else
    {
    	platform_write ("QMSS successfully initialized \n");
    }

    /* Initialize CPPI */
    if (CSL_chipReadDNUM() == 0)
    {
        cppi_cfg.master_core        = 1;
    } else
    {
    	cppi_cfg.master_core        = 0;
    }

    cppi_cfg.dma_num            = Cppi_CpDma_PASS_CPDMA;
    cppi_cfg.num_tx_queues      = NUM_PA_TX_QUEUES;
    cppi_cfg.num_rx_channels    = NUM_PA_RX_CHANNELS;
    if (NIMU_initCppi (&cppi_cfg) != 0)
    {
    	platform_write ("Failed to initialize CPPI subsystem \n");
        goto close_n_exit;
    }
    else
    {
    	platform_write ("CPPI successfully initialized \n");
    }


    if (NIMU_initPass()!= 0) {
    	platform_write ("Failed to initialize the Packet Accelerator \n");
        goto close_n_exit;
    }
    else
    {
    	platform_write ("PA successfully initialized \n");
    }
#endif

    status = NC_SystemOpen( NC_PRIORITY_LOW, NC_OPMODE_INTERRUPT );
    if(status != NC_OPEN_SUCCESS)
    {
        platform_write("NC_SystemOpen Failed (%d)\n",status);
        goto close_n_exit;
    }

    /* Create the heap that will be used to allocate messages. */
    HeapBufMP_Params_init(&heapBufParams);
    heapBufParams.regionId       = 0;
    heapBufParams.name           = IMAGE_PROCESSING_HEAP_NAME;
    heapBufParams.numBlocks      = number_of_cores;
    heapBufParams.blockSize      = sizeof(process_message_t);
    heapHandle = HeapBufMP_create(&heapBufParams);
    if (heapHandle == NULL) {
        platform_write("Main: HeapBufMP_create failed\n" );
        goto close_n_exit;
    }

    /* Register this heap with MessageQ */
    status = MessageQ_registerHeap((IHeap_Handle)heapHandle, IMAGE_PROCESSING_HEAPID);
    if(status != MessageQ_S_SUCCESS) {
        platform_write("Main: MessageQ_registerHeap failed\n" );
        goto close_n_exit;
    }

    if (mc_process_init(number_of_cores)) {
        platform_write("mc_process_init returns error\n");
        goto close_n_exit;
    }

    /* Create a new configuration */
    hCfg = CfgNew();
    if( !hCfg )
    {
        platform_write("Unable to create configuration\n");
        goto close_n_exit;
    }

    /* Validate the length of the supplied names */
    if( strlen( DomainName ) >= CFG_DOMAIN_MAX ||
            strlen( HostName ) >= CFG_HOSTNAME_MAX )
    {
        platform_write("Domain or Host Name too long\n");
        goto close_n_exit;
    }

    /* Add our global hostname to hCfg (to be claimed in all connected domains) */
    CfgAddEntry( hCfg, CFGTAG_SYSINFO, CFGITEM_DHCP_HOSTNAME, 0,
            strlen(HostName), (uint8_t *)HostName, 0 );

	/* increase stack size */
    rc = 8192;
    CfgAddEntry(hCfg, CFGTAG_OS, CFGITEM_OS_TASKSTKBOOT, CFG_ADDMODE_UNIQUE, sizeof(uint32_t), (uint8_t *)&rc, 0 );

#ifndef TCI6614
    /*
     ** Read User SW 1
     ** If user SW 1 = OFF position: static IP mode (default), SW 1 = ON: client mode
     */
    ///if (!platform_get_switch_state(1)) {
    if (0) {
        CI_IPNET NA;
        CI_ROUTE RT;
        IPN      IPTmp;

        /* Setup an IP address to this EVM */
        bzero( &NA, sizeof(NA) );
        NA.IPAddr  = inet_addr(EVMStaticIP);
        NA.IPMask  = inet_addr(LocalIPMask);
        strcpy( NA.Domain, DomainName );

        /* Add the address to interface 1 */
        CfgAddEntry( hCfg, CFGTAG_IPNET, 1, 0, sizeof(CI_IPNET), (uint8_t *)&NA, 0 );

        /* Add the default gateway (back to user PC) */
        bzero( &RT, sizeof(RT) );
        RT.IPDestAddr = inet_addr(PCStaticIP);
        RT.IPDestMask = inet_addr(LocalIPMask);
        RT.IPGateAddr = inet_addr(GatewayIP);

        /* Add the route */
        CfgAddEntry( hCfg, CFGTAG_ROUTE, 0, 0, sizeof(CI_ROUTE), (uint8_t *)&RT, 0 );

        /* Manually add the DNS server when specified */
        IPTmp = inet_addr(DNSServer);
        if( IPTmp )
            CfgAddEntry( hCfg, CFGTAG_SYSINFO, CFGITEM_DHCP_DOMAINNAMESERVER,
                    0, sizeof(IPTmp), (uint8_t *)&IPTmp, 0 );

        platform_write("EVM in StaticIP mode at %s\n", EVMStaticIP);
        platform_write("Set IP address of PC to %s\n", PCStaticIP);
    }
    else
#endif
    {
        platform_write("Configuring DHCP client\n");

        bzero( &dhcpservice, sizeof(dhcpservice) );
        dhcpservice.cisargs.Mode   = CIS_FLG_IFIDXVALID;
        dhcpservice.cisargs.IfIdx  = 1;
        dhcpservice.cisargs.pCbSrv = &ServiceReport;
        dhcpservice.param.pOptions = dhcp_options;
        dhcpservice.param.len = 2;
        CfgAddEntry( hCfg, CFGTAG_SERVICE, CFGITEM_SERVICE_DHCPCLIENT, 0,
                sizeof(dhcpservice), (uint8_t *)&dhcpservice, &(dhcpservice.cisargs.hService) );
    }

    /* Add web files */
    image_processing_webfiles_add();

    /* Specify HTTP service */
    bzero( &http, sizeof(http) );
    http.cisargs.IPAddr = INADDR_ANY;
    http.cisargs.pCbSrv = &ServiceReport;
    CfgAddEntry( hCfg, CFGTAG_SERVICE, CFGITEM_SERVICE_HTTP, 0,
            sizeof(http), (uint8_t *)&http, &(http.cisargs.hService) );

    /*
     ** Configure IPStack/OS Options
     */

    /* Set debug message level */
    status = DBG_WARN;
    CfgAddEntry( hCfg, CFGTAG_OS, CFGITEM_OS_DBGPRINTLEVEL,
            CFG_ADDMODE_UNIQUE, sizeof(uint32_t), (uint8_t *)&status, 0 );

    /*
     ** Boot the system using this configuration
     **
     ** We keep booting until the function returns 0. This allows
     ** us to have a "reboot" command.
     */

    do
    {
        status = NC_NetStart( hCfg, NetworkOpen, NetworkClose, NetworkIPAddr );
    } while( status > 0 );

    platform_write ("Shutting things down\n");

close_n_exit:

    /* Free the WEB files */
    image_processing_webfiles_remove();
    /* Delete Configuration */
    CfgFree( hCfg );
    NC_SystemClose();
    return 0;
}

/*
 *Main Entry Point
 */
int main(void)
{

#ifdef AETINT_MEMWATCH

    if(ctools_dsptrace_init() != CTOOLS_SOK)
    {
        platform_write ("Error: DSP ETB init Failed\n");
    }

    if(ctools_pct_start_exc() != CTOOLS_SOK)
    {
        platform_write ("Error: PC Trace exception start Failed\n");
    }

#endif

	/*
     *  Ipc_start() calls Ipc_attach() to synchronize all remote processors
     *  because 'Ipc.procSync' is set to 'Ipc.ProcSync_ALL' in *.cfg
     */
    Ipc_start();

    /* Start the BIOS 6 Scheduler */
    BIOS_start ();
    return 1;
}

/*************************************************************************
 *  @b EVM_init()
 *
 *  @n
 *
 *  Initializes the platform hardware. This routine is configured to start in
 *     the evm.cfg configuration file. It is the first routine that BIOS
 *     calls and is executed before Main is called. If you are debugging within
 *  CCS the default option in your target configuration file may be to execute
 *  all code up until Main as the image loads. To debug this you should disable
 *  that option.
 *
 *  @param[in]  None
 *
 *  @retval
 *      None
 ************************************************************************/
void EVM_init(void)
{
	Int32 board_status;
	Board_IDInfo sBoardInfo;

	int i, j;

    /*
     * You can choose what to initialize on the platform by setting the following
     * flags. We will initialize everything.
     */
#if (defined(DEVICE_C6678)||defined(DEVICE_C6657))
    board_status = Board_init(BOARD_INIT_ETH_PHY|BOARD_INIT_ECC);
#else
    board_status = Board_init(BOARD_INIT_UART_STDIO|BOARD_INIT_ETH_PHY|BOARD_INIT_ECC);
#endif

    /* If we initialized the board okay */
    if (board_status == BOARD_SOK) {
        /* Get information about the platform so we can use it in various places */
        memset( (void *) &sBoardInfo, 0, sizeof(Board_IDInfo));
        ///platform_get_info(&sPlatformInfo);
        //////board_status = Board_getIDInfo(&sBoardInfo);
        ///number_of_cores = sPlatformInfo.cpu.core_count;
#if (defined(DEVICE_C6678)||defined(DEVICE_K2H)||defined(DEVICE_K2K))
        number_of_cores = 8;
#endif

#if (defined(DEVICE_C6657))
        number_of_cores = 2;
#endif

        MultiProc_setLocalId((Uint16) CSL_chipReadDNUM());

        /* Create our host name: Its board name + last 6 digits of the serial number.
         * Since the serial number is in I2C it can be altered or even not there so
         * we have to take into account that it may not be what we expect.
         */
        strcpy (HostName, "tidemo-");
        ///i = strlen(HostName);
        ///j = strlen(sBoardInfo.serialNum);

        ///if (j > 0) {
        ///    if (j > 6) {
        ///        memcpy (&HostName[i], &sBoardInfo.serialNum[j-6], 6);
        ///        HostName[i+7] = '\0';
        ///    } else {
        ///        memcpy (&HostName[i], sBoardInfo.serialNum, j);
        ///        HostName[i+j+1] = '\0';
        ///    }
        ///}
    } else {
        /* Initialization of the platform failed... die */
        platform_write("Board failed to initialize. Error code %d \n", board_status);
        platform_write("We will die in an infinite loop... \n");
        while (1) {
            ///(void) platform_led(1, PLATFORM_LED_ON, PLATFORM_SYSTEM_LED_CLASS);
            ///(void) platform_delay(50000);
            ///(void) platform_led(1, PLATFORM_LED_OFF, PLATFORM_SYSTEM_LED_CLASS);
            ///(void) platform_delay(50000);
        };
    }

    return;
}

/*************************************************************************
 *  @b NetworkOpen()
 *
 *  @n
 *
 *  This function is called after the Network stack has started..
 *
 *  @param[in]  None
 *
 *  @retval
 *      None
 ************************************************************************/
static void NetworkOpen()
{
    return;
}

/*************************************************************************
 *  @b NetworkClose()
 *
 *  @n
 *
 *  This function is called when the network is shutting down,
 *     or when it no longer has any IP addresses assigned to it.
 *
 *  @param[in]  None
 *
 *  @retval
 *      None
 ************************************************************************/
static void NetworkClose()
{
    return;
}

/*************************************************************************
 *  @b NetworkIPAddr( IPN IPAddr, uint32_t IfIdx, uint32_t fAdd )
 *
 *  @n
 *
 *  This function is called whenever an IP address binding is
 *  added or removed from the system.
 *
 *  @param[in]
 *     IPAddr - The IP address we are adding or removing.
 *
 *  @param[in]
 *     IfIdx - Interface index (number). Used for multicast.
 *
 *  @param[in]
 *     fAdd -  True if we added the interface, false if its being removed.
 *
 *  @retval
 *      None
 ************************************************************************/

static void NetworkIPAddr( IPN IPAddr, uint32_t IfIdx, uint32_t fAdd )
{
    static uint32_t fAddGroups = 0;
    IPN IPTmp;

    if( fAdd )
        platform_write("Network Added: ");
    else
        platform_write("Network Removed: ");

    /* Print a message */
    IPTmp = NDK_ntohl( IPAddr );
    platform_write("If-%d:%d.%d.%d.%d \n", IfIdx,
            (uint8_t)(IPTmp>>24)&0xFF, (uint8_t)(IPTmp>>16)&0xFF,
            (uint8_t)(IPTmp>>8)&0xFF, (uint8_t)IPTmp&0xFF );


    /* This is a good time to join any multicast group we require */
    if( fAdd && !fAddGroups )
    {
        fAddGroups = 1;
        /*      IGMPJoinHostGroup( inet_addr("224.1.2.3"), IfIdx ); */
    }

    return;
}

/*************************************************************************
 *  @b DHCP_reset( uint32_t IfIdx, uint32_t fOwnTask )
 *
 *  @n
 *
 *  This function is called whenever an IP address binding is
 *  added or removed from the system.
 *
 *  @param[in]
 *     IfIdx - Interface index (number) that is using DHCP.
 *
 *  @param[in]
 *     fOwnTask -  Set when called on a new task thread (via TaskCreate()).
 *
 *  @retval
 *      None
 ************************************************************************/
void DHCP_reset( uint32_t IfIdx, uint32_t fOwnTask )
{
    CI_SERVICE_DHCPC dhcpc;
    void* h;
    int    rc,tmp;
    uint32_t   idx;

    /* If we were called from a newly created task thread, allow
       the entity that created us to complete */
    if( fOwnTask ) {
        TaskSleep(500);
    }

    /* Find DHCP on the supplied interface */
    for(idx=1; ; idx++)
    {
        /* Find a DHCP entry */
        rc = CfgGetEntry( 0, CFGTAG_SERVICE, CFGITEM_SERVICE_DHCPCLIENT,
                idx, &h );
        if( rc != 1 )
            goto RESET_EXIT;

        /* Get DHCP entry data */
        tmp = sizeof(dhcpc);
        rc = CfgEntryGetData( h, &tmp, (uint8_t *)&dhcpc );

        /* If not the right entry, continue */
        if( (rc<=0) || dhcpc.cisargs.IfIdx != IfIdx )
        {
            CfgEntryDeRef(h);
            h = 0;
            continue;
        }

        /* This is the entry we want! */

        /* Remove the current DHCP service */
        CfgRemoveEntry( 0, h );

        /* Specify DHCP Service on specified IF */
        bzero( &dhcpc, sizeof(dhcpc) );
        dhcpc.cisargs.Mode   = CIS_FLG_IFIDXVALID;
        dhcpc.cisargs.IfIdx  = IfIdx;
        dhcpc.cisargs.pCbSrv = &ServiceReport;
        CfgAddEntry( 0, CFGTAG_SERVICE, CFGITEM_SERVICE_DHCPCLIENT, 0,
                sizeof(dhcpc), (uint8_t *)&dhcpc, 0 );
        break;
    }

RESET_EXIT:
    /* If we are a function, return, otherwise, call TaskExit() */
    if( fOwnTask )
        TaskExit();

    return;
}


/*************************************************************************
 *  @b ServiceReport( uint32_t Item, uint32_t Status, uint32_t Report, void* h )
 *
 *  @n
 *
 *  Here's a quick example of using service status updates from the IP
 *  Stack. Lets store the states of the services so we can refrence them
 *  elsehwere (e.g. the information Page).
 *  The defines for the services are in the NDK header file netcfg.h
 *
 *  @param[in]
 *     Item - The service that is reporting (ie Telnet, HTTP, DHCP, etc).
 *
 *  @param[in]
 *     Status - Overall status of that service.
 *
 *  @param[in]
 *     Report - What its reporting.
 *
 *  @param[in]
 *     Handle - Handle to  the Service.
 *
 *  @retval
 *      None
 ************************************************************************/
void CheckDHCPOptions();

/*
 *  Defines for dealing with IP services so we can report on the state of them.
 * See netcfg.h in the NDK and callback in hpdspua.c.
 */
typedef struct _service_state {
    char name[10];
    uint32_t report;
    uint32_t status;
}Service_state_s;

/* These arrays are order dependent based on defines in the NDK header files */
char *ReportStr[] = { "","Running","Updated","Complete","Fault" };
char *StatusStr[] = { "Disabled","Waiting","IPTerm","Failed","Enabled" };

Service_state_s ServiceStatus [CFGITEM_SERVICE_MAX] = {
    {"Telnet", 0, 0},
    {"HTTP", 0, 0},
    {"NAT", 0, 0},
    {"DHCPS", 0, 0},
    {"DHCPC", 0, 0},
    {"DNS", 0, 0}
};

static void ServiceReport( uint32_t Item, uint32_t Status, uint32_t Report, void* h )
{

    /* Save off the status */
    ServiceStatus[Item-1].status = Status;
    ServiceStatus[Item-1].report = Report;

    platform_write( "Service Status: %-9s: %-9s: %-9s: %03d\n",
            ServiceStatus[Item-1].name, StatusStr[ServiceStatus[Item-1].status],
            ReportStr[ServiceStatus[Item-1].report/256], Report&0xFF );

    /*
    // Example of adding to the DHCP configuration space
    //
    // When using the DHCP client, the client has full control over access
    // to the first 256 entries in the CFGTAG_SYSINFO space.
    //
    // Note that the DHCP client will erase all CFGTAG_SYSINFO tags except
    // CFGITEM_DHCP_HOSTNAME. If the application needs to keep manual
    // entries in the DHCP tag range, then the code to maintain them should
    // be placed here.
    //
    // Here, we want to manually add a DNS server to the configuration, but
    // we can only do it once DHCP has finished its programming.
    */
    if( Item == CFGITEM_SERVICE_DHCPCLIENT &&
            Status == CIS_SRV_STATUS_ENABLED &&
            (Report == (NETTOOLS_STAT_RUNNING|DHCPCODE_IPADD) ||
             Report == (NETTOOLS_STAT_RUNNING|DHCPCODE_IPRENEW)) )
    {
        IPN IPTmp;

        /* Manually add the DNS server when specified */
        IPTmp = inet_addr(DNSServer);
        if( IPTmp )
            CfgAddEntry( 0, CFGTAG_SYSINFO, CFGITEM_DHCP_DOMAINNAMESERVER,
                    0, sizeof(IPTmp), (uint8_t *)&IPTmp, 0 );
#if 0
        /* We can now check on what the DHCP server supplied in
           response to our DHCP option tags. */
        CheckDHCPOptions();
#endif

    }

    /* Reset DHCP client service on failure */
    if( Item==CFGITEM_SERVICE_DHCPCLIENT && (Report&~0xFF)==NETTOOLS_STAT_FAULT )
    {
        CI_SERVICE_DHCPC dhcpc;
        int tmp;

        /* Get DHCP entry data (for index to pass to DHCP_reset). */
        tmp = sizeof(dhcpc);
        CfgEntryGetData( h, &tmp, (uint8_t *)&dhcpc );

        /* Create the task to reset DHCP on its designated IF
           We must use TaskCreate instead of just calling the function as
           we are in a callback function. */
        TaskCreate( DHCP_reset, "DHCPreset", OS_TASKPRINORM, 0x1000,
                dhcpc.cisargs.IfIdx, 1, 0 );
    }

    return;
}

/*************************************************************************
 *  @b CheckDHCPOptions()
 *
 *  @n
 *
 *  Checks the DHCP Options and configures them.
 *
 *  @param[in]
 *     None
 *
 *  @retval
 *      None
 ************************************************************************/
void CheckDHCPOptions()
{
    char IPString[16];
    IPN  IPAddr;
    int  i, rc;

    /*
     *  Now scan for DHCPOPT_SERVER_IDENTIFIER via configuration
     */
    platform_write("\nDHCP Server ID:\n");
    for(i=1;;i++)
    {
        /* Try and get a DNS server */
        rc = CfgGetImmediate( 0, CFGTAG_SYSINFO, DHCPOPT_SERVER_IDENTIFIER,
                i, 4, (uint8_t *)&IPAddr );
        if( rc != 4 )
            break;

        /* We got something */

        /* Convert IP to a string */
        NtIPN2Str( IPAddr, IPString );
        platform_write("DHCP Server %d = '%s'\n", i, IPString);
    }
    if( i==1 )
        platform_write("None\n\n");
    else
        platform_write("\n");

    /*  Now scan for DHCPOPT_ROUTER via the configuration */
    platform_write("Router Information:\n");
    for(i=1;;i++)
    {
        /* Try and get a DNS server */
        rc = CfgGetImmediate( 0, CFGTAG_SYSINFO, DHCPOPT_ROUTER,
                i, 4, (uint8_t *)&IPAddr );
        if( rc != 4 )
            break;

        /* We got something */

        /* Convert IP to a string */
        NtIPN2Str( IPAddr, IPString );
        platform_write("Router %d = '%s'\n", i, IPString);
    }
    if( i==1 )
        platform_write("None\n\n");
    else
        platform_write("\n");

    return;
}
