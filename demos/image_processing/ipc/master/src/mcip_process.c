/*
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Types.h>
#include <xdc/runtime/Timestamp.h>
#include <ti/sysbios/hal/Cache.h>

#include <xdc/runtime/Log.h>
#include <ti/uia/events/UIABenchmark.h>

#include "demos/image_processing/ipc/master/inc/mcip_process.h"

/* w should be power of 2 */
#define ROUNDUP(n,w) (((n) + (w) - 1) & ~((w) - 1))
#define MAX_CACHE_LINE (128)

#define MAX_SLICES (10) /* This should be more than # of cores in device */
#define DEFAULT_SLICE_OVERLAP_SIZE 2

#define MASTER_QUEUE_NAME "master_queue"
static char slave_queue_name[MAX_SLICES][16];

static process_message_t ** p_queue_msg = 0;
static int max_core = 0;

int mc_process_init (int number_of_cores)
{
    int i;

    p_queue_msg = (process_message_t **) calloc(number_of_cores, sizeof(process_message_t *));
    if (!p_queue_msg) {
        printf("alloc_queue_message: Can't allocate memory for queue message\n");
        return -1;
    }

    for (i = 0; i < number_of_cores; i++) {
        p_queue_msg[i] =  (process_message_t *) MessageQ_alloc(IMAGE_PROCESSING_HEAPID, sizeof(process_message_t));
        if (!p_queue_msg[i]) {
            printf("alloc_queue_message: Can't allocate memory for queue message %d\n", i);
            return -1;            
        }
        memset(p_queue_msg[i]->info.scratch_buf_len, 0, NUMBER_OF_SCRATCH_BUF * sizeof(uint32_t));
        memset(p_queue_msg[i]->info.scratch_buf, 0, NUMBER_OF_SCRATCH_BUF * sizeof(uint8_t *));
    }

    max_core = number_of_cores;

    memset(slave_queue_name, 0, MAX_SLICES*16);
    for (i = 0; i < MAX_SLICES; i++) {
        GET_SLAVE_QUEUE_NAME(slave_queue_name[i], i);
    }
    return 0;
}

int mc_process_bmp (processing_type_e processing_type, 
                    raw_image_data_t * p_input_image, raw_image_data_t * p_output_image,
                    int number_of_cores, double * processing_time)
{
	bmp_color_table_t * p_color_table = 0;
	bmp_header_t bmp_header;
	uint8_t * pixel_array_rgb = 0;
	uint8_t * edge[MAX_SLICES] = {0};
	uint8_t * pixel_array_edge = 0;
	uint8_t * rgb[MAX_SLICES] = {0};
	uint32_t height[MAX_SLICES];
    int edge_size[MAX_SLICES];
    int color_table_size, pixel_array_rgb_size, pixel_array_edge_size;
	int pixel_size, row_width, slice_height, guard_height;
	int i, j, ret_val = 0;
    process_message_t * p_msg = 0;
    MessageQ_Handle h_receive_queue = 0;
    MessageQ_QueueId queue_id[MAX_SLICES];
    uint16_t msgId = 0;
    Types_FreqHz freq;
    Int32 ts1, ts2;

    Log_write1(UIABenchmark_start, (xdc_IArg)"BMP_Proc_Prologue");

    Timestamp_getFreq(&freq);

    if ((number_of_cores <= 0) || (number_of_cores > max_core)) {
		printf("Invalid number_of_cores: It should be between 1 to %d\n", max_core);
		ret_val = -1;
		goto close_n_exit;        
    }

	if ((p_input_image == 0) || (p_input_image->length == 0) || (p_input_image->data == 0)) {
		printf("Invalid BMP image data\n");
		ret_val = -1;
		goto close_n_exit;
	}

	if (bmp_read_header(p_input_image, &bmp_header) < 0) {
		printf("Error in reading header\n");
		ret_val = -1;
		goto close_n_exit;
	}

	pixel_size = bmp_header.dib.bits_per_pixel / 8;
	row_width  = bmp_header.dib.image_width * pixel_size;

	if (bmp_header.dib.number_of_colors) {
		/* Color table present */
        color_table_size = ROUNDUP(sizeof(bmp_color_table_t) * bmp_header.dib.number_of_colors, MAX_CACHE_LINE);
		p_color_table = (bmp_color_table_t *)Memory_alloc(0, color_table_size, MAX_CACHE_LINE, NULL);
		if(!p_color_table) {
			printf("Can't allocate memory for color table\n");
			ret_val = -1;
			goto close_n_exit;
		}
		if (bmp_read_colormap(p_input_image, &bmp_header, p_color_table) < 0) {
			printf("Error in reading color map\n");
			ret_val = -1;
			goto close_n_exit;
		}
        Cache_wb(p_color_table, color_table_size, Cache_Type_ALL, FALSE);
	}

	/* Read the pixels */
    pixel_array_rgb_size = ROUNDUP(bmp_header.dib.image_height * row_width, MAX_CACHE_LINE);
	pixel_array_rgb = (uint8_t *) Memory_alloc(0, pixel_array_rgb_size, MAX_CACHE_LINE, NULL);
	if (!pixel_array_rgb) {
		printf("Can't allocate memory for pixel_array_rgb\n");
		ret_val = -1;
		goto close_n_exit;
	}
	if (bmp_read_image (p_input_image, &bmp_header, pixel_array_rgb) < 0) {
		printf("Error in reading pixel image\n");
		ret_val = -1;
		goto close_n_exit;
	}

    Cache_wb(pixel_array_rgb, (bmp_header.dib.image_height * row_width), Cache_Type_ALL, FALSE);
    
    /* Create the local message queue */
    h_receive_queue = MessageQ_create(MASTER_QUEUE_NAME, NULL);
    if (h_receive_queue == NULL) {
        printf("MessageQ_create failed\n" );
		ret_val = -1;
		goto close_n_exit;
    }

    for (j = 0; j < number_of_cores; j++) {
        do {
            i = MessageQ_open(slave_queue_name[j], &queue_id[j]);
        } while (i < 0);
    }

    if (number_of_cores > 1) {
        guard_height = DEFAULT_SLICE_OVERLAP_SIZE;
    } else {
        guard_height = 0;
    }

	/* Split the image into multiple (= number_of_cores) sections */
	slice_height = bmp_header.dib.image_height / number_of_cores;
	rgb[0] = &(pixel_array_rgb[0]);
	height[0] = slice_height + guard_height;
	for (i = 1; i < number_of_cores; i++) {
		rgb[i] = pixel_array_rgb + ((i * slice_height * row_width) - (row_width * guard_height));
		height[i] = slice_height + (2 * guard_height);
	}
	height[number_of_cores - 1] = slice_height + guard_height;

    for (i = 0; i < number_of_cores; i++) {
        edge_size[i] = ROUNDUP((height[i] * row_width), MAX_CACHE_LINE);
        edge[i] = (uint8_t *) Memory_alloc(0, edge_size[i], MAX_CACHE_LINE, NULL);
        if (!edge[i]) {
            printf("mc_process_bmp: Memory_alloc failed for edge[%d]\n", i);
    		ret_val = -1;
    		goto close_n_exit;
        }

        /* Allocate scratch buffers for slave processors */
        p_queue_msg[i]->info.scratch_buf[0] = 
            (uint8_t *) Memory_alloc(0, ROUNDUP(bmp_header.dib.image_width * height[i], MAX_CACHE_LINE), MAX_CACHE_LINE, NULL);
        if(!p_queue_msg[i]->info.scratch_buf[0]) {
            printf("mc_process_bmp: Memory_alloc failed for scratch_buf[%d][0]\n", i);
    		ret_val = -1;
    		goto close_n_exit;
        }
        p_queue_msg[i]->info.scratch_buf_len[0] = ROUNDUP(bmp_header.dib.image_width * height[i], MAX_CACHE_LINE);

        if ((p_color_table) || (bmp_header.dib.bits_per_pixel != 8)) {
            p_queue_msg[i]->info.scratch_buf[1] = 
                (uint8_t *) Memory_alloc(0, ROUNDUP(bmp_header.dib.image_width * height[i], MAX_CACHE_LINE), MAX_CACHE_LINE, NULL);
            if(!p_queue_msg[i]->info.scratch_buf[1]) {
                printf("mc_process_bmp: Memory_alloc failed for scratch_buf[%d][1]\n", i);
        		ret_val = -1;
        		goto close_n_exit;
            }
            p_queue_msg[i]->info.scratch_buf_len[1] = ROUNDUP(bmp_header.dib.image_width * height[i], MAX_CACHE_LINE);
        }
    }
     Log_write1(UIABenchmark_stop, (xdc_IArg)"BMP_Proc_Prologue");
    ts1 = (Int32) Timestamp_get32();
    /* Send messages to process the slices */
	for (i = number_of_cores-1; i >= 0; i-- ) {
        p_msg = p_queue_msg[i];
        p_msg->core_id = i;

        p_msg->info.processing_type = edge_detection;
        p_msg->info.bitspp          = bmp_header.dib.bits_per_pixel;
        p_msg->info.p_color_table   = (color_table_t*) p_color_table;
        p_msg->info.width           = bmp_header.dib.image_width;
        p_msg->info.height          = height[i];
        p_msg->info.rgb_in          = rgb[i];
        p_msg->info.out             = edge[i];
        p_msg->info.flag            = 0;

        MessageQ_setMsgId(p_msg, ++msgId);
        MessageQ_setReplyQueue(h_receive_queue, (MessageQ_Msg)p_msg);

        /* send the message to the remote processor */
        if (MessageQ_put(queue_id[i], (MessageQ_Msg)p_msg) < 0) {
            printf("MessageQ_put had a failure error\n");
    		ret_val = -1;
    		goto close_n_exit;
        }

	}
    
    /* Receive the result */
    for (i = 0; i < number_of_cores; i++) {

        if (MessageQ_get(h_receive_queue, (MessageQ_Msg *)&p_msg, MessageQ_FOREVER) < 0) {
            printf("This should not happen since timeout is forever\n");
    		ret_val = -1;
        } else if (p_msg->info.flag != 0) {
            printf("Process image error received from core %d\n", i);
    		ret_val = -1;
        }
    }
    ts2 = (Int32) Timestamp_get32();
    ts2 = ts2 - ts1;
    *processing_time = ((double)ts2 / (double)freq.lo) * 1000;

     Log_write1(UIABenchmark_start, (xdc_IArg)"BMP_Proc_Epilogue");

    if (ret_val == -1) {
    		goto close_n_exit;
    }
	/* Merge all outputs */
    pixel_array_edge_size = bmp_header.dib.image_width * bmp_header.dib.image_height;
	pixel_array_edge = (uint8_t *) Memory_alloc(0, pixel_array_edge_size, 0, NULL);
	if (!pixel_array_edge) {
		printf("Can't allocate memory for pixel_array_edge\n");
		ret_val = -1;
		goto close_n_exit;
	}
	memcpy(&(pixel_array_edge[0]), &(edge[0][0]), slice_height * bmp_header.dib.image_width);

    if (number_of_cores > 1) {
    	for (i = 1; i < number_of_cores; i++) {
            Cache_inv(edge[i], (p_msg->info.height * p_msg->info.width), 
                    Cache_Type_ALL, FALSE);
    		memcpy(pixel_array_edge + (i * slice_height * bmp_header.dib.image_width), 
                edge[i] + (bmp_header.dib.image_width * guard_height), slice_height * bmp_header.dib.image_width);
    	}
    }

    p_output_image->length = bmp_get_gray_bmpfile_size(bmp_header.dib.image_width, bmp_header.dib.image_height);
    p_output_image->data   = (uint8_t *) Memory_alloc(0, p_output_image->length, 0, NULL);
    if (!p_output_image->data) {
        p_output_image->length = 0;
		printf("Can't allocate memory for output bmp image\n");
		ret_val = -1;
		goto close_n_exit;
    }
	/* Create (Gray Scale) Image */
	if (bmp_write_gray_bmpfile (p_output_image, pixel_array_edge,
			                    bmp_header.dib.image_width, bmp_header.dib.image_height) < 0) {
		printf("Error in bmp_write_gray_bmpfile\n");
		ret_val = -1;
		goto close_n_exit;
	}

	ret_val = 0;
	
close_n_exit:
	if(p_color_table) Memory_free(0, p_color_table, color_table_size);
    if(pixel_array_rgb) Memory_free(0, pixel_array_rgb, pixel_array_rgb_size);
    for (i = 0; i < number_of_cores; i++) {
    	if(edge[i]) Memory_free(0, edge[i], edge_size[i]);
        for (j = 0; j < NUMBER_OF_SCRATCH_BUF; j++) {
            if(p_queue_msg[i]->info.scratch_buf_len[j]) {
                Memory_free(0, p_queue_msg[i]->info.scratch_buf[j], p_queue_msg[i]->info.scratch_buf_len[j]);
                p_queue_msg[i]->info.scratch_buf_len[j] = 0;
            }
        }
    }
    if(pixel_array_edge) Memory_free(0, pixel_array_edge, pixel_array_edge_size);
    for (i = 0; i < number_of_cores; i++) {
    	if(queue_id[i]) MessageQ_close(&queue_id[i]);
    }

    Log_write1(UIABenchmark_stop, (xdc_IArg)"BMP_Proc_Epilogue");

    if(h_receive_queue) MessageQ_delete(&h_receive_queue);
    
    return ret_val;
}

