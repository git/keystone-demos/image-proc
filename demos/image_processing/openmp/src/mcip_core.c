/*
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ti/sysbios/BIOS.h> 
#include <ti/sysbios/hal/Cache.h>

#include "ti/platform/platform.h"
#include "../inc/mcip_core.h"
#include "imglib.h"

/* This value is used as threshold on the Sobel output to get edge data. */
#define IMAGE_THRESHOLD_VALUE (0xfe)

/*********************************************************************************/
/* RGB to Y image convert routine                                                */
/* The conversion method used is Y = 0.299R + 0.587G + 0.114B                    */
/* (ITU-R Recommendation BT. 601-4)                                              */           
/*********************************************************************************/
static int convert_rgb_to_y (uint16_t bits_per_pixel,
		                  color_table_t * p_color_table,
						  uint8_t * pixel_array_rgb,
						  uint8_t * pixel_array_y,
						  uint32_t width, uint32_t height)
{
	int i;
	int pixel_size = bits_per_pixel / 8;

	if(pixel_size == 1) {
        if (p_color_table) {
    		for(i = 0; i < (width * height); i++) {
    			pixel_array_y[i] = (uint8_t)(((double)p_color_table[pixel_array_rgb[i]].blue * 0.114)
    									   + ((double)p_color_table[pixel_array_rgb[i]].green * 0.587)
    									   + ((double)p_color_table[pixel_array_rgb[i]].red * 0.299));
    		}
        } else {
    		printf("BPP 8 must have color table\n");
    		return -1;
        }
	} else if(pixel_size == 3) {
		for(i = 0; i < (width * height); i++) {
			pixel_array_y[i] = (uint8_t)(((double)pixel_array_rgb[3 * i] * 0.114)       /* Blue */
					                   + ((double)pixel_array_rgb[3 * i + 1] * 0.587)   /* Green */
					                   + ((double)pixel_array_rgb[3 * i + 2] * 0.299)); /* Red */
		}
	} else {
		printf("BPP %d not supported\n", bits_per_pixel);
		return -1;
	}

	return 0;
}

/*********************************************************************************/
/* Process RGB image                                                             */
/*********************************************************************************/
void process_rgb (processing_info_t * p_info)
{
	uint8_t * y = 0;
	uint8_t * sobel = 0;

    if (platform_get_coreid() != 0) {
        Cache_inv(p_info->rgb_in,
            (p_info->height * p_info->width * (p_info->bitspp / 8)), Cache_Type_ALL, FALSE);
        if (p_info->p_color_table) {
            Cache_inv(p_info->p_color_table, (sizeof(color_table_t) * 256), Cache_Type_ALL, FALSE);
        }
    }

    p_info->flag = -1;

    if(p_info->processing_type != edge_detection) {
        printf("Unsupported processing type %d\n", p_info->processing_type);
		goto close_n_exit;
    }

    if ((p_info->bitspp == 8) && (!p_info->p_color_table)) {
        y = p_info->rgb_in;
    } else {
    	if ((!p_info->scratch_buf_len[1]) || 
            ((p_info->width * p_info->height) > p_info->scratch_buf_len[1])) {
    		printf("process_rgb: Scratch buffer for Y not available: 0x%x\n",
                p_info->scratch_buf_len[1]);
    		goto close_n_exit;
    	}

        y = p_info->scratch_buf[1];
    	if (convert_rgb_to_y (p_info->bitspp, p_info->p_color_table,
    							  p_info->rgb_in, y,
    							  p_info->width, p_info->height) < 0) {
    		printf("Error in converting RGB to Y\n");
    		goto close_n_exit;
    	}
    }

	/* Run Sobel Filter */
	if ((!p_info->scratch_buf_len[0]) || 
            ((p_info->width * p_info->height) > p_info->scratch_buf_len[0])) {
		printf("Can't allocate memory for sobel\n");
		goto close_n_exit;
	}
    sobel = p_info->scratch_buf[0];
	IMG_sobel_3x3_8(y, sobel,
			p_info->width, p_info->height);

	/* Run Threshold Function */
	IMG_thr_le2min_8(sobel, p_info->out,
				p_info->width, p_info->height, IMAGE_THRESHOLD_VALUE);

    p_info->flag = 0;

    if (platform_get_coreid() != 0) {
        Cache_wb(p_info->out, (p_info->height * p_info->width), Cache_Type_ALL, FALSE);
    }

    if (platform_get_coreid() != 0) {
        Cache_inv(p_info->scratch_buf[0], p_info->scratch_buf_len[0], Cache_Type_ALL, FALSE);
        if (p_info->scratch_buf[1]) {
            Cache_inv(p_info->scratch_buf[1], p_info->scratch_buf_len[1], Cache_Type_ALL, FALSE); 
        }
        Cache_inv(p_info, sizeof(processing_info_t), Cache_Type_ALL, FALSE);
    }

close_n_exit:
    return;
}

