/*
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "../inc/mcip_process.h"

/*#define USER_INPUT_FILE*/

char image_name[250] = "../images/evmc6678l_689x306_618KB.bmp";
char out_image_name[250] = "../images/evmc6678l_689x306_618KB_edge.bmp";

int main(void)
{
	FILE * fpr = 0;
	FILE * fpro = 0;
	raw_image_data_t  raw_image = {0, 0};
    raw_image_data_t  out_raw_image = {0, 0};
	uint32_t read_length = 0, write_length = 0;
    double processing_time = 0;
    int ret_val = 0;

#ifdef USER_INPUT_FILE
    printf("Enter input image name: ");
    gets(image_name);
    printf("Enter output image name: ");
    gets(out_image_name);
#endif

	fpr = fopen(image_name, "rb");
	if(!fpr) {
		printf("Unable to open image file %s\n", image_name);
		goto close_n_exit;
	}

	fseek(fpr, 0, SEEK_END);
	raw_image.length = ftell(fpr);
	fseek(fpr, 0, SEEK_SET);

	raw_image.data = malloc(raw_image.length);
	if(!raw_image.data) {
		printf("Unable allocate buffer for raw image file read (%s)\n", image_name);
		goto close_n_exit;
	}

	printf("Processing start: Input image file name: %s, size: %d\n", image_name, raw_image.length);

	do {
		ret_val = fread(raw_image.data + read_length, 1, raw_image.length - read_length, fpr);
		if (!ret_val) {
			printf("Unable read the raw image file %s\n", image_name);
			goto close_n_exit;
		}
		read_length += ret_val;
	} while (read_length < raw_image.length);

    mc_process_bmp(edge_detection, &raw_image, &out_raw_image, 8, &processing_time);

    /* FILE write */
	fpro = fopen(out_image_name, "wb");
	if(!fpro) {
		printf("Unable to open image file %s\n", out_image_name);
		goto close_n_exit;
	}

	do {
		ret_val = fwrite(out_raw_image.data + write_length, 1, out_raw_image.length - write_length, fpro);
		if(!ret_val) {
			printf("Unable write image to the file %s\n", out_image_name);
			goto close_n_exit;
		}
		write_length += ret_val;
	} while (write_length < out_raw_image.length);

	printf("Processing end:   Output image file name: %s, size: %d\n", out_image_name, out_raw_image.length);

close_n_exit:
    if(fpr) fclose(fpr);
    if(fpro) fclose(fpro);
    if(raw_image.data) free(raw_image.data);
    if(out_raw_image.data) free(out_raw_image.data);
    return 1;
}

