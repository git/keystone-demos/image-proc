
if [ -z $CGT_INSTALL_DIR ]; then
    export CGT_INSTALL_DIR=~/ti/TI_CGT_C6000_7.4.0
fi

export C_DIR=$CGT_INSTALL_DIR
export PATH=$CGT_INSTALL_DIR/bin:$PATH

rm -rf tmp

python ../../../../../tools/boot_loader/mad-utils/map-tool/maptool.py config-files/maptoolCfg_evmc6678l_bypass_prelink_omp.json bypass-prelink

