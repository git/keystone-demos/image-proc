/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef BMP_UTILS_H
#define BMP_UTILS_H

#include <stdint.h>

/*
 * PACK Macros: Pack bytes into 16 and 32 bit values. Used
 * to solve alignment issues. byte0 is LSB byteX is MSB.
*/
#define PACK_32(ptr) (((uint32_t)(uint8_t)(*ptr+0) << 0x18) | \
                      ((uint32_t)(uint8_t)(*ptr+1) << 0x10) | \
                      ((uint32_t)(uint8_t)(*ptr+2) << 0x08) | \
                      ((uint32_t)(uint8_t)(*ptr+3) << 0x00))

#define PACK_16(ptr) (((uint16_t)(uint8_t)(*ptr+0) << 0x08) | \
                      ((uint16_t)(uint8_t)(*ptr+1) << 0x00))

/****************************************************************************/
/* Bitmap header structure                                                  */
/****************************************************************************/
#ifdef _HOST_BUILD
#pragma pack(1)
#endif

typedef struct bmpfile_signature {
  uint8_t signature[2];      /* Signature - 'BM' */
} bmpfile_signature_t;

typedef struct bmpfile_header {
  uint32_t file_size;       /* File size in bytes */
  uint16_t reserved1;
  uint16_t reserved2;
  uint32_t bitmap_offset;   /* Offset to bitmap */
} bmpfile_header_t;

typedef struct bmpfile_dib_header {
  uint32_t header_size;    /* Size of this struct */
  int32_t  image_width;        /* Image width in pixels */
  int32_t  image_height;       /* Image image_height in pixels */
  uint16_t number_of_planes;      /* Number of planes */
  uint16_t bits_per_pixel;       /* Bits per pixel */
  uint32_t compression_type;/* Compression flag */
  uint32_t image_size;   /* Image size in bytes */
  int32_t  horizontal_resolution;         /* Horizontal resolution */
  int32_t  vertical_resolution;         /* Vertical resolution */
  uint32_t number_of_colors;      /* Color map size */
  uint32_t important_color_count;   /* Important color count */
} bmpfile_dib_header_t;

typedef struct bmp_header {
	bmpfile_signature_t  signature;
	bmpfile_header_t     file;
	bmpfile_dib_header_t dib;
} bmp_header_t;

/****************************************************************************/
/* Bitmap RGB colormap entry structure                                      */
/****************************************************************************/
typedef struct {
  uint8_t red;
  uint8_t green;
  uint8_t blue;
  uint8_t reserved;
} bmp_color_table_t;

#ifdef _HOST_BUILD
#pragma pack()
#endif

typedef enum {
  BMP_RGB = 0,
  BMP_RLE8,
  BMP_RLE4,
  BMP_BITFIELDS,
  BMP_JPEG,
  BMP_PNG
} bmp_compression_method_e;

/* Raw image data
 */

typedef struct raw_image_data {
	uint8_t * data;
	uint32_t  length;
} raw_image_data_t;

/* This function reads the header information from a bitmap file.
 * Note: It also does some preliminary checking if the file is valid.
 * It would return 0 for success and < 0 if file read or check fails.
 */

extern int bmp_read_header (raw_image_data_t * image, bmp_header_t * hdr);

/* This function reads the color table from the file.
 * It should be called if info_header.ncolors > 0.
 * The color_table structure should be an array of bmp_color_table_t size
 * length hdr->dib.ncolors.
 */
extern int bmp_read_colormap (raw_image_data_t * image, bmp_header_t * hdr,
									bmp_color_table_t * color_table);

/* This function reads the image (or pixel value). It expects the pixel_array_rgb should
 * be of size [hdr->dib.height * (hdr->dib.width * hdr->dib.bitspp / 8)], i.e., image size
 * in bytes.
 */
extern int bmp_read_image (raw_image_data_t * image, bmp_header_t * hdr, uint8_t * pixel_array_rgb);

/* This function creates and writes a gray scale image to system.
 * It expects the pixel_array should be of size
 * [hdr->dib.height * (hdr->dib.width * hdr->dib.bitspp / 8)],
 * i.e., image size in bytes.
 */
extern int bmp_write_gray_bmpfile (raw_image_data_t * image, uint8_t * pixel_array,
		                           uint32_t width, uint32_t height);

/*
 * Get a gray scale image file file size
 */
extern uint32_t bmp_get_gray_bmpfile_size (uint32_t width, uint32_t height);

#endif /*BMP_UTILS_H*/
