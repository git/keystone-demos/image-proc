/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef WEBPAGE_UTILS_H
#define WEBPAGE_UTILS_H

#define HTML_FATAL         -1   /* Internal error of some sort.*/
#define HTML_TOO_LARGE     -2   /* If the Post exceeds our MAX_POST_FILESZ*/
#define HTML_PARSER_ERROR  -3   /* HTML Parser error. On mime pages it could mean no file was specified.*/
#define HTML_RECEIVE_ERROR -4   /* rcv call returns error.*/

#define MAX_POST_FILESZ    0x600000 /* Maximum size of a file upload (including fields and boundry markers)! */
#define MAX_OUTIMAGE_SZ    0x200000 /* Output image size */

/*************************************************************************
 *  @b  cgiParseMulti(SOCKET htmlSock, int ContentLength, char *pBuf )
 * 
 *  @n
 *  
 *  This is our routine to process a multi-part MIME post (the kind 
 *  you use when posting a file). Ideally we would have one parser for 
 *  all types of POSTs but they can quickly become rather complicated to 
 *  implement so its eaiser to have this one and html_processPost. 
 * 
 * 
 *  @retval
 *   HTML_FATAL         Internal error of some sort.
 *   HTML_TOO_LARGE     If the Posted file exceeds our MAX_POST_FILESZ
 *   HTML_PARSER_ERROR  HTML Parser error
 *   length             The size of the file that was posted.
 *
 ***************************************************************************/
int cgiParseMulti(int ContentLength, unsigned char *pBuf);

/*************************************************************************
 *  @b  html_getValueFor(char *name)
 * 
 *  @n
 *  
 *     Returns the value associated with a named field from a POST.
 * 
 *  @param[in]  
 *  name    - The name of the field you want the value for
 * 
 * 
 *  @retval
 *  Pointer to the value or NULL if nameis not found
 ************************************************************************/
char *html_getValueFor(char *name);

#endif /* WEBPAGE_UTILS_H*/
