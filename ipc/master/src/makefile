# Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

DEMO_ROOT  := ../..
MASTER_INC := $(DEMO_ROOT)/master/inc
COMMON_INC := $(DEMO_ROOT)/common/inc
O_DIR      := obj


#$(CMEM_INSTALL_DIR)/packages/ti/sdo/linuxutils/cmem/lib/cmem.a470MV


ifeq ($(BUILD_LOCAL),true)
ifndef IPC_DIR
IPC_DIR       := $(shell echo ~)/ipc_3_35_01_07
IPC_LINUX     := $(IPC_DIR)/linux
endif
ifndef CMEM_INSTALL_DIR
CMEM_INSTALL_DIR := $(shell echo ~)/linuxutils_3_23_00_01
endif
CROSS_COMPILE ?= arm-linux-gnueabihf-
CC            := $(CROSS_COMPILE)gcc
CFLAGS        := -Wall -I$(COMMON_INC) -I$(MASTER_INC) -I$(IPC_LINUX)/include -I$(IPC_DIR)/packages -D_GNU_SOURCE
LFLAGS        := -lpthread -L$(IPC_LINUX)/src/transport/\.libs -ltitransportrpmsg -L$(IPC_LINUX)/src/api/\.libs -ltiipc -L$(IPC_LINUX)/src/utils/\.libs -ltiipcutils
else
CFLAGS        := -Wall -I$(COMMON_INC) -I$(MASTER_INC) -D_GNU_SOURCE
LFLAGS        := -lpthread -ltitransportrpmsg -ltiipc -ltiipcutils
endif

SRC_FILES := $(wildcard *.c)
O_FILES   := $(SRC_FILES:%.c=%.o)

EXEC := $(DEMO_ROOT)/master/image_processing_master.out

$(EXEC): $(O_FILES)
	$(CC) $^ -o $@ $(LFLAGS)

mcip_master_main.c: $(MASTER_INC)/mcip_webpage.h $(MASTER_INC)/mcip_mem_mgmt.h
	touch $@

mcip_mem_mgmt.c: $(MASTER_INC)/mcip_mem_mgmt.h
	touch $@

mcip_webpage.c: $(MASTER_INC)/webpage_utils.h $(MASTER_INC)/mcip_bmp_utils.h $(MASTER_INC)/mcip_process.h
	touch $@

mcip_webpage_utils.c: $(MASTER_INC)/webpage_utils.h
	touch $@

mcip_bmp_utils.c: $(MASTER_INC)/mcip_bmp_utils.h
	touch $@

mcip_process.c: $(MASTER_INC)/mcip_process.h $(MASTER_INC)/mcip_mem_mgmt.h
	touch $@

$(MASTER_INC)/mcip_process.h: $(COMMON_INC)/mcip_core.h $(MASTER_INC)/mcip_bmp_utils.h
	touch $@

clean:
	rm -f *.o
	rm -f $(EXEC)
