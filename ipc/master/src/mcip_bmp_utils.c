/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* ======================================================================== */
/*  TEXAS INSTRUMENTS, INC.                                                 */
/*                                                                          */
/* ======================================================================== */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mcip_bmp_utils.h"

/*#define BMP_UTILS_DEBUG*/

static uint32_t pack_32_bit(uint8_t * value_lsb)
{
    uint8_t value_bytes[4];
    uint32_t value;
    value_bytes[0] = *value_lsb++;
    value_bytes[1] = *value_lsb++;
    value_bytes[2] = *value_lsb++;
    value_bytes[3] = *value_lsb++;
    
    value = (((uint32_t)value_bytes[3] << 0x18) |
             ((uint32_t)value_bytes[2] << 0x10) |
             ((uint32_t)value_bytes[1] << 0x08) |
             ((uint32_t)value_bytes[0] << 0x00));
    
    return value;
}

static uint16_t pack_16_bit(uint8_t * value_lsb)
{
    uint8_t value_bytes[2];
    uint16_t value;
    value_bytes[0] = *value_lsb++;
    value_bytes[1] = *value_lsb++;
    
    value = (((uint16_t)value_bytes[1] << 0x08) |
             ((uint16_t)value_bytes[0] << 0x00));
    
    return value;
}

/* BMP Gray image default header */
static bmp_header_t default_grayscale_bmp_header = {
        {
            {'B', 'M'} /*signature*/
        },
        {
            263222,  /*file_size*/
            0, 0,    /*reserved1, reserved2*/
            1078     /*bitmap_offset*/
        },
        {
            40,      /*header_size*/
            512,     /*width*/
            512,     /*height*/
            1,       /*nplanes*/
            8,       /*bitspp*/
            0,       /*compress_type*/
            262144,  /*bmp_bytesz*/
            0,       /*hres*/
            0,       /*vres*/
            256,     /*ncolors*/
            0        /*nimpcolors*/
        }
};

/****************************************************************************/
/* Bitmap header reading routine                                            */
/****************************************************************************/
int bmp_read_header (raw_image_data_t * image, bmp_header_t * hdr)
{
    bmpfile_signature_t  *hdr_signature;
    bmpfile_header_t     *hdr_file;
    bmpfile_dib_header_t *hdr_dib;
    uint8_t *image_ptr;
    
    if (image->length < 
        sizeof(bmpfile_signature_t) + sizeof(bmpfile_header_t) + sizeof(bmpfile_dib_header_t)) {
        printf ("Insufficient Image Buffer Length %d\n", image->length);
        fflush(stdout);
        return -1;
    }
    hdr_signature = &(hdr->signature);
    hdr_file      = &(hdr->file);
    hdr_dib       = &(hdr->dib);
    
    image_ptr = image->data; /* Pointing to signature data */
    hdr_signature->signature[0] = *image_ptr++;
    hdr_signature->signature[1] = *image_ptr++;
    
    image_ptr = image->data + sizeof(bmpfile_signature_t); /* Pointing to header file data */
    hdr_file->file_size = pack_32_bit(image_ptr); image_ptr += 4;
    hdr_file->reserved1 = pack_16_bit(image_ptr); image_ptr += 2;
    hdr_file->reserved2 = pack_16_bit(image_ptr); image_ptr += 2;
    hdr_file->bitmap_offset = pack_32_bit(image_ptr); image_ptr += 4;
    
    image_ptr = image->data + sizeof(bmpfile_signature_t) + sizeof(bmpfile_header_t); /* Pointing to header dib file data */
    hdr_dib->header_size = pack_32_bit(image_ptr); image_ptr += 4;
    hdr_dib->image_width = (int32_t)pack_32_bit(image_ptr); image_ptr += 4;
    hdr_dib->image_height = (int32_t)pack_32_bit(image_ptr); image_ptr += 4;
    hdr_dib->number_of_planes = pack_16_bit(image_ptr); image_ptr += 2;
    hdr_dib->bits_per_pixel = pack_16_bit(image_ptr); image_ptr += 2;
    hdr_dib->compression_type = pack_32_bit(image_ptr); image_ptr += 4;
    hdr_dib->image_size = pack_32_bit(image_ptr); image_ptr += 4;
    hdr_dib->horizontal_resolution = (int32_t)pack_32_bit(image_ptr); image_ptr += 4;
    hdr_dib->vertical_resolution = (int32_t)pack_32_bit(image_ptr); image_ptr += 4;
    hdr_dib->number_of_colors = pack_32_bit(image_ptr); image_ptr += 4;
    hdr_dib->important_color_count = pack_32_bit(image_ptr); image_ptr += 4;
    
#if BMP_UTILS_DEBUG
    printf("signature = %c%c\n", hdr->signature.signature[0], hdr->signature.signature[1]);

    printf("header.file_size = %d\n", hdr->file.file_size);
    printf("header.reserved1 = %d\n", hdr->file.reserved1);
    printf("header.reserved2 = %d\n", hdr->file.reserved2);
    printf("header.bitmap_offset = %d\n", hdr->file.bitmap_offset);

    printf("info_header.header_size = %d\n", hdr->dib.header_size);
    printf("info_header.width = %d\n", hdr->dib.image_width);
    printf("info_header.height = %d\n", hdr->dib.image_height);
    printf("info_header.nplanes = %d\n", hdr->dib.number_of_planes);
    printf("info_header.bitspp = %d\n", hdr->dib.bits_per_pixel);
    printf("info_header.compress_type = %d\n", hdr->dib.compression_type);
    printf("info_header.bmp_bytesz = %d\n", hdr->dib.image_size);
    printf("info_header.hres = %d\n", hdr->dib.horizontal_resolution);
    printf("info_header.vres = %d\n", hdr->dib.vertical_resolution);
    printf("info_header.ncolors = %d\n", hdr->dib.number_of_colors);
    printf("info_header.nimpcolors = %d\n", hdr->dib.important_color_count);
#endif
    //printf("<P>Here 5");
    fflush(stdout);
    if((hdr->signature.signature[0] != 'B') || (hdr->signature.signature[1] != 'M')) {
        printf("Incorrect MAGIC number 0x%x 0x%x\n", hdr->signature.signature[0], hdr->signature.signature[1]);
        return -1;
    }

    //printf("<P>Here 6");
    fflush(stdout);
    if((hdr->dib.bits_per_pixel != 8) &&  (hdr->dib.bits_per_pixel != 24)) {
        printf("Only 8 or 24 bits per pixel supported, the image bpp is %d\n", hdr->dib.bits_per_pixel);
        return -1;
    }

    //printf("<P>Here 7");
    fflush(stdout);
    if(hdr->dib.compression_type != BMP_RGB) {
        printf("Need a RGB type image, the image type is %d\n", hdr->dib.compression_type);
        return -1;
    }
    
    return 0;
}

/****************************************************************************/
/* Bitmap colormap reading routine                                          */
/****************************************************************************/
int bmp_read_colormap (raw_image_data_t * image, bmp_header_t * hdr,
                        bmp_color_table_t * color_table)
{
    int index;

    if(hdr->dib.number_of_colors == 0) {
        printf("Color table can't be read, ncolors = %d\n", hdr->dib.number_of_colors);
        return -1;
    }

    index = sizeof(bmpfile_signature_t) + sizeof(bmpfile_header_t) + hdr->dib.header_size;

    memcpy(color_table, image->data + index, sizeof(bmp_color_table_t) * hdr->dib.number_of_colors);

#if BMP_UTILS_DEBUG
    {
        int i;
        printf("Color Table:\nindex:\tblue\tgreen\tred\n");
        for (i = 0; i < hdr->dib.number_of_colors; i++){
            printf("%d:\t0x%02x\t0x%02x\t0x%02x\n",
                i,  color_table[i].blue, color_table[i].green, color_table[i].red);
        }
    }
#endif
    return 0;
}

/****************************************************************************/
/* Bitmap image row reading routine                                         */
/****************************************************************************/
int bmp_read_image (raw_image_data_t * image, bmp_header_t * hdr, uint8_t * pixel_array_rgb)
{ 
    int i;
    int index;
    int pixel_size = hdr->dib.bits_per_pixel / 8;
    int row_width  = hdr->dib.image_width * pixel_size;
    int row_width_with_pad = ((row_width) + 3) & (~3);

    for(i = 0; i < hdr->dib.image_height; i++) {
        index = hdr->file.bitmap_offset + (row_width_with_pad * (hdr->dib.image_height - i - 1));
        memcpy(pixel_array_rgb + (i * row_width), image->data + index, row_width);
#if BMP_UTILS_DEBUG
        if (i == 0) {
            int j, k;
            printf("Pixel Value (0): ");
            for(j = 0; j < 512; j++){
                for(k = 0; k < pixel_size; k++) {
                    printf("%x", (uint8_t)*(pixel_array_rgb + (i * row_width) + (j * pixel_size) + k));
                }
                printf(" ");
            }
            printf("\n");
        }
#endif
    }
    return 0;
}

/****************************************************************************/
/* Write a gray scale image file                                            */
/****************************************************************************/
int bmp_write_gray_bmpfile (raw_image_data_t * image, uint8_t * pixel_array,
                            uint32_t width, uint32_t height)
{
    int i, j;
    int row_width_with_pad = (width + 3) & (~3);
    int pad_size = row_width_with_pad - width;
    bmp_color_table_t * color_table = 0;
    uint8_t * pad_array = 0;
    bmp_header_t hdr = default_grayscale_bmp_header;
    int ret_val = 0;
    uint8_t *dest_ptr, *src_ptr;

    if(pad_size) {
        pad_array = calloc(pad_size, 1);
    }

    hdr.dib.image_height     = height;
    hdr.dib.image_width      = width;
    hdr.dib.image_size = (row_width_with_pad * hdr.dib.image_height);

    color_table = calloc(sizeof(bmp_color_table_t), hdr.dib.number_of_colors);

    for(i = 0; i < hdr.dib.number_of_colors; i++) {
        color_table[i].blue  = i;
        color_table[i].green = i;
        color_table[i].red   = i;
    }

    hdr.file.file_size =
            sizeof(bmpfile_signature_t) + sizeof(bmpfile_header_t)
            + hdr.dib.header_size
            + (sizeof(bmp_color_table_t) * hdr.dib.number_of_colors)
            + (row_width_with_pad * hdr.dib.image_height);
    hdr.file.bitmap_offset =
            sizeof(bmpfile_signature_t)
            + sizeof(bmpfile_header_t) + hdr.dib.header_size
            + (sizeof(bmp_color_table_t) * hdr.dib.number_of_colors);

    if (image->length < hdr.file.file_size) {
        printf("Insufficient image array size %d (expected %d)", 
            image->length, hdr.file.file_size);
        ret_val = -1;
        goto close_n_exit;
    }

    dest_ptr = image->data;
    src_ptr = (uint8_t*)&hdr.signature;
    for(i = 0; i < sizeof(bmpfile_signature_t); i++)
        *dest_ptr++ = *src_ptr++;
    
    src_ptr = (uint8_t*)&hdr.file;
    for(i = 0; i < sizeof(bmpfile_header_t); i++)
        *dest_ptr++ = *src_ptr++;
    
    src_ptr = (uint8_t*)&hdr.dib;
    for(i = 0; i < sizeof(bmpfile_dib_header_t); i++)
        *dest_ptr++ = *src_ptr++;
    
    src_ptr = (uint8_t*)color_table;
    for(i = 0; i < (sizeof(bmp_color_table_t) * hdr.dib.number_of_colors); i++)
        *dest_ptr++ = *src_ptr++;

    for(i = hdr.dib.image_height - 1; i >= 0; i--) {
        src_ptr = (uint8_t*)(pixel_array + (hdr.dib.image_width * i));
        for(j = 0; j < hdr.dib.image_width; j++)
            *dest_ptr++ = *src_ptr++;
        if (pad_size) {
            src_ptr = (uint8_t*)(pad_array);
            for(j = 0; j < pad_size; j++)
                *dest_ptr++ = *src_ptr++;
        }
    }

    ret_val = 0;

close_n_exit:
    if(color_table) free(color_table);
    if(pad_array)   free(pad_array);

    return ret_val;
}

/****************************************************************************/
/* Get a gray scale image file file size                                    */
/****************************************************************************/
uint32_t bmp_get_gray_bmpfile_size (uint32_t width, uint32_t height)
{
    int row_width_with_pad = (width + 3) & (~3);
    bmp_header_t hdr = default_grayscale_bmp_header;

    return(sizeof(bmpfile_signature_t) + sizeof(bmpfile_header_t)
            + hdr.dib.header_size
            + (sizeof(bmp_color_table_t) * hdr.dib.number_of_colors)
            + (row_width_with_pad * height));
}

