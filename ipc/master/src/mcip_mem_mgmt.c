/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* =============================================================================
 *  @file   MessageQApp.c
 *
 *  @brief  Sample application for MessageQ module between MPU and Remote Proc
 *
 *  ============================================================================
 */

/* Standard headers */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>

/* SysLink/IPC Headers: */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>

#include "mcip_core.h"
#include "mcip_mem_mgmt.h"

/* App defines: Must match on remote proc side: */
#define HEAPID              0u
#define SLAVE_MESSAGEQNAME  "SLAVE_DDR"
#define MPU_MESSAGEQNAME    "HOST_DDR"

#define PROC_ID_DFLT        1       /* Host is zero, remote cores start at 1 */
#define NUM_LOOPS_DFLT      100

typedef struct AllocMsg {
    MessageQ_MsgHeader header;
    int8_t operation;
    size_t size;
    size_t align;
    void *ptr;
} AllocMsg;

static MessageQ_Handle g_ddrMsgHandle = (MessageQ_Handle) NULL;
static MessageQ_QueueId g_remoteQueueId = (MessageQ_QueueId) NULL;
static int g_devmem = 0;

/* Static array that holds mapping between virtual and physical addresses */
typedef struct MappedPtr {
    void *virt;
    void *phys;
    off_t offset;
    size_t size;
} MappedPtr;

static MappedPtr g_ptrArray[128];
static uint8_t map_index = 0;

static void * get_phys_ptr(void *virt_ptr)
{
    int i;
    void *result;
    for(i = 0; i < 128; i++)
    {
        if (virt_ptr >= (g_ptrArray[i].virt + g_ptrArray[i].offset) && virt_ptr <= (g_ptrArray[i].virt + g_ptrArray[i].offset + g_ptrArray[i].size))
        {
            result = (virt_ptr - (g_ptrArray[i].virt + g_ptrArray[i].offset)) + g_ptrArray[i].phys;
            return result;
        }
    }
    return NULL;
}

static void * get_virt_ptr(void *phys_ptr)
{
    int i;
    void *result;
    for(i = 0; i < 128; i++)
    {
        if (phys_ptr >= g_ptrArray[i].phys && phys_ptr <= (g_ptrArray[i].phys + g_ptrArray[i].size))
        {
            result = (phys_ptr - g_ptrArray[i].phys) + (g_ptrArray[i].virt + g_ptrArray[i].offset);
            return result;
        }
    }
    return NULL;
}

/* MessageQ_put wrapper */
int DDR_MessageQ_put(MessageQ_QueueId queueId, MessageQ_Msg msg)
{
    processing_info_t * msg_info;
    int status;
    
    msg_info = &(((process_message_t*)msg)->info);
    
    msg_info->rgb_in = (uint8_t*)get_phys_ptr(msg_info->rgb_in);
    msg_info->p_color_table = (color_table_t*)get_phys_ptr(msg_info->p_color_table);
    msg_info->out = (uint8_t*)get_phys_ptr(msg_info->out);
    msg_info->scratch_buf[0] = (uint8_t*)get_phys_ptr(msg_info->scratch_buf[0]);
    msg_info->scratch_buf[1] = (uint8_t*)get_phys_ptr(msg_info->scratch_buf[1]);
    
    status = MessageQ_put(queueId, msg);
    
    msg_info->rgb_in = (uint8_t*)get_virt_ptr(msg_info->rgb_in);
    msg_info->p_color_table = (color_table_t*)get_virt_ptr(msg_info->p_color_table);
    msg_info->out = (uint8_t*)get_virt_ptr(msg_info->out);
    msg_info->scratch_buf[0] = (uint8_t*)get_virt_ptr(msg_info->scratch_buf[0]);
    msg_info->scratch_buf[1] = (uint8_t*)get_virt_ptr(msg_info->scratch_buf[1]);
    
    return status;
}

/* MessageQ_get wrapper */
int DDR_MessageQ_get(MessageQ_Handle handle, MessageQ_Msg *msg, uint32_t timeout)
{
    int status;
    
    status = MessageQ_get(handle, msg, timeout);
    
    if (status < 0) return status;
    
    processing_info_t * msg_info;
    
    msg_info = &(((process_message_t*)(*msg))->info);
    
    msg_info->rgb_in = (uint8_t*)get_virt_ptr(msg_info->rgb_in);
    msg_info->p_color_table = (color_table_t*)get_virt_ptr(msg_info->p_color_table);
    msg_info->out = (uint8_t*)get_virt_ptr(msg_info->out);
    msg_info->scratch_buf[0] = (uint8_t*)get_virt_ptr(msg_info->scratch_buf[0]);
    msg_info->scratch_buf[1] = (uint8_t*)get_virt_ptr(msg_info->scratch_buf[1]);
    
    return status;
}

/* Initialize the MessageQ */
int DDR_MEM_Init()
{
    Int32               status = 0;
    MessageQ_Params     msgParams;
    char                remoteQueueName[64];
        
    /* Create the local Message Queue for receiving. */
    MessageQ_Params_init(&msgParams);
    g_ddrMsgHandle = MessageQ_create(MPU_MESSAGEQNAME, &msgParams);
    if (g_ddrMsgHandle == NULL) {
        printf("Error in MessageQ_create\n");
        return -1;
    }
    
    sprintf(remoteQueueName, "%s_%s", SLAVE_MESSAGEQNAME,
            MultiProc_getName(PROC_ID_DFLT));
    
    /* Poll until remote side has it's messageQ created before we send: */
    do {
        status = MessageQ_open(remoteQueueName, &g_remoteQueueId);
        sleep(1);
    } while (status == MessageQ_E_NOTFOUND);
    
    if (status < 0) {
        printf("Error in MessageQ_open [%d]\n", status);
        return -1;
    }
    
    g_devmem = open("/dev/mem", O_RDWR | O_SYNC);
    if(g_devmem < 0)
    {
        printf("Error opening /dev/mem\n");
        return -1;
    }
    
    return 0;
}
    
/* Allocates a section of data in DDR */
void* DDR_alloc(size_t size)
{
    MessageQ_Msg        msg = NULL;
    void *phys_mem, *virt_mem;
    off_t PageSize, PageAddress;
    
    msg = MessageQ_alloc(HEAPID, sizeof(AllocMsg));
    if (msg == NULL) {
        return NULL;
    }
    
    MessageQ_setMsgId(msg, 0);
    MessageQ_setReplyQueue(g_ddrMsgHandle, msg);
    
    PageSize = sysconf(_SC_PAGE_SIZE);
    
    ((AllocMsg*)msg)->operation = 0; // Alloc
    ((AllocMsg*)msg)->align = PageSize;
    ((AllocMsg*)msg)->size = size;
    
    if (MessageQ_put(g_remoteQueueId, msg) < 0) {
        MessageQ_free(msg);
        return NULL;
    }
    
    if (MessageQ_get(g_ddrMsgHandle, &msg, MessageQ_FOREVER) < 0)
    {
        MessageQ_free(msg);
        return NULL;
    }
    
    phys_mem = ((AllocMsg*)msg)->ptr;
    if(phys_mem == NULL)
        return NULL;
    
    g_ptrArray[map_index].phys = phys_mem;
    
    /* This should do nothing as the alloc should be aligned to PageSize */
    PageSize = (off_t) phys_mem % PageSize;
    PageAddress = (off_t) (phys_mem - PageSize);
    g_ptrArray[map_index].offset = PageSize;
    
    /* should we malloc the address of ddr_mem? */
    
    virt_mem = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, g_devmem, PageAddress);
    
    g_ptrArray[map_index].size = size;
    g_ptrArray[map_index++].virt = virt_mem;
    
    /* 
       should really use a linked list, but since this method
       of memory allocation is temporary this should work for now.
       Shouldn't be allocating so much memory that we use the entire
       buffer either.
    */
    if(map_index >= 128) map_index = 0;
    virt_mem += PageSize;
    
    MessageQ_free(msg);
    
    return virt_mem;
}

/* Frees a a section of data in DDR */
void DDR_free(void* ptr, size_t size)
{
    int                 i;
    void                *phys_ptr = NULL;
    void                *virt_base = NULL;
    MessageQ_Msg        msg = NULL;
    
    // printf("<P>Trying to find physical address for virtual address 0x%lX", ptr);
    
    for(i = 0; i < 128; i++)
    {
        if ((g_ptrArray[i].virt + g_ptrArray[i].offset) == ptr)
        {
            phys_ptr = g_ptrArray[i].phys;
            virt_base = g_ptrArray[i].virt;
            g_ptrArray[i].virt = NULL;
            g_ptrArray[i].phys = NULL;
            g_ptrArray[i].offset = 0;
            g_ptrArray[i].size = 0;
            continue;
        }
    }
    
    if(phys_ptr == NULL || virt_base == NULL) {
        printf("Could not find phys to virt mapping<br>");
        return;
    }
    
    /* unmap the virtual address */
    munmap(virt_base, size);
    
    /* Free the physical address using IPC */
    msg = MessageQ_alloc(HEAPID, sizeof(AllocMsg));
    if (msg == NULL) {
        return;
    }
    
    MessageQ_setMsgId(msg, 0);
    MessageQ_setReplyQueue(g_ddrMsgHandle, msg);
    
    ((AllocMsg*)msg)->operation = 1; // Free
    ((AllocMsg*)msg)->align = 0;
    ((AllocMsg*)msg)->size = size;
    ((AllocMsg*)msg)->ptr = phys_ptr;
    
    if (MessageQ_put(g_remoteQueueId, msg) < 0) {
        MessageQ_free(msg);
        return;
    }
    
    if (MessageQ_get(g_ddrMsgHandle, &msg, MessageQ_FOREVER) < 0)
    {
        MessageQ_free(msg);
        return;
    }
    
    MessageQ_free(msg);
}

/* Destroy the MessageQ */
void DDR_MEM_Destroy()
{
    /* put if statements to determine necessity */
    if(g_remoteQueueId)
        MessageQ_close(&g_remoteQueueId);
    if(g_ddrMsgHandle)
        MessageQ_delete(&g_ddrMsgHandle);
    if(g_devmem)
        close(g_devmem);
}
