/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* SysLink/IPC Headers: */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/transports/TransportRpmsg.h>
#include <ti/ipc/MessageQ.h>

#include "mcip_mem_mgmt.h"
#include "mcip_process.h"
#include "webpage_utils.h"

/*
 * We use this buffer an variables for holding anything we upload.
*/
static char * gRxBuffer = 0; /* We use this buffer any uploaded file */
static int gRxBuffer_size = 0;

static char * gTxImgBuffer = 0; /* Buffer to store output image */
static int gTxImgBuffer_size = 0;

#define gTempRxBuffer_SIZE 1024
static char gTempRxBuffer[gTempRxBuffer_SIZE];

/*
 *  Create Result page tags 
 */
static char header_doc_type[] = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
static char html_page_start[] = "<html>\n";
static char html_page_end[] = "</html>\n";
static char result_page_header[] = "<head><meta content=\"text/html; charset=ISO-8859-1\" http-equiv=\"content-type\">\n\
<title>Multicore Image Processing Demonstration - Output</title>\n\
<style type=\"text/css\">\n\
#imageblock {border-width: 1; border: solid; height:640px; overflow: auto;}\n\
</style>\n\
</head>\n";
static char html_body_start[]="<body style=\"font-family: helvetica;\">\n";
static char html_body_end[]="</body>\n";
static char result_page_header_table[]="<table style=\"text-align: left; background-color: white; height: 132px;\">\n\
<tbody>\n\
<tr>\n\
<td style=\"width: 147px;\"><img src=\"../../images/mc-chip.png\"></td>\n\
<td><span style=\"font-size:200%;\">Multicore Image Processing Demonstration - Output</span></td>\n\
</tr>\n\
</tbody>\n\
</table> <br>\n";
// static char link_main_page[] = "<a href='/usr/share/matrix-gui-2.0/apps/imgprocdemo_demo/imgprocdemo_demo.html'>Return to Main Page</a><br><br>\n";
static char result_page_table_start[] = "<table style=\"text-align: left;\" border=\"1\" cellpadding=\"10\" cellspacing=\"2\"> <tbody>\n";
static char result_page_table_end[] = "</tbody> </table> <br><br>\n";
static char result_page_table_row_fmt[] = "<tr><th>%s</th><td>%s</td></tr>\n";
static char result_page_image_text_fmt[] = "<span style=\"font-weight: bold;\">%s<br> <br> </span>\n";
static char result_page_image_fmt[] = "<div style=\"height: 640px; scrolling: auto; overflow:scroll;\"><img src=\"%s\"></div> <br><br>\n";

void send_error_page(char * message)
{
    printf(html_body_start);
    printf(result_page_header_table);
    printf("<br><br>");
    printf("<strong>Following error occured while processing the request</strong><br><br>");
    printf(message);
    printf("<br><br>");
    printf(html_body_end);
    printf(html_page_end);
}

/*
 * Creates result page dynamically
 */
int serve_result_page(void)
{
    FILE *input_image_file, *output_image_file;
    int ContentLength = 0;
    int input_file_length = 0;
    int output_file_length = 0;
    raw_image_data_t input_image = {0, 0};
    raw_image_data_t output_image = {0, 0};
    bmp_header_t image_hdr;
    char temp_array[32];
    double delay = 0;
    int    number_of_cores;
    char * temp_str = 0;
    int32_t status = 0;
    
    printf(header_doc_type);
    printf(html_page_start);
    printf(result_page_header);
    
    Ipc_transportConfig(&TransportRpmsg_Factory);
    status = Ipc_start();
    
    if (status >= 0) {
        if (DDR_MEM_Init() < 0) {
            send_error_page("DDR Setup failed\n");
            DDR_MEM_Destroy();
            Ipc_stop();
            return 1;
        }
    }
    else {
        send_error_page("IPC_start() failed");
        return 1;
    }

    temp_str = getenv("CONTENT_LENGTH");
    if(temp_str == NULL || sscanf(temp_str, "%d", &ContentLength) != 1)
    {
        send_error_page("Error in invocation - wrong FORM probably.");
        return 1;
    }

    gRxBuffer = (char*)DDR_alloc(ContentLength);
    if (!gRxBuffer) {
        int len;
        /* Read all the data in same buffer otherwise the page won't accept
           any html request */
        while (ContentLength > 0)
        {
            if ((ContentLength - gTempRxBuffer_SIZE) > gTempRxBuffer_SIZE)
                len = gTempRxBuffer_SIZE;
            else
                len = ContentLength;
            
            fread(gTempRxBuffer, sizeof(char), len, stdin);
            ContentLength -= len;
        }
        send_error_page("Malloc failed for the POST data");
        return 1;
    }
    gRxBuffer_size = ContentLength;

    input_file_length = cgiParseMulti(ContentLength, (unsigned char *) gRxBuffer );

    if (input_file_length <= 0) {
        switch (input_file_length) {
        case HTML_RECEIVE_ERROR:
            send_error_page("Receive error");
            break;
        case HTML_PARSER_ERROR:
            send_error_page("CGI parser error");
            break;
        default:
            send_error_page("Unknown error in cgiParseMulti");
            break;
        }
        DDR_free(gRxBuffer, gRxBuffer_size);
        return 1;
    }

    input_image.data = (uint8_t *) gRxBuffer;
    input_image.length = gRxBuffer_size;

    output_image.data = (uint8_t *) gTxImgBuffer;
    output_image.length = gTxImgBuffer_size;

    temp_str = html_getValueFor("numberofcores");

    number_of_cores = atoi(temp_str);

    if (number_of_cores == 0) {
        send_error_page("Invalid number of cores in the input");
        DDR_free(gRxBuffer, gRxBuffer_size);
        return 1;
    }
    
    fflush(stdout);
    if (mc_process_bmp(edge_detection, &input_image, &output_image, number_of_cores, &delay) < 0) {
        send_error_page("Error in running edge detection");
        return 1;
    }

    gTxImgBuffer_size = output_image.length;
    gTxImgBuffer      = (char *) output_image.data;

    if (bmp_read_header(&output_image, &image_hdr) < 0) {
        send_error_page("Error in reading output image header");
        return 1;
    }

    output_file_length = image_hdr.file.file_size;

    if ((input_image_file = fopen("/dev/shm/input_image.bmp", "wb")) == NULL) {
        send_error_page("Error opening input image file");
        DDR_free(gRxBuffer, gRxBuffer_size);
        DDR_free(gTxImgBuffer, gTxImgBuffer_size);
        return 1;
    }

    if ((output_image_file = fopen("/dev/shm/output_image.bmp", "wb")) == NULL) {
        send_error_page("Error opening output image file");
        DDR_free(gRxBuffer, gRxBuffer_size);
        DDR_free(gTxImgBuffer, gTxImgBuffer_size);
        return 1;
    }

    if ((fwrite(input_image.data, sizeof(char), input_file_length, input_image_file)) < input_file_length) {
        snprintf(temp_array, 32, "Error writing input data to file");
        send_error_page(temp_array);
        DDR_free(gRxBuffer, gRxBuffer_size);
        DDR_free(gTxImgBuffer, gTxImgBuffer_size);
        fclose(input_image_file);
        fclose(output_image_file);
        return 1;
    }

    unlink("input_image.bmp");
    symlink("/dev/shm/input_image.bmp", "input_image.bmp");

    if ((fwrite(output_image.data, sizeof(char), output_file_length, output_image_file)) < output_file_length) {
        send_error_page("Error writing output data to file");
        DDR_free(gRxBuffer, gRxBuffer_size);
        DDR_free(gTxImgBuffer, gTxImgBuffer_size);
        fclose(input_image_file);
        fclose(output_image_file);
    }
    
    unlink("output_image.bmp");
    symlink("/dev/shm/output_image.bmp", "output_image.bmp");

    fclose(input_image_file);
    fclose(output_image_file);

    DDR_free(gRxBuffer, gRxBuffer_size);
    DDR_free(gTxImgBuffer, gTxImgBuffer_size);

    printf(html_body_start);
    printf(result_page_header_table);
    printf(result_page_table_start);
    printf(result_page_table_row_fmt, "Image Processing Function", html_getValueFor("processingtype"));
    snprintf(temp_array, 32, "%dx%d", image_hdr.dib.image_width, image_hdr.dib.image_height);
    printf(result_page_table_row_fmt, "Image Dimension (in pixels)", temp_array);
    snprintf(temp_array, 32, "%d", input_file_length);
    printf(result_page_table_row_fmt, "Input Image Size (in bytes)", temp_array);
    printf(result_page_table_row_fmt, "Number of Cores Used", html_getValueFor("numberofcores"));
    snprintf(temp_array, 32, "%.3lfms", delay);
    printf(result_page_table_row_fmt, "Processing Time", temp_array); 
    printf(result_page_table_end);
    printf(result_page_image_text_fmt, "Input Image");
    printf(result_page_image_fmt, "input_image.bmp");
    printf(result_page_image_text_fmt, "Output Image");
    printf(result_page_image_fmt, "output_image.bmp");
    printf(html_body_end);
    printf(html_page_end);
    

    DDR_MEM_Destroy();
    Ipc_stop();

    return 0;
}

