/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Types.h>
#include <xdc/runtime/IHeap.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/heaps/HeapMem.h>

#include <ti/ipc/MessageQ.h>

#include "image_processing/ipc/common/inc/mcip_core.h"

#ifdef STATISTICAL_PROFILING_ENABLE

extern int statistical_profiling_start (void);
extern int statistical_profiling_end (void);

#endif

#if defined (AETINT_MEMWATCH) || defined(PCT_MEMWATCH)

#include <Ctools_UCLib\include\ctools_uclib.h>

#endif

#ifdef PCT_MEMWATCH

#define DSP_ETB_NO_DRAIN 1

#elif AETINT_MEMWATCH

#define DSP_ETB_NO_DRAIN 0

#endif

void slave_main(void)
{
    process_message_t * p_msg = 0;    
    MessageQ_Handle  h_receive_queue = 0;
    MessageQ_QueueId reply_queue_id = 0;
    char receive_queue_name[16];
    
    System_printf("Start core task\n");

    GET_SLAVE_QUEUE_NAME(receive_queue_name, MultiProc_self());

#if defined (AETINT_MEMWATCH) || defined(PCT_MEMWATCH)

#if DSP_ETB_NO_DRAIN

    if(ctools_dsptrace_init() != CTOOLS_SOK)
    {
        System_printf ("Error: DSP Trace init Failed\n");
    }

#endif

    unsigned int dummy_test_variable = 0;

#ifdef AETINT_MEMWATCH

    // Enable the watch point
    if (ctools_memwatch_aetint_setup(&dummy_test_variable, &dummy_test_variable) != CTOOLS_SOK)
    {
        System_printf ("Error: ctools_memwatch_aetint_setup Failed\n");
    }

    //Route AETINT as an Exception to C66x core

    unsigned int exp_mask_reg;
    exp_mask_reg = *(volatile unsigned int *)0x018000C0;
    *(volatile unsigned int *)0x018000C0 = exp_mask_reg & (~(0x1 << 9));

#elif PCT_MEMWATCH

    // Enable the watch point
    if (ctools_memwatch_store_pc_setup(&dummy_test_variable, &dummy_test_variable) != CTOOLS_SOK)
    {
        System_printf ("Error: ctools_memwatch_store_pc_setup Failed\n");
    }

#endif

#endif
    
    /* Create the local message queue */
    h_receive_queue = MessageQ_create(receive_queue_name, NULL);    
    if (h_receive_queue == NULL) {
        System_printf("MessageQ_create failed\n" );
        goto close_n_exit;
    }
    
    System_printf("Core_Task: created MessageQ: %s; QueueID: 0x%x\n",
        receive_queue_name, MessageQ_getQueueId(h_receive_queue));

    System_printf("Waiting for message from master\n");
    if (MessageQ_get(h_receive_queue, (MessageQ_Msg *)&p_msg, MessageQ_FOREVER) < 0) {
        System_printf("%s: This should not happen since timeout is forever\n", receive_queue_name);
        goto close_n_exit;
    }

    System_printf("Received message\n");
    reply_queue_id = MessageQ_getReplyQueue(p_msg);
    if (reply_queue_id == MessageQ_INVALIDMESSAGEQ) {
        System_printf("receive_queue_name: Ignoring the message as reply queue is not set.\n", receive_queue_name);
        goto close_n_exit;
    }

#if defined (AETINT_MEMWATCH) || defined(PCT_MEMWATCH)

    // Perform a valid access to dummy test variable

    // Disable the AET watch point
    if (ctools_memwatch_disable() != CTOOLS_SOK)
    {
        System_printf ("Error: ctools_memwatch_disable Failed\n");
    }

    dummy_test_variable = 0;

    // Re-enable the AET watch point
    if (ctools_memwatch_enable() != CTOOLS_SOK)
    {
        System_printf ("Error: ctools_memwatch_enable Failed\n");
    }

#endif

#ifdef STATISTICAL_PROFILING_ENABLE

    // Start Statistical profiling
    if (statistical_profiling_start() < 0)
    {
        System_printf ("Error: statistical_profiling_start Failed\n");
    }

#endif

        process_rgb(&(p_msg->info));

#ifdef STATISTICAL_PROFILING_ENABLE

    // End Statistical profiling
    if (statistical_profiling_end() < 0)
    {
        System_printf ("Error: statistical_profiling_end Failed\n");
    }

#endif

#if defined (AETINT_MEMWATCH) || defined(PCT_MEMWATCH)

        // Perform an invalid access to dummy test variable
        dummy_test_variable = 1;

        //Give delay for CPU to halt or before we read AET counter

        unsigned int i;

        for(i=0;i<10;i++);

        dummy_test_variable = 2;

        for(i=0;i<10;i++);

        dummy_test_variable = 3;

        for(i=0;i<10;i++);

        dummy_test_variable = 4;

        for(i=0;i<10;i++);

        dummy_test_variable = 5;

        for(i=0;i<10;i++);

        dummy_test_variable = 6;

        for(i=0;i<10;i++);

        // Perform an invalid access to dummy test variable
        dummy_test_variable = 1;

        // Close the AET watch point
        if (ctools_memwatch_close() != CTOOLS_SOK)
        {
            System_printf ("Error: ctools_memwatch_close Failed\n");
        }

#if DSP_ETB_NO_DRAIN

    if(ctools_dsptrace_shutdown() != CTOOLS_SOK)
    {
        System_printf ("Error: DSP Trace shutdown Failed\n");
    }

#endif

#endif

#ifdef EVENT_PROFILE
        unsigned int dummy_variable;

        *(volatile unsigned int *)(0x99000004) = 0xABCD1234;

        dummy_variable = *(volatile unsigned int *)(0x99000004);

        dummy_variable++;
#endif

    System_printf("Sending reply...\n");
    /* send the message to the remote processor */
    if (MessageQ_put(reply_queue_id, (MessageQ_Msg)p_msg) < 0) {
        System_printf("%s: MessageQ_put had a failure error\n", receive_queue_name);
    }

close_n_exit:
    if(h_receive_queue) MessageQ_delete(&h_receive_queue);
    
    System_printf("Shuting down\n");
}

