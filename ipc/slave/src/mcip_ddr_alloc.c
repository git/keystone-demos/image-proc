/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Types.h>
#include <xdc/runtime/IHeap.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/heaps/HeapMem.h>

#include <ti/ipc/MessageQ.h>

#define DDR_HEAP (HeapMem_Handle_to_xdc_runtime_IHeap(ddr_heap))

#define SLAVE_MESSAGEQNAME  "SLAVE_DDR"

#define MessageQ_payload(m) ((void *)((char *)(m) + sizeof(MessageQ_MsgHeader)))

typedef struct AllocMsg {
    MessageQ_MsgHeader header;
    int8_t operation;
    size_t size;
    size_t align;
    void *ptr;
} AllocMsg;

extern HeapMem_Handle DDR_Heap;

/*
 *  ======== DDR_MEM_MGMT ========
 *  Handles requests from master to
 *  allocate and free DDR memory
 */
Void DDR_MEM_MGMT()
{
    MessageQ_Msg        msg;
    MessageQ_Handle     messageQ;
    MessageQ_QueueId    master_messageQ;
    Memory_Stats        ddr_stats;
    Char                localQueueName[64];
    UInt16              procId;
    void *ddr_ptr;
    size_t size, align;
    
    System_printf("Hello\n");
    
    /* Construct a MessageQ name adorned with core name: */
    System_sprintf(localQueueName, "%s_%s", SLAVE_MESSAGEQNAME,
                   MultiProc_getName(MultiProc_self()));

    messageQ = MessageQ_create(localQueueName, NULL);
    if (messageQ == NULL) {
        System_abort("MessageQ_create failed\n");
    }
    
    System_printf("DDR_MEM_MGMT: created MessageQ: %s; QueueID: 0x%x\n",
        localQueueName, MessageQ_getQueueId(messageQ));
    while(1)
    {
        /* Wait for message from master */
        System_printf("Awaiting message from host...\n");
        MessageQ_get(messageQ, &msg, MessageQ_FOREVER);
        
        master_messageQ = MessageQ_getReplyQueue(msg);
        procId = MessageQ_getProcId(master_messageQ);
        
        System_printf("Received msg from (procId:master_messageQ): 0x%x:0x%x\n"
            "\tpayload: %d bytes.\n",
            procId, master_messageQ,
            (MessageQ_getMsgSize(msg) - sizeof(MessageQ_MsgHeader)));
        
        if (((AllocMsg*)msg)->operation == 0) // Alloc
        {
            size = ((AllocMsg*)msg)->size;
            align = ((AllocMsg*)msg)->align;
            
            Memory_getStats(DDR_HEAP, &ddr_stats);
            if (ddr_stats.largestFreeSize < size) // Not enough space to allocate
            {
                ddr_ptr = NULL;
                System_printf("Could not allocate memory.\nRequested size: %d\nSize available: %d\n", size, ddr_stats.largestFreeSize);
            }
            else
            {
                ddr_ptr = Memory_alloc(DDR_HEAP, size, align, NULL);
                System_printf("Allocating %d bytes of memory at 0x%x\n", size, (unsigned int)ddr_ptr);
            }
            ((AllocMsg*)msg)->ptr = ddr_ptr;
        }
        else // Free
        {
            ddr_ptr = ((AllocMsg*)msg)->ptr;
            size = ((AllocMsg*)msg)->size;
            
            System_printf("Freeing %d bytes of memory at 0x%x\n", size, (unsigned int)ddr_ptr);
            
            Memory_free(DDR_HEAP, ddr_ptr, size);
        }
        
        MessageQ_put(master_messageQ, msg);
    }
}
